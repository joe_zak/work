import "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import React from "react";
import { Text } from "react-native";
import { HomeScreen } from "./screens/HomeScreen";
import { SafeAreaProvider } from "react-native-safe-area-context";

const Drawer = createDrawerNavigator();

const ProfileScreen = ({ navigation, route }) => {
	return <Text>Profile</Text>;
};

const App = () => {
	return (
		<SafeAreaProvider>
			{/* <NavigationContainer>
				<Drawer.Navigator initialRouteName="Home">
					<Drawer.Screen name="Home" component={HomeScreen} />
					<Drawer.Screen name="Profile" component={ProfileScreen} />
				</Drawer.Navigator>
			</NavigationContainer> */}
			<HomeScreen />
		</SafeAreaProvider>
	);
};

export default App;
