import React from "react";
import { Text } from "react-native";
import BootstrapIData from "./bootstrap-icons.json";

const BootstrapIcon = (props) => {
	let { style = {}, icon = "" } = props;
	return (
		<Text style={{ fontFamily: "bootstrap-icons", ...style }}>
			{BootstrapIData[icon]}
		</Text>
	);
};

export default BootstrapIcon;
