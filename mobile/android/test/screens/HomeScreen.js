import React, { useState } from "react";
import { View, Text, TouchableOpacity, SectionList } from "react-native";
import { Avatar, Badge, Header, SearchBar } from "react-native-elements";
import BootstrapIcon from "../components/BootstrapIcon";
import HomeStyle from "../styles/HomeStyle";

export const HomeHeader = (props) => {
	const { nav } = props;
	return (
		<Header placement="left" backgroundColor="#fff" barStyle="dark-content">
			<TouchableOpacity onPress={() => nav.toggleDrawer()}>
				<BootstrapIcon icon="list" style={HomeStyle.header} />
			</TouchableOpacity>
			<Text style={{ color: "#000", fontSize: 21 }}>
				Buy something or I'll kill you
			</Text>
			<TouchableOpacity>
				<View>
					<BootstrapIcon icon="cart4" style={HomeStyle.header} />
					<Badge
						status="error"
						value="9+"
						containerStyle={{ position: "absolute", top: -4, right: -4 }}
					/>
				</View>
			</TouchableOpacity>
		</Header>
	);
};

export const HomeScreen = ({ navigation }) => {
	const [search, updateSearch] = useState("");
	return (
		<>
			<HomeHeader nav={navigation} />
			<SearchBar
				placeholder="Search..."
				onChangeText={(text) => updateSearch(text)}
				value={search}
				containerStyle={{ paddingTop: 0, paddingBottom: 0 }}
				platform="android"
			/>
			<SectionList></SectionList>
		</>
	);
};
