import { StyleSheet } from "react-native";

const HomeStyle = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	button: {
		alignItems: "center",
		backgroundColor: "#DDDDDD",
		padding: 10,
		marginBottom: 10,
	},
	input: {
		height: 40,
		width: 200,
		borderRadius: 10,
		padding: 10,
		borderColor: "gray",
		borderWidth: 1,
	},
	header: {
		fontSize: 25,
		textAlignVertical: "center",
		// color: "#fff",
	},
});

export default HomeStyle;
