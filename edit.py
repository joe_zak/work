import tkinter as tk
import tkinter.scrolledtext
import tkinter.ttk
import gnupg
import json

class App:
    def __init__(self):
        self.error = False
        self.pword = None
        self.gpg = gnupg.GPG()
        self.root = tk.Tk()
        self.root.title("Edit")
        self.root.geometry("400x250")
        self.root.minsize(400, 250)
        self.root.bind("<Escape>", lambda event: self.root.destroy())
        frame = tk.Frame(self.root)
        frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)
        edit_b = tk.ttk.Button(frame, text="Edit", command=lambda: self.passw(self.edit))
        edit_b.pack_propagate(0)
        edit_b.grid(row=1, column=0, padx=20)
        view_b = tk.ttk.Button(frame, text="View", command=lambda: self.passw(self.view))
        view_b.pack_propagate(0)
        view_b.grid(row=1, column=1, padx=20)

    def passw(self, func):
        self.func = func
        if not self.pword:
            self.root.withdraw()
            self.window = tk.Toplevel(self.root)
            self.window.geometry("400x250")
            self.window.minsize(400, 250)
            self.window.protocol("WM_DELETE_WINDOW", lambda: [self.window.destroy(), self.root.destroy()])
            self.window.bind("<Escape>", lambda event: [self.window.destroy(), self.root.update(), self.root.iconify()])
            frame1 = tk.Frame(self.window)
            frame1.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
            pword_l = tk.ttk.Label(frame1, text="Password:")
            pword_l.pack(side=tk.LEFT, padx=10)
            self.pword_e = tk.ttk.Entry(frame1, show="*")
            self.pword_e.pack_propagate(0)
            self.pword_e.pack(side=tk.LEFT, fill=tk.X, expand=True, padx=15)
            self.pword_e.bind("<Return>", self.check)
            frame2 = tk.Frame(self.window)
            frame2.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
            check_b = tk.ttk.Button(frame2, text="Check", command=self.check)
            check_b.pack(side=tk.TOP, pady=50)
        else:
            self.func()

    def check(self, event=None):
        data = self.gpg.decrypt_file(open("./data_json", "rb"), passphrase=self.pword_e.get())
        if data.ok:
            self.pword = self.pword_e.get()
            self.data = json.loads(str(data))
            self.window.destroy()
            self.func()
        elif not self.error:
            self.error = True
            frame = tk.Frame(self.window)
            frame.pack(side=tk.TOP)
            error_l = tk.ttk.Label(frame, text="Wrong password")
            error_l.pack(side=tk.BOTTOM)

    def edit(self):
        self.window = tk.Toplevel(self.root)
        self.window.geometry("400x250")
        self.window.minsize(400, 250)
        self.window.attributes("-zoomed", True)
        self.window.protocol("WM_DELETE_WINDOW", lambda: [self.window.destroy(), self.root.destroy()])
        self.window.bind_class("Text", "<Control-a>", lambda event: event.widget.tag_add("sel", "1.0", "end"))
        self.window.bind_class("Text", "<Control-s>", self.save)
        self.window.bind("<Escape>", lambda event: (self.window.destroy(), self.root.update(), self.root.iconify()))
        st = tk.scrolledtext.ScrolledText(self.window, bg="white", fg="black")
        st.configure(font=("Consolas", 12))
        st.pack(expand=True, fill="both")
        st.insert("1.0", self.data)
        st.focus_set()

    def view(self):
        self.window = tk.Toplevel(self.root)
        self.window.geometry("400x250")
        self.window.minsize(400, 250)
        self.window.attributes("-zoomed", True)
        self.window.protocol("WM_DELETE_WINDOW", lambda: [self.window.destroy(), self.root.destroy()])
        self.window.bind("<Escape>", lambda event: [self.window.destroy(), self.root.update(), self.root.iconify()])
        view_s = tk.ttk.Scrollbar(self.window)
        view_s.pack(fill=tk.Y, side=tk.RIGHT)
        view_t = tk.Text(self.window)
        for i in self.data:
            view_t.insert(tk.END, "Email: {0[email]}\nPassword: {0[password]}\nWebsite: {0[website]}\nComments: {1}\n\n".format(i, "\n".join(i["comments"])))
        view_t.config(state=tk.DISABLED)
        view_t.configure(yscrollcommand=view_s.set, font=("Consolas", 12))
        view_t.pack(side=tk.LEFT, fill=tk.BOTH)

    def save(self, event):
        self.data = event.widget.get("1.0", "end")
        self.gpg.encrypt(self.data.encode(), symmetric="AES256", output="data_json", passphrase=self.pword, recipients=None)
        self.window.destroy()
        self.root.update()
        self.root.iconify()

    def run(self):
        self.root.mainloop()

app = App()
app.run()
