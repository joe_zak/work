<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>SafeTube</title>
		<style>
			body {
				background-color: #000000;
			}
			.header_main {
				position: fixed;
				top: 0;
				left: 0;
				right: 0;
				background-color: rgba(255, 255, 255, 0.2);
			}
		</style>
	</head>
	<body>
		<div class="header_main"></div>
		<div class="content_main"></div>
		<div class="footer_main"></div>
	</body>
</html>