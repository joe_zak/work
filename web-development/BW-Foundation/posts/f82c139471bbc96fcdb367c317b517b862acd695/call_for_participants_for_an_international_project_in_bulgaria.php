<?php
session_start();
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
$post = $data->getPosts("CALL FOR PARTICIPANTS FOR AN INTERNATIONAL PROJECT IN BULGARIA")->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="/css/posts.css">
		<title>BWF | CALL FOR PARTICIPANTS FOR AN INTERNATIONAL PROJECT IN BULGARIA</title>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<div class="content_content">
			<h2><?= $post["title"] ?></h2>
			<span style="font-size: 15px; float: right;"> written by <span style="border-bottom: 2px dotted #000000;"><?= $post["author"] ?></span> <?= $post["date"] ?></span>
			<div style="padding: 10px 40px;">
				<div align="center"><img src="http://bwf.epizy.com/posts/img/5d89125f8ca5faed7796c536d5ffc1b5a717a936.jpg"></div>
				<div align="left"><br></div>
				<div align="left">
					فرصة للسفر و التدريب بلغاريا - منحة من الاتحاد الأوروبي<br> برنامج تدريبي بلغاريا<br> Better World Foundation - Erasmus+ Program Team are happy to have Egypt represented in Bulgaria.<br> Date of Project:8th  - 14th May 2019<br> Location: Plovdiv - Bulgaria<br> Project Description:<p>
					  LEARN TO LIVE TOGETHER, know how to live together with no boundaries
					and all people are the same. The main aim of the project will be to
					build the confidence and common ground between participants from
					different countries and showing the right of living and education is for
					 all. Several methods of formal and non formal education rights will be
					discussed. Workshops and training of collaboration and highlighting
					stereotypes and how to deal with them locally and internationally.
					Participants Social work will help in giving examples and how to use
					your own culture, to raise awareness of and address these issues and the
					 methods in which problems may be resolved. This is an Eramus Plus
					Training Course. </p>
					<p>  40 participants in total from the following 12
					 countries: Bulgaria, Macedonia, Croatia, Greece, Turkey, Romania,
					Azerbaijan, Georgia, Lithuania, Portugal, Jordan and Egypt.</p>
					<p> Who Can Apply?<br> Applicants are expected to be:</p>
					<p> - Age over 22 - 33; <br>
					 - Occupation: youth workers, NGO leaders and staff members, university
					students, teachers, active volunteers, social workers in youth field
					(i.e. working related experience is needed – it is a training Course); <br> - Proven experience in the field of youth work; <br> - Strongly motivated to participate; <br> - Fluent in English; <br> - Able to present at the whole time of the event.</p>
					<p>
					 Participants have to be motivated and have interest and experience in
					the topic of the project and have a good level of communication in
					English (English test will be conducted). Language fluency is not
					required, functioning English is needed though. </p>
					<p> For those who
					have conducted Phase II Evaluations in a previous Project Application -
					Please send your Scores as it is valid for 6 Months and no need to redo
					it again. Scores after 6 months are not valid. (when you receive English
					 Level Stage, just reply with your scores) </p>
					<p> Motivation and need
					for this experience is highly considered. People with no travel
					experience are highly encouraged to apply as well.<br> Our selection will be gender balanced with no discrimination.</p>
					<p>
					 Application Deadline:15 March 2019 (at 8:00PM Cairo Local Time) – First
					 come First served – No phone or email requests will be replied before
					this date. We will only reply to those who get to the next phase</p>
					<p> Fill out the form from here: <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Fgoo.gl%2FFnueRD&amp;h=AT2J5Zfynb4GAAvCYl-YM8Q0pZTlCZw9C9qSPhhqahfuUU2VNWEi-LkZkGo0DAIiWOTFa1yN3H2hOY9f1yfYFvtCZz8Lq4zuhrhAHW1tzHL3ROXolS5KccNQ2ezCajydV7wGE_xB-4yWCvx6Q3QKfZj8sGlfS2NvXTEtX9IHVki_ZHkfG3FESA-vYs89zXecquoqFJt0i325_8rlsPBKT9x8Fe0yZ_Uo2afEFE4VqhXB7cRtpXwr5WqTvp0jF22vCxqJxGcgOa_kj3_hF1r2LOGtmSLcUa0ovpGSwa8YJLzDrdsPKU0hf8qv11_lllrnDMOawkV0X2PLb8h6ro69G7kAlxONp4LkF3snEbbcLGMEoqZtuYVxwh5w4KUCrY5GFyFOOPgcKQCyZ78SpSVAF8xR3xDBgMD0qj2-AgALLuS8PpZqwCK24vagQlEjLW4wF4aKkyavc80ZdhULvmAwuNvz_RKoA5IJhleIYoekILVPc-AH-fGrRyp_H7LmI9vcVb_N0UImYagt-ijs66leOjDinJ4LXc9DaDhWXRNpV-gZy1Y5_T5tDIcaYABBpQ1AwE5uSw1hV5CLgqjMJHSw6rBcWNHrTSVtjjgkfdvMr1p4IDv68EncZc33suJeWPQjW2QTYYwA5Onhhw" data-ft='{"tn":"-U"}' rel="noopener nofollow" data-lynx-mode="async">https://goo.gl/FnueRD</a> </p>
					<p>
					 * Please send your CV / Resume to ERASMUS@BWNGO.ORG with your "First,
					Last name – LiveTogether- Bulgaria” as a title. Applications with no CVs
					 will be ignored.</p>
					<p> Certificate<br> EUROPEAN UNION - ERASMUS+ Youth Pass certificates will be given after the completion of the training.<br> *Project is funded by Erasmus+ Programme<br><a href="https://www.facebook.com/hashtag/erasmus?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBCkd9XpzBpz5jOwt3l6g9n9nQwUQkFVwxpQj9GWFN6eZc350TT6PC8Y8VprAAJsq_6VnbgnRu4XX9B4jhwFYOaXosTABysC2XA040xtNy9mXnO9pzM1QkTGMhInEChbwimVuexA3FNQAnARTVTBNeuNnMO6UZFqnDoD-1mp7E64tI13Gs_Xdw_iXF04LUPJou-YD2evNQ9bKhv5s171gFH8ZVzLtNKLQYF-MUVxHEI-KAL3K_-8hStC9aZ7QaNQbAnP32N8v4JziDetCIn029t17hy2_BpxtE7kxoG01jMeZYqSZyg-TzMYGHo-AsNHAomn__jbrbbsSixYMUoEBQXwg&amp;__tn__=%2ANK-R" data-ft='{"type":104,"tn":"*N"}'><span><span class="_58cl _5afz">#</span><span>Erasmus</span></span></a> <br><a href="https://www.facebook.com/hashtag/betterworldngo?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBCkd9XpzBpz5jOwt3l6g9n9nQwUQkFVwxpQj9GWFN6eZc350TT6PC8Y8VprAAJsq_6VnbgnRu4XX9B4jhwFYOaXosTABysC2XA040xtNy9mXnO9pzM1QkTGMhInEChbwimVuexA3FNQAnARTVTBNeuNnMO6UZFqnDoD-1mp7E64tI13Gs_Xdw_iXF04LUPJou-YD2evNQ9bKhv5s171gFH8ZVzLtNKLQYF-MUVxHEI-KAL3K_-8hStC9aZ7QaNQbAnP32N8v4JziDetCIn029t17hy2_BpxtE7kxoG01jMeZYqSZyg-TzMYGHo-AsNHAomn__jbrbbsSixYMUoEBQXwg&amp;__tn__=%2ANK-R" data-ft='{"type":104,"tn":"*N"}'><span><span class="_58cl _5afz">#</span><span>Betterworldngo</span></span></a><br><a href="https://www.facebook.com/hashtag/international_program?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBCkd9XpzBpz5jOwt3l6g9n9nQwUQkFVwxpQj9GWFN6eZc350TT6PC8Y8VprAAJsq_6VnbgnRu4XX9B4jhwFYOaXosTABysC2XA040xtNy9mXnO9pzM1QkTGMhInEChbwimVuexA3FNQAnARTVTBNeuNnMO6UZFqnDoD-1mp7E64tI13Gs_Xdw_iXF04LUPJou-YD2evNQ9bKhv5s171gFH8ZVzLtNKLQYF-MUVxHEI-KAL3K_-8hStC9aZ7QaNQbAnP32N8v4JziDetCIn029t17hy2_BpxtE7kxoG01jMeZYqSZyg-TzMYGHo-AsNHAomn__jbrbbsSixYMUoEBQXwg&amp;__tn__=%2ANK-R" data-ft='{"type":104,"tn":"*N"}'><span><span class="_58cl _5afz">#</span><span>International_Program</span></span></a> <br><a href="https://www.facebook.com/hashtag/better_world_foundation?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARBCkd9XpzBpz5jOwt3l6g9n9nQwUQkFVwxpQj9GWFN6eZc350TT6PC8Y8VprAAJsq_6VnbgnRu4XX9B4jhwFYOaXosTABysC2XA040xtNy9mXnO9pzM1QkTGMhInEChbwimVuexA3FNQAnARTVTBNeuNnMO6UZFqnDoD-1mp7E64tI13Gs_Xdw_iXF04LUPJou-YD2evNQ9bKhv5s171gFH8ZVzLtNKLQYF-MUVxHEI-KAL3K_-8hStC9aZ7QaNQbAnP32N8v4JziDetCIn029t17hy2_BpxtE7kxoG01jMeZYqSZyg-TzMYGHo-AsNHAomn__jbrbbsSixYMUoEBQXwg&amp;__tn__=%2ANK-R" data-ft='{"type":104,"tn":"*N"}'><span><span class="_58cl _5afz">#</span><span>Better_World_Foundation</span></span></a></p>
				</div>
			</div>
			<?php include $_SERVER["DOCUMENT_ROOT"]."/static/comment.php"; ?>
		</div>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>