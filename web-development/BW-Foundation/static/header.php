<div class="header_header" id="header_header">
	<div class="header_container">
		<a href="/" style="content: url(/img/logo.png);" class="header_logo"></a>
		<span class="header_icon" onclick="nav(this);">
			<center>
				<div class="header_bar1"></div>
				<div class="header_bar2"></div>
				<div class="header_bar3"></div>
			</center>
		</span>
	</div>
	<?php if(!$data->is_logged && !$data->is_member) { ?>
		<a href="/login.php" class="header_pages"><?= $data->locale["pages"]["login"]; ?></a>
		<a href="/subscribe.php" class="header_pages"><?= $data->locale["pages"]["subscribe"]; ?></a>
	<?php } else { ?>
		<a href="/static/functions.php?f=logout" class="header_pages"><?= $data->locale["pages"]["logout"]; ?></a>
		<a href="/dashboard.php" class="header_pages">DASHBOARD</a>
		<a href="/posts/" class="header_pages">CREATE A POST</a>
	<?php } ?>
	<a href="/contact-us.php" class="header_pages"><?= $data->locale["pages"]["contact_us"]; ?></a>
	<a href="/about-us.php" class="header_pages"><?= $data->locale["pages"]["about_us"]; ?></a>
	<div class="header_dropmenu">
		<span class="header_btn"><?= strtoupper($_SESSION["locale"]); ?> <span style="font-size: 10px;">&#9660;</span></span>
		<div class="header_menu">
			<span onclick="set(this.innerHTML);">EN</span>
			<span onclick="set(this.innerHTML);">AR</span>
		</div>
	</div>
</div>
