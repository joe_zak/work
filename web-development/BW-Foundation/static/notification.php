<style type="text/css">
	.notification_notification {
		position: fixed;
		right: 20px;
		top: 20px;
		z-index: 3;
		cursor: default;
	}
	.notification_notification span {
		cursor: default;
	}
	.notification_notification a {
		border: 2px solid #FFFFFF;
		padding: 10px;
		color: white;
	}
	.notification_alert * {
		color: white;
	}
	.notification_alert {
		padding: 20px;
		opacity: 0;
		transition: opacity 0.6s;
		margin-bottom: 15px;
		display: none;
	}
	.notification_alert.notification_danger {
		background-color: #f44336;
	}
	.notification_alert.notification_success {
		background-color: #4CAF50;
	}
	.notification_alert.notification_info {
		background-color: #2196F3;
	}
	.notification_alert.notification_warning {
		background-color: #ff9800;
	}
	@media screen and (max-width: 700px){
		.notification {
			left: 20px;
		}
	}
</style>
<div class="notification_notification">
	<div class="notification_alert notification_danger">
		<span></span>
	</div>
	<div class="notification_alert notification_success">
		<span></span>
	</div>
	<div class="notification_alert notification_info">
		<span></span>
	</div>
	<div class="notification_alert notification_warning">
		<span></span>
	</div>
</div>