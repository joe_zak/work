<?php
session_start();
class Data {

	protected $env;
	public $roles;
	public $locale;
	public $result;
	public $is_admin = false;
	public $can_post = false;
	public $is_member = false;
	public $is_logged = false;
	public $is_mobile = false;

	public function __construct(){
		$this->roles = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/data/data.json"), true);
		$this->env = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/data/env.json"));
		if (!isset($_SESSION["locale"])){
			$_SESSION["locale"] = "EN";
		}
		$this->locale = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/locale/data.json"), true)[strtolower($_SESSION["locale"])];
		$this->is_mobile = preg_match("/(android|kik|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		if (isset($_SESSION["uname"])){
			$this->is_logged = true;
			if (array_key_exists($_SESSION["uname"], $this->roles)){
				$this->is_member = true;
				$this->is_admin = (boolean)$this->roles[$_SESSION["uname"]]["is_admin"];
				$this->can_post = (boolean)$this->roles[$_SESSION["uname"]]["can_post"];
			}
		}
	}

	private function remove_elements($dom, $allowed_tags, $allowed_attrs){
		foreach ($dom->getElementsByTagName("*") as $tag){
			if (!in_array($tag->tagName, $allowed_tags)){
				$tag->parentNode->removeChild($tag);
			} else {
				foreach ($tag->attributes as $attr){
					if (!in_array($attr->nodeName, $allowed_attrs)){
						$tag->removeAttribute($attr->nodeName);
					}
				}
			}
		}
		return $dom;
	}

	private function connect($statement, $database){
		try {
			$conn = new PDO($this->env->connection.":host=".$this->env->host.";dbname=".$database, $this->env->username, $this->env->password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn->query($statement);
		} catch (PDOExeption $e){
			die("An error occurred try again later.");
		}
	}

	public function login(){
		$uname = trim(strtolower($_POST["uname"]));
		$password = htmlspecialchars(trim($_POST["pword"]), ENT_QUOTES | ENT_HTML5);
		$result = $this->connect("SELECT * FROM ".$this->env->databases->users->tables[0]." WHERE uname='$uname' OR email='$uname'", $this->env->databases->users->name);
		if ($result->rowCount() == 1){
			$row = $result->fetch(PDO::FETCH_ASSOC);
			if (password_verify($password, $row["password"])){
				$_SESSION["uname"] = $row["uname"];
				$_SESSION["email"] = $row["email"];
				$_SESSION["verified"] = $row["verified"];
				$this->is_logged = true;
			}
		}
		$conn = null;
	}

	public function getUsers($uname=null){
		if ($uname){
			return $this->connect("SELECT uname, email, verified FROM ".$this->env->databases->users->tables[0]." WHERE uname='$uname'", $this->env->databases->users->name);
		}
		return $this->connect("SELECT uname, email, verified FROM ".$this->env->databases->users->tables[0], $this->env->databases->users->name);
	}

	public function getPosts($title=null){
		if ($title){
			return $this->connect("SELECT * FROM ".$this->env->databases->posts->tables[0]." WHERE title='$title' OR link='$title'", $this->env->databases->posts->name);
		}
		return $this->connect("SELECT * FROM ".$this->env->databases->posts->tables[0], $this->env->databases->posts->name);
	}

	public function addUser(){}

	public function addPost(){
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			if (isset($_POST["textarea"]) && isset($_POST["title"])){
				$title = htmlspecialchars(trim($_POST["title"]));
				$hex = bin2hex($_SESSION["uname"].date("Y-m-d H:i:s"));
				$link = "/posts/".$hex.strtolower(preg_replace("/[^a-zA-Z0-9_\-+]/", "_", trim($_POST["title"]))).".php";
				if (!isset($title) && !$this->is_logged && !isset($link) && !isset($_POST["textarea"]) && empty($_POST["textarea"]) && empty($link) && empty($title)){
					http_response_code(400);
					die("Invalid values");
				}
				try {
					if (mkdir($_SERVER["DOCUMENT_ROOT"]."/post/".$hex, 0700)){
						if ($this->getPosts($title)->rowCount() == 0){
							$html = $this->strip(trim($_POST["textarea"]), "<a><div><span><s><strike><b><strong><i><u><br><img><p><font>", "[align][href][src][size]");
							file_put_contents($_SERVER["DOCUMENT_ROOT"].$link, "<?php session_start(); include \$_SERVER[\"DOCUMENT_ROOT\"].\"/static/functions.php\"; \$post = \$data->getPosts(\"$title\")->fetch(PDO::FETCH_ASSOC); ?><!DOCTYPE html><html><head><?php include \$_SERVER[\"DOCUMENT_ROOT\"].\"/static/styles.php\"; ?><link rel=\"stylesheet\" type=\"text/css\" href=\"/css/posts.css\"><title>BWF | $title</title></head><body><?php include \$_SERVER[\"DOCUMENT_ROOT\"].\"/static/header.php\"; ?><div class=\"content\"><h2><?= \$post[\"title\"] ?></h2><span style=\"font-size: 15px; float: right;\"> written by <span style=\"border-bottom: 2px dotted #000000;\"><?= \$post[\"author\"] ?></span> <?= \$post[\"date\"] ?></span><div style=\"padding: 10px 40px;\">$html</div><?php include \$_SERVER[\"DOCUMENT_ROOT\"].\"/static/comment.php\"; ?></div><?php include \$_SERVER[\"DOCUMENT_ROOT\"].\"/static/footer.php\"; ?></body></html>");
							$this->connect("INSERT INTO ".$this->env->databases->posts->tables[0]." (link, title, author) VALUES ('".$link."', '".$title."', '".$_SESSION["uname"]."')", $this->env->databases->posts->name);
							$this->connect("CREATE TABLE $hex (id INT NOT NULL AUTO_INCREMENT, uname VARCHAR(10) NOT NULL, date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, type INT NOT NULL DEFAULT 0, reply_id INT DEFAULT NULL) PRIMARY KEY (id)", $this->env->databases->posts->name);
							http_response_code(200);
							die("Operation successful <a href=\"$link\">View</a>");
						}
					} else {
						http_response_code(400);
						die("An error occurred");
					}
					http_response_code(400);
					die("Title already exists");
				} catch (Throwable $e){
					http_response_code(400);
					die("An error occurred");
				}
			}
		} else {
			http_response_code(405);
			die("Method not allowed");
		}
	}

	public function delUser(string $uname){
		unset($this->roles[$uname]);
		file_put_contents($_SERVER["DOCUMENT_ROOT"]."/data/data.json", json_encode($this->roles));
		return $this->connect("DELETE FROM ".$this->env->databases->users->tables[0]." WHERE uname='$uname'", $this->env->databases->users->name);
	}

	public function delPost(string $link){
		unlink($_SERVER["DOCUMENT_ROOT"].$link);
		return $this->connect("DELETE FROM ".$this->env->databases->posts->tables[0]." WHERE link='$link'", $this->env->databases->posts->name);
	}

	public function strip($text, $allowed_tags="", $allowed_attrs=""){
		if (!strlen($text)){
			return false;
		}
		preg_match_all("@<(.+?)[\s]*/?[\s]*>@si", trim($allowed_tags), $allowed_tags);
		$allowed_tags = array_unique($allowed_tags[1]);
		preg_match_all("@\[(.+?)[\s]*/?[\s]*\]@si", trim($allowed_attrs), $allowed_attrs);
		$allowed_attrs = array_unique($allowed_attrs[1]);
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		if (LIBXML_VERSION >= 20708){
			if ($dom->loadHTML($text, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDT)){
				$this->remove_elements($dom, $allowed_tags, $allowed_attrs);
			}
			return utf8_decode($dom->saveHTML($dom->documentElement));
		} elseif (LIBXML_VERSION >= 20706){
			$allowed_tags[] = "html";
			$allowed_tags[] = "body";
			$allowed_tags[] = "head";
			if ($dom->loadHTML($text)){
				$this->remove_elements($dom, $allowed_tags, $allowed_attrs);
			}
			return preg_replace("@</*?!*?(?:doctype|body|html|head).*?>@si", "", utf8_decode($dom->saveHTML($dom->documentElement)));
		} else {
			return false;
		}
	}

	public function browse(){
		if ($this->is_logged && ($this->can_post || $this->is_admin)){
			$dir = new DirectoryIterator($_SERVER["DOCUMENT_ROOT"]."/posts/img");
			$return = ["status" => true, "links" => []];
			foreach ($dir as $file){
				if (!$file->isDot() && $file->getFilename() != ".htaccess"){
					$return["links"][] = "/posts/img/".$file->getFilename();
				}
			}
			echo json_encode($return);
		} else {
			echo json_encode(["status" => false]);
		}
	}

	public function upload(){
		header("Content-Type: application/json");
		if ($this->can_post){
			$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
			$translate = array(IMAGETYPE_PNG => "png", IMAGETYPE_JPEG => "jpg", IMAGETYPE_GIF => "gif");
			$filetype = exif_imagetype("php://input");
			if (in_array($filetype, $allowedTypes)){
				$dir = new DirectoryIterator($_SERVER["DOCUMENT_ROOT"]."/posts/img");
				$filename = bin2hex(random_bytes(20)).".".$translate[$filetype];
				foreach ($dir as $file){
					if (!$file->isDot() && $file->getFilename() != ".htaccess"){
						if (hash_file("sha256", $file->getPathname()) == hash_file("sha256", "php://input")){
							if (file_put_contents($_SERVER["DOCUMENT_ROOT"]."/posts/img/".$filename, file_get_contents("php://input"))){
								die(json_encode(["status" => true, "link" => "/posts/img/".$file->getFilename()]));
							} else {
								die(json_encode(["status" => false]));
							}
						}
					}
				}
				if (file_put_contents($_SERVER["DOCUMENT_ROOT"]."/posts/img/".$filename, file_get_contents("php://input"))){
					die(json_encode(["status" => true, "link" => "/posts/img/".$filename]));
				} else {
					die(json_encode(["status" => false]));
				}
			} else {
				die(json_encode(["status" => false]));
			}
		} else {
			die(json_encode(["status" => false]));
		}
	}

	public function logout(){
		session_start();
		session_destroy();
		header("Location: /login.php");
		exit();
	}

	public function remove(){
		try {
			if ($_SERVER["REQUEST_METHOD"] == "POST"){
				if (isset($_SESSION["csrf"]) && $_GET["csrf"] == $_SESSION["csrf"]){
					unset($_SESSION["csrf"]);
					$uname = htmlspecialchars_decode(urldecode($_POST["uname"]));
					$link = htmlspecialchars_decode(urldecode($_POST["link"]));
					if (strtolower($_GET["type"]) == "user" && ($_SESSION["uname"] != $uname || $this->is_admin)){
						if ($this->getUsers($uname)->rowCount() == 1){
							$result = $this->delUser($uname);
							if ($result->rowCount() == 1){
								http_response_code(200);
								die("User deleted successfully");
							} else {
								http_response_code(400);
								die("User deletion unsuccessful");
							}
						} else {
							http_response_code(400);
							die("User not found");
						}
					} elseif (strtolower($_GET["type"]) == "post" && ($_SESSION["uname"] == $uname || $this->is_admin)){
						if ($this->getPosts($link=$link)->rowCount() == 1){
							$result = $this->delPost($link);
							if ($result->rowCount() == 1){
								http_response_code(200);
								die("Post deleted successfully");
							} else {
								http_response_code(400);
								die("Post deletion unsuccessful");
							}
						} else {
							http_response_code(400);
							die("Post not found");
						}
					}
				} else {
					http_response_code(400);
					die("An error occurred");
				}
			} else {
				http_response_code(405);
				die("Method not allowed");
			}
		} catch (Throwable $e){
			http_response_code(400);
			die("An error was thrown");
		}
	}

	public function getComments(){}
}
$data = new Data();
if (isset($_GET["f"]) && method_exists($data, $_GET["f"])){
	call_user_func(array($data, $_GET["f"]));
}
if ($data->is_logged && !$data->is_member){
	$data->logout();
}
?>
