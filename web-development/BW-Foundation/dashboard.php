<?php
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
if (!$data->is_logged){
	header("Location: /login.php");
	exit();
}
unset($_SESSION["csrf"]);
$_SESSION["csrf"] = bin2hex(random_bytes(20));
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>BWF | <?= $_SESSION["uname"]; ?></title>
		<style>
			.content div#create {
				padding-left: 20%;
				padding-right: 20%;
			}
			.content {
				position: relative;
				overflow: auto;
			}
			.content .bar {
				display: flex;
				background-color: #F4F4F4;
				position: absolute;
				left: 0;
				top: 0;
				right: 0;
				margin: 0;
				overflow: hidden;
				height: 60px;
			}
			.bar .tab {
				cursor: pointer;
				padding: 10px 15px;
				height: 100%;
				width: 70px;
			}
			.bar .tab:hover {
				background-color: #DBDBDB;
			}
			.bar .active {
				background-color: #DBDBDB;
			}
			.content .tabContent {
				display: none;
				overflow: auto;
			}
			.tabContent div {
				clear: left;
			}
			label {
				float: left;
			}
			div#profile img {
				float: left;
				height: 250px;
				border: 1px solid #BDBDBD;
				padding: 10px;
				margin-right: 30px;
			}
			input:not([type=radio]) {
				display: block;
				width: 100%;
				margin: 10px 0;
			}
			div#remove {
				overflow-x: auto;
			}
			div#profile, div#remove, div#create {
				overflow: auto;
			}
			@media screen and (max-width: 700px){
				.content div#create {
					padding-left: 5%;
					padding-right: 5%;
				}
				div#profile {
					text-align: center;
				}
				div#profile img {
					float: none;
					margin: 0;
				}
				label {
					float: none;
				}
				.tabContent div {
					display: inline;
				}
			}
		</style>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<div class="content">
			<input type="hidden" name="csrf" value="<?= $_SESSION["csrf"]; ?>">
			<div class="bar">
				<img src="/img/user_default.png" id="profile" class="tab active" title="Profile" alt="Profile" onclick="location.hash = 'profile';">
			<?php if ($data->is_admin){ ?>
				<img src="/img/add.png" id="create" class="tab" title="Add User" alt="Add User" onclick="location.hash = 'create';">
				<img src="/img/remove.png" id="users" class="tab" title="Remove User" alt="Remove User" onclick="location.hash = 'users';">
			<?php } ?>
				<img src="/img/posts.png" id="posts" class="tab" title="Posts" alt="Posts" onclick="location.hash = 'posts';">
			</div>
			<div class="tabContent" id="profile" style="display: block;">
				<img src="/img/user_default.png">
				<p><strong>Username:</strong> <?= $_SESSION["uname"] ?></p>
				<p><strong>E-Mail:</strong> <?= $_SESSION["email"] ?></p>
				<p><strong>Verified:</strong> <?php if($_SESSION["verified"]){ echo "Yes"; } else { echo "No. <a href=\"/verify.php\">verify?</a>"; } ?></p>
			</div>
		<?php if ($data->is_admin){ ?>
			<div class="tabContent" id="create">
				<center>
					<form onsubmit="addUser();">
						<input type="text" placeholder="Username" name="uname" required>
						<input type="email" placeholder="E-Mail" name="email" required>
						<input type="password" placeholder="Password" name="pword" required>
						<label for="is_admin">Admin status: </label>
						<div><input type="radio" name="is_admin" value="1" required>Yes</div>
						<div><input type="radio" name="is_admin" value="0" required checked>No</div><br>
						<label for="can_post">Post ability: </label>
						<div><input type="radio" name="can_post" value="1" required>Yes</div>
						<div><input type="radio" name="can_post" value="0" required checked>No</div><br>
						<input type="submit" class="submit" value="create">
					</form>
				</center>
			</div>
			<div class="tabContent" id="users">
				<span style="color: red;">* this tab is only to remove existing users if you want to delete posts you can delete them individualy</span>
				<table>
					<thead>
						<tr>
							<th>Username</th>
							<th>E-Mail</th>
							<th>Verified</th>
							<th>Can post</th>
							<th>Is admin</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data->getUsers()->fetchAll() as $row){ ?>
						<tr>
							<td><?= $row["uname"] ?></td>
							<td><?= $row["email"] ?></td>
							<td><?php if ($row["verified"]){ echo "Yes"; } else { echo "No"; } ?></td>
							<td><?php if ($data->roles[$row["uname"]]["can_post"]){ echo "Yes"; } else { echo "No"; } ?></td>
							<td><?php if ($data->roles[$row["uname"]]["is_admin"]){ echo "Yes"; } else { echo "No"; } ?></td>
							<td><button class="submit" type="button"<?php if ($_SESSION["uname"] == $row["uname"]){ echo " disabled"; } else { echo " onclick=\"remove(this, 'users', 'user', '".$row["uname"]."');\""; } ?>>Delete</button></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		<?php } ?>
			<div class="tabContent" id="posts">
				<input type="hidden" name="csrf" value="<?= $_SESSION["csrf"]; ?>">
				<table>
					<thead>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					<?php if ($data->getPosts()->rowCount() == 0){ ?>
						<tr>
							<td colspan="5" style="text-align: center; font-style: italic; color: #B0B0B0;">Empty</td>
						</tr>
					<?php } else { foreach ($data->getPosts()->fetchAll() as $row){ ?>
						<tr>
							<td><?= $row["title"] ?></td>
							<td><?= $row["author"] ?></td>
							<td><?= $row["date"] ?></td>
							<td><button class="submit" style="width: 100%;" type="button" onclick="window.location = '<?= $row["link"] ?>';">View</button><br><br><button class="submit" style="width: 100%;" type="button"<?php if ($_SESSION["uname"] == $row["author"] || $data->is_admin){ echo " onclick=\"remove(this, 'posts', 'post', '".urlencode($row["author"])."', '".urlencode($row["link"])."');\""; } else { echo " disabled"; } ?>>Delete</button></td>
						</tr>
					<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
		<script>
			var tabContent = document.getElementsByClassName("tabContent");
			function clear(){
				for (var i = 0; i < document.getElementsByClassName('tab').length; i++){
					document.getElementsByClassName('tab')[i].classList.remove('active');
				}
				for (var i = 0; i < tabContent.length; i++){
					tabContent[i].style.display = "none";
				}
			}
			if (location.hash){
				clear();
				document.querySelector("div"+location.hash).style.display = "block";
				document.querySelector("img"+location.hash).classList.add('active');
			}
			window.onhashchange = function(){
				clear();
				document.querySelector("div"+location.hash).style.display = "block";
				document.querySelector("img"+location.hash).classList.add('active');
			};
			function remove(element, id, type, uname, link=null){
				var csrf = document.getElementsByName("csrf")[0].value;
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						var div = document.getElementsByClassName("success")[0];
						div.getElementsByTagName("span")[0].innerHTML = this.responseText;
						div.style.display = "block";
						div.style.opacity = "1";
						setTimeout(function(){
							div.style.opacity = "0";
							setTimeout(function(){
								div.style.display = "none";
							}, 600);
						}, 5000);
						element.parentElement.parentElement.parentElement.removeChild(element.parentElement.parentElement);
					}
					if (this.status == 400 || this.status == 405){
						var div = document.getElementsByClassName("danger")[0];
						div.getElementsByTagName("span")[0].innerHTML = this.responseText;
						div.style.display = "block";
						div.style.opacity = "1";
						setTimeout(function(){
							div.style.opacity = "0";
							setTimeout(function(){
								div.style.display = "none";
							}, 600);
						}, 5000);
					}
				};
				xhttp.open("POST", "/static/functions.php?f=remove&csrf="+csrf+"&type="+type, true);
				xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				var string = "uname="+uname;
				if (type == "post"){
					string += "&link="+link;
				}
				console.log(string);
				xhttp.send(string);
			}
			function addUser(element){
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){}
				}
				xhttp.open("POST", "/functions.php?f=addUser", true);
				xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhttp.send();
			}
		</script>
	</body>
</html>
