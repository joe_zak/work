<?php
try {
	$env = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/data/env.json"));
    foreach ($env->databases as $db->){
        if ($db == "")
        $conn = new PDO($env->connection.":host=".$env->host.";dbname=".$db->name, $env->username, $env->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = $conn->query("SELECT * FROM ".$env->databases->->tables[0]);
    }
} catch (PDOExeption $e){
	die("Connection failed: ".$e->getMessage());
}
$conn = null;
?>
