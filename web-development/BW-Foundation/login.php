<?php
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
$data = new Data();
if (!$data->is_logged){
	$error = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST"){
		if ($_SESSION["csrf"][0] === $_POST["csrf"]){
			unset($_SESSION["csrf"][0]);
			$data->login();
			if ($data->is_logged){
				header("Location: /dashboard.php");
				exit();
			} else {
				$error = "<span style='color: #FF0000; font-size: 13px;'>* Wrong username or password</span><br>";
			}
		} else {
			$error = "<span style='color: #FF0000; font-size: 13px;'>* Wrong username or password</span><br>";
		}
	}
} elseif (!$data->is_member){
	$data->logout();
} else {
	header("Location: /dashboard.php");
	exit();
}
unset($_SESSION["csrf"]);
$_SESSION["csrf"] = bin2hex(random_bytes(20));
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>BWF | login</title>
		<style type="text/css">
			.content_content {
				width: unset;
				display: inline-block;
				text-align: left;
				padding: 30px 6%;
			}
			input {
				width: 100%;
				margin: 10px 0;
			}
		</style>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<center>
			<div class="content_content">
				<form style="width: 250px;" action="/login.php" method="post">
					<p style="color: #404040; font-size: 20px; margin: 10px 0 30px 0; padding-bottom: 20px; border-bottom: 1px solid #E0E0E0;">Log in</p>
					<?php echo $error; ?>
					<input type="text" name="uname" placeholder="Username or E-Mail" required><br>
					<input type="password" name="pword" placeholder="Password" required><br>
					<input type="hidden" name="csrf" value="<?= $_SESSION["csrf"][0] ?>">
					<input class="submit" type="submit" value="submit">
				</form>
				<p style="color: #404040; font-size: 13px;">Not registered? <a href="/signup.php">Create an account</a></p>
				<a href="/forgotten.php" style="font-size: 13px;">Forgot your password?</a>
			</div>
		</center>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
