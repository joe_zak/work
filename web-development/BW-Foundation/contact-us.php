<?php
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>BWF | contact us</title>
		<style type="text/css">
			p {
				margin: 10px 0;
			}
			#side_img {
				margin: 50px 0;
				float: right;
				width: 200px;
			}
			@media screen and (max-width: 700px) {
				#side_img {
					display: none;
				}
			}
		</style>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<div class="content">
			<img src="/img/getInvolved.jpg" id="side_img">
			<h1>Contact Us</h1>
			<p><strong>Information Desk:</strong> <a href="mailto:Info@bwngo.org">Info@bwngo.org</a></p>
			<p><strong>Public Relations:</strong> <a href="mailto:PR@bwngo.org">PR@bwngo.org</a></p>
			<p><strong>Applying (Volunteer/Intern/Teach):</strong> <a href="mailto:Apply@bwngo.org">Apply@bwngo.org</a></p>
			<p><strong>Address:</strong> <a href="https://www.google.com/maps/place/%D9%85%D8%A4%D8%B3%D8%B3%D8%A9+%D8%B9%D8%A7%D9%84%D9%85+%D8%A3%D9%81%D8%B6%D9%84%E2%80%AD/@30.0889577,31.2986375,20z/" target="_blank">5 Ellan Street, Ibn Sendr Square, Mansheyiet El Bakry</a></p>
			<p><strong>Phone no.:</strong> +20 127 334 4556 - +20 106 338 5837</p>
		</div>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
