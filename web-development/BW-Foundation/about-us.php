<?php
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>BWF | about us</title>
		<style type="text/css">
			.content {
				position: relative;
				overflow: auto;
			}
			.content div:not(:last-child) {
				overflow: auto;
			}
			.content video {
				width: 80%;
			}
			.content .bar {
				display: flex;
				background-color: #F4F4F4;
				position: absolute;
				left: 0;
				top: 0;
				right: 0;
				margin: 0;
				overflow: hidden;
				height: 60px;
			}
			.bar .tab {
				cursor: pointer;
				padding: 10px 15px;
				height: 100%;
				width: 70px;
			}
			.bar .tab:hover {
				background-color: #DBDBDB;
			}
			.bar .active {
				background-color: #DBDBDB;
			}
			.content .tabContent {
				display: none;
				overflow: auto;
			}
			.tabContent div {
				clear: left;
			}
			.side_img {
				float: <?= $data->locale["dir"][1]; ?>;
				margin: 7.5px 15px;
				width: 80px;
				height: 80px;
			}
			#objectives li {
				white-space: normal;
				word-wrap: break-all;
				background-image: url(/img/done.png);
				background-repeat: no-repeat;
				background-position: 3px 3px;
				background-size: 25px 25px;
				padding-left: 30px;
				list-style-type: none;
			}
			@media screen and (max-width: 700px) {
				.content video {
					width: 100%;
				}
				.tabContent div {
					display: inline;
				}
			}
		</style>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<div class="content" dir="<?= $data->locale["dir"][0]; ?>">
			<div class="bar">
				<img src="/img/history.png" class="tab active" title="Our History" alt="Our History" onclick="show(event, 'our_history');">
				<img src="/img/mission.png" class="tab" title="Our Mission" alt="Our Mission" onclick="show(event, 'our_mission');">
				<img src="/img/vision.png" class="tab" title="Our Vision" alt="Our Vision" onclick="show(event, 'our_vision');">
				<img src="/img/objectives.png" class="tab" title="Objectives" alt="Objectives" onclick="show(event, 'objectives');">
			</div>
			<div class="tabContent" id="our_history" style="display: block;">
				<h2><?= $data->locale["our_history"]["title"]; ?></h2>
				<center>
					<video src="/vid/better_world_foundation_story.mp4" preload="none" poster="/vid/better_world_foundation_story.jpg" controls></video>
				</center><br>
				<img src="/img/history.png" class="side_img">
				<span><?= $data->locale["our_history"]["content"]; ?></span>
			</div>
			<div class="tabContent" id="our_mission">
				<h2>Our Mission</h2>
				<img src="/img/mission.png" class="side_img">
				<span>Better World aims to prepare the young people of Egypt for success in the modern professional world through the development technological literacy, life and occupational skills, and by providing ongoing employment support.</span>
			</div>
			<div class="tabContent" id="our_vision">
				<h2>Our Vision</h2>
				<img src="/img/vision.png" class="side_img">
				<span>For Egypt's youth to have a competitive advantage, making them active and equally valuable members in today's global community of professionals.</span>
			</div>
			<div class="tabContent" id="objectives">
				<h2>Objectives</h2>
				<ul>
					<li>Provide accredited training courses in technology, world-languages and other career enhancing skills to public university students and recent graduates.</li><br>
					<li>Teach students to be aware of environmental and social issues directly impacting their communities and providing opportunities for them to reach out to their communities on these issues.</li><br>
					<li>Integrate young people from different backgrounds and cultures through specialized international programs.</li><br>
					<li>Optimize resources by fostering relationships of cooperation between the Better World Foundation, the private and public sectors, and other domestic and international institutions.</li><br>
				</ul>
			</div>
		</div>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
		<script>
			function show(ev, id){
				var tabContent = document.getElementsByClassName("tabContent");
				for (var i = 0; i < tabContent.length; i++){
					tabContent[i].style.display = "none";
				}
				var tabs = document.getElementsByClassName("tab");
				var i;
				for (i = 0; i < tabs.length; i++){
					tabs[i].classList.remove("active");
				}
				document.getElementById(id).style.display = "block";
				ev.currentTarget.classList.add("active");
			}
		</script>
	</body>
</html>
