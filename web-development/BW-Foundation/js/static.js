function static_nav(element){
	document.getElementById("header_header").classList.toggle("header_responsive");
	element.classList.toggle("header_change");
}
function static_set(locale){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if (this.readyState == 4 && this.status == 200){
			location.reload(true);
		}
	};
	xhttp.open("POST", "/locale/set.php", true);
	xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhttp.send("locale="+locale);
}
