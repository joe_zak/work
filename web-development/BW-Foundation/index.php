<?php
include $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
$posts = $data->getPosts();
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>BWF | <?= $data->locale["home"]["title"]; ?></title>
		<style type="text/css">
			.content .container {
				-webkit-touch-callout: none;
				-webkit-user-select: none;
				-khtml-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
				position: relative;
				padding: 0;
				box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.2);
				display: inline-block;
				width: 60%;
				margin-top: 20px;
			}
			.container:hover #player, .container #next_container:hover, .container #prev_container:hover {
				opacity: 1;
			}
			.content img#imgs {
				width: 100%;
				height: 100%;
			}
			.content p {
				margin: 0;
			}
			#next, #prev, #player {
				position: absolute;
				color: #FFFFFF;
				top: 50%;
				transform: translate(-50%, -50%);
			}
			#next {
				right: 20px;
			}
			#prev {
				left: 30px;
			}
			#next_container, #prev_container, #player {
				padding: 3px;
				cursor: pointer;
				opacity: 0;
				transition: opacity ease-in-out 0.5s;
			}
			#next_container, #prev_container {
				position: absolute;
				top: 0;
				margin: 0;
				width: 40%;
				bottom: 0;
			}
			#player {
				width: 50px;
				left: 50%;
			}
			#next_container {
				right: 0;
				background-image: linear-gradient(to left, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0));
			}
			#prev_container {
				left: 0;
				background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0));
			}
			@media screen and (max-width: 700px) {
				.container:hover #next_container, .container:hover #prev_container {
					opacity: 1;
				}
				.content .container {
					width: 100%;
				}
				#player {
					width: 30px;
				}
			}
		</style>
	</head>
	<body>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<div class="content" dir="<?= $data->locale["dir"][0]; ?>">
			<h2><?= $data->locale["home"]["content"]["welcome"]; ?></h2>
			<center>
				<div class="container" dir="ltr">
					<img id="imgs" src="/img/1.jpg">
					<div id="next_container">
						<span id="next">&#x2771;</span>
					</div>
					<img id="player" src="">
					<div id="prev_container">
						<span id="prev">&#x2770;</span>
					</div>
					<p id="count">1 / 13</p>
				</div>
			</center>
			<div>
				<img src="/img/hello.jpg" style="float: <?= $data->locale["dir"][1]; ?>; margin: 7.5px 15px;">
				<p style="font-size: 18px;" class="text">The Better World Foundation NGO was established to help the Egyptian youth advance in the critical areas of academia and technology. Through experienced volunteers, high-quality equipment, and established methods, Better World is changing the professional outlook for hundreds of Egyptians every year by teaching world-languages, international skills, and proactive attitudes. We currently offer services to thousands of public university students and recent graduates.<br><br>With our balanced organizational membership (50% Egyptian and 50% foreign), we offer youth a multi-national and interdisciplinary experience that provides opportunities for fruitful inter-cultural relations.</p>
			</div>
		<?php if ($posts->rowCount() > 0){ ?>
			<div>
				<h2>Posts:</h2>
				<ul>
				<?php foreach ($posts->fetchAll() as $row){ ?>
					<li>
						<a href="<?= $row["link"]; ?>"><span style="font-size: 20px;"><?= $row["title"]; ?></span></a>
						<span style="font-size: 15px;"> written by <?= "<span style='border-bottom: 2px dotted #000000;'>".$row["author"]."</span> ".$row["date"]; ?></span>
					</li>
				<?php } ?>
				</ul>
			</div>
		<?php } ?>
		</div>
		<?php include $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
		<script type="text/javascript">
			var timeout;
			var images = new Array();
			function preload(){
				for (i = 0; i < preload.arguments.length; i++) {
					images[i] = new Image();
					images[i].src = preload.arguments[i];
				}
			}
			preload("/img/1.jpg","/img/2.jpg","/img/3.jpg","/img/4.jpg","/img/5.jpg","/img/6.jpg","/img/7.jpg","/img/8.jpg","/img/9.jpg","/img/10.jpg","/img/11.jpg","/img/12.jpg","/img/13.jpg","/img/play.png");
			function next(){
				var element = document.getElementById("imgs");
				var num = Number(element.src.split("/").splice(-1)[0].split(".")[0]);
				if (num == 13){
					element.src = "/img/1.jpg";
					document.getElementById("count").innerHTML = "1 / 13";
				} else {
					num++;
					element.src = images[num-1].src;
					document.getElementById("count").innerHTML = num.toString()+" / 13";
				}
			}
			function prev(){
				var element = document.getElementById("imgs");
				var num = Number(element.src.split("/").splice(-1)[0].split(".")[0]);
				if (num == 1){
					element.src = "/img/13.jpg";
					document.getElementById("count").innerHTML = "13 / 13";
				} else {
					num--;
					element.src = images[num-1].src;
					document.getElementById("count").innerHTML = num.toString()+" / 13";
				}
			}
			function start(){
				document.getElementById("player").src = "/img/pause.png";
				document.getElementById("player").onclick = function(){ stop(); };
				document.getElementById("next_container").onclick = function(){ stop(); next(); start(); };
				document.getElementById("prev_container").onclick = function(){ stop(); prev(); start(); };
				window.timeout = setInterval("next()", 3000);
			}
			function stop(){
				clearInterval(window.timeout);
				document.getElementById("player").src = "/img/play.png";
				document.getElementById("player").onclick = function(){ start(); };
				document.getElementById("next_container").onclick = function(){ next(); };
				document.getElementById("prev_container").onclick = function(){ prev(); };
			}
			start();
		</script>
	</body>
</html>
