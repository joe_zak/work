package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	fs := http.FileServer(http.Dir("assets/"))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", fs)).Methods("GET")
	r.Handle("/", http.FileServer(http.Dir("."))).Methods("GET")
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		panic(err)
	}
}
