module.exports = {
	purge: {
		enabled: false,
		content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
	},
	darkMode: "media", // false or 'media' or 'class'
	theme: {
		fontFamily: {
			sans: ["sans-serif"],
			display: ["sans-serif"],
			body: ["sans-serif"],
		},
		colors: {
			primary: "#EC1B25",
			bright: "#F14056",
		},
		extend: {},
	},
	variants: {
		extend: {},
	},
	plugins: [],
};
