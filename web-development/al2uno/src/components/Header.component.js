import React from "react";
import logo from "../img/Logo.png";
import { Link } from "react-router-dom";
import { UserIcon } from "@heroicons/react/outline";

class Header extends React.Component {
	render() {
		return (
			<header>
				<div className="h-36 grid">
					<div className="grid-flow-row">
						<Link to="/" className="grid-flow-col">
							<img src={logo} className="h-16 inline-block" alt="logo" />
						</Link>
						<Link className="grid-flow-col">
							<UserIcon className="h-16 inline-block" />
						</Link>
					</div>
					<div className="grid-flow-row h-10">
						<Link>Browse Categories</Link>
						<Link>Home</Link>
						<Link>Shop</Link>
						<Link>On Sale Today</Link>
					</div>
				</div>
			</header>
		);
	}
}

export default Header;
