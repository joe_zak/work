/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import Home from "./screens/Home.screen";

class App extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<Route path="/" component={Home} />
			</BrowserRouter>
		);
	}
}

export default App;
