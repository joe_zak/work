import React from "react";
import { Helmet } from "react-helmet";
import AOS from "aos";
import "aos/dist/aos.css";
import mojs from "@mojs/core";

import Header from "../components/Header.component";

class Home extends React.Component {
	componentDidMount() {
		AOS.init();
		const bigCircle = new mojs.Shape({
			parent: "#test",
			shape: "circle",
			stroke: "#FEF0D5",
			strokeWidth: 18,
			fill: "none",
			left: "90%",
			top: "50%",
			x: { 0: 100 },
			radius: 75,
			isShowStart: true,
			duration: 10000,
			repeat: 999,
			isYoyo: true,
			easing: "ease.inout",
		});
		const smallCircle = new mojs.Shape({
			parent: "#test",
			shape: "circle",
			stroke: "#C4CDFC",
			strokeWidth: 5,
			fill: "none",
			left: "85%",
			top: "60%",
			x: { 10: 90 },
			radius: 50,
			isShowStart: true,
			duration: 8000,
			repeat: 999,
			isYoyo: true,
			easing: "ease.inout",
		});
		bigCircle.play();
		smallCircle.play();
	}
	render() {
		return (
			<div className="dark:bg-gray-800 h-full w-full">
				<Helmet>
					<title>Al2uno - Buying and selling all products in Egypt</title>
				</Helmet>
				<Header />
				<div id="test" className="overflow-x-hidden border relative h-56"></div>
			</div>
		);
	}
}

export default Home;
