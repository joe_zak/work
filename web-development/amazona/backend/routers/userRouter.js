import express from "express";
import data from "../data.js";
import User from "../models/userModel.js";
import bcrypt from "bcryptjs";
import { generateToken } from "../utils.js";

const userRouter = express.Router();

userRouter.get("/seed", async (req, res) => {
	try {
		const createdUsers = await User.insertMany(data.users);
		res.send({ createdUsers });
	} catch (err) {
		res.send({ message: err.message });
	}
});

userRouter.post("/signin", async (req, res) => {
	try {
		console.log(req.body);
		const user = await User.findOne({ email: req.body.email });
		if (user) {
			if (bcrypt.compareSync(req.body.password, user.password)) {
				res.send({
					_id: user._id,
					name: user.name,
					email: user.email,
					isAdmin: user.isAdmin,
					token: generateToken(user),
				});
				return;
			}
		}
		res.status(401).send({ message: "Invalid user email or password" });
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
});

export default userRouter;
