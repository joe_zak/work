import express from "express";
import data from "../data.js";
import Product from "../models/productModel.js";

const productRouter = express.Router();

productRouter.get("/", async (req, res) => {
	try {
		const products = await Product.find({});
		res.send(products);
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
});

productRouter.get("/seed", async (req, res) => {
	// await product.remove({});
	try {
		const createdProducts = await Product.insertMany(data.products);
		res.send({ createdProducts });
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
});

productRouter.get("/:id", async (req, res) => {
	try {
		const product = await Product.findById(req.params.id);
		if (product) {
			res.send(product);
		} else {
			res.status(404).send({ message: "Product not found" });
		}
	} catch (err) {
		res.status(500).send({ message: err.message });
	}
});

export default productRouter;
