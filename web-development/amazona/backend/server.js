import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import dotenv from "dotenv";
import userRouter from "./routers/userRouter.js";
import productRouter from "./routers/productRouter.js";

dotenv.config();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect(
	process.env.DATABASE_URI ||
		"mongodb+srv://admin:Password123@cluster0.evfm9.mongodb.net/amazona?retryWrites=true&w=majority",
	{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
);

const port = process.env.PORT || 5000;

app.use("/api/users", userRouter);

app.use("/api/products", productRouter);

app.get("/", (req, res) => {
	res.send("Server is ready");
});

app.listen(port, () => {
	console.log(`Serve at http://localhost:${port}`);
});
