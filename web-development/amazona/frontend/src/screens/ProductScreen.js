import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { detailsProduct } from "../actions/productActions";
import Rating from "../components/Rating";
import { Alert, Spinner, Row, Col, Card, Button } from "react-bootstrap";

export default function ProductScreen(props) {
	const dispatch = useDispatch();
	const productId = props.match.params.id;
	const [qty, setQty] = useState(1);
	const productDetails = useSelector((state) => state.productDetails);
	const { loading, error, product } = productDetails;
	useEffect(() => {
		dispatch(detailsProduct(productId));
	}, [dispatch, productId]);
	const addToCartHandler = () => {
		props.history.push(`/cart/${productId}?qty=${qty}`);
	};
	return (
		<>
			{loading ? (
				<Spinner animation="border" />
			) : error ? (
				<Alert variant="danger">{error}</Alert>
			) : (
				<>
					<Link to="/">Back to result</Link>
					<Row>
						<Col md="4">
							<img className="large" src={product.image} alt={product.name}></img>
						</Col>
						<Col>
							<ul>
								<li>
									<h1>{product.name}</h1>
								</li>
								<li>
									<Rating
										rating={product.rating}
										numReviews={product.numReviews}
									></Rating>
								</li>
								<li>Price: ${product.price}</li>
								<li>
									Description:
									<p>{product.description}</p>
								</li>
							</ul>
						</Col>
						<Col md="1">
							<Card>
								<Card.Body>
									<ul>
										<li>
											<Row>
												<span>Status:</span>
												{product.quantity > 0 ? (
													<span className="success">In Stock</span>
												) : (
													<span className="danger">Out of Stock</span>
												)}
											</Row>
										</li>
										{product.quantity > 0 && (
											<>
												<li>
													<Row>
														<div>Qty</div>
														<div>
															<select
																value={qty}
																onChange={(e) =>
																	setQty(e.target.value)
																}
															>
																{[
																	...Array(
																		product.quantity
																	).keys(),
																].map((x) => (
																	<option
																		key={x + 1}
																		value={x + 1}
																	>
																		{x + 1}
																	</option>
																))}
															</select>
														</div>
													</Row>
												</li>
												<li>
													<button
														onClick={addToCartHandler}
														className="primary block"
													>
														Add to Cart
													</button>
												</li>
											</>
										)}
									</ul>
								</Card.Body>
							</Card>
						</Col>
					</Row>
				</>
			)}
		</>
	);
}
