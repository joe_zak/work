import React, { useEffect } from "react";
import Product from "../components/Product";
import MessageBox from "../components/MessageBox";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../actions/productActions";
import { Carousel, Spinner } from "react-bootstrap";

export default function HomeScreen() {
	const dispatch = useDispatch();
	const { loading, error, products } = useSelector((state) => state.productList);
	useEffect(() => {
		dispatch(listProducts());
	}, [dispatch]);
	return (
		<div>
			<Carousel>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://via.placeholder.com/800x400/373940/?text=First+slide"
						alt="First slide"
					></img>
					<Carousel.Caption>
						<h1>First slide label</h1>
						<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://via.placeholder.com/800x400/282c34/?text=Second+slide"
						alt="Second slide"
					></img>
					<Carousel.Caption>
						<h1>Second slide label</h1>
						<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
			{loading ? (
				<Spinner animation="border" />
			) : error ? (
				<MessageBox variant="danger">{error}</MessageBox>
			) : (
				<div className="d-flex flex-wrap justify-content-center align-items-center my-5">
					{products.map((product) => (
						<Product key={product._id} product={product}></Product>
					))}
				</div>
			)}
		</div>
	);
}
