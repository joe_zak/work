import { useSelector } from "react-redux";
import { BrowserRouter, Link, Route } from "react-router-dom";
import CartScreen from "./screens/CartScreen";
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import { Badge } from "react-bootstrap";

function App() {
	const cart = useSelector((state) => state.cart);
	const { cartItems } = cart;
	return (
		<BrowserRouter>
			<div className="grid-container">
				<header className="d-flex flex-row justify-content-between align-items-center">
					<div>
						<Link className="brand" to="/">
							<i className="bi bi-chevron-left"></i>
							amazona
						</Link>
					</div>
					<div className="">
						<Link to="/cart">
							<i className="bi bi-cart4"></i>
							<Badge pill variant="info">
								{cartItems.length}
							</Badge>
						</Link>
						<Link to="/signin">Sign In</Link>
					</div>
				</header>
				<main>
					<Route path="/cart/:id?" component={CartScreen}></Route>
					<Route path="/product/:id" component={ProductScreen}></Route>
					<Route path="/" component={HomeScreen} exact></Route>
				</main>
				<footer className="d-flex flex-row justify-content-center align-items-center">
					All rights reserved
				</footer>
			</div>
		</BrowserRouter>
	);
}

export default App;
