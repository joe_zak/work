import React from "react";

export default function Rating(props) {
	const { rating, numReviews } = props;
	let n = Math.floor(rating);
	let d = Boolean(rating % 1);
	let values = [];
	for (let i = 1; i <= 5; i++) {
		if (i <= n) {
			values.push("bi bi-star-fill");
		} else if (d && i - n === 1) {
			values.push("bi bi-star-half");
		} else {
			values.push("bi bi-star");
		}
	}
	return (
		<div className="rating">
			{values.map((value) => (
				<i className={value}></i>
			))}
			<span>{`${numReviews} reviews`}</span>
		</div>
	);
}
