import React from "react";
import { Link } from "react-router-dom";
import Rating from "./Rating";
import { Card } from "react-bootstrap";

export default function Product(props) {
	const { product } = props;
	return (
		<Card key={product._id} className="m-3">
			<Link to={`/product/${product._id}`}>
				<Card.Img className="medium" src={product.image} alt={product.name} />
			</Link>
			<Card.Body>
				<Link to={`/product/${product._id}`}>
					<Card.Title>{product.name}</Card.Title>
				</Link>
				<Card.Text>
					<Rating rating={product.rating} numReviews={product.numReviews}></Rating>
					<div className="price">${product.price}</div>
				</Card.Text>
			</Card.Body>
		</Card>
	);
}
