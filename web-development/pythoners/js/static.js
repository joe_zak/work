function static_nav(element){
	document.getElementById("header_header").classList.toggle("header_responsive");
	element.classList.toggle("header_change");
}

function static_getPostsData(element){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if (this.readyState == 4 && this.status == 200){
			let response = JSON.parse(this.responseText);
			if (response.num > 0){
				for (i = 0; i < response.num; i++){
					element.insertAdjacentHTML("beforeend", `
						<div class="post">
							<a href="/posts/?id=${response.posts[i].id}"><span>${response.posts[i].title}</span></a><br>
							<div class="info author"><i class="fa fa-pen"></i> ${response.posts[i].author}</div>
							<div class="info date"><i class="fa fa-clock"></i> ${response.posts[i].date}</div>
						</div>
					`);
				}
				if (response.num === limit){
					offset += limit;
					element.appendChild(`
						<button class="static_submit">تحميل المزيد</button>
					`);
				}
			} else {
				element.appendChild(`
					<span style="color: #B0B0B0; display: block; font-style: italic; text-align: center; font-size: 18px;">
						فارغ
					</span>
				`);
			}
		}
	}
	xhttp.open("GET", `/static/functions.php?f=getPostsData&limit=${limit}&offset=${offset}`);
	xhttp.send();
}