<?php
require $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
if (!$data->is_logged){
	header("Location: /login.php");
	exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>Pythoners | <?= $_SESSION["uname"]; ?></title>
		<style>
			.content_content div#create {
				padding-left: 20%;
				padding-right: 20%;
			}
			.content_content {
				direction: rtl;
				position: relative;
				overflow: auto;
			}
			.content_content .bar {
				display: flex;
				background-color: #F4F4F4;
				position: absolute;
				left: 0;
				top: 0;
				right: 0;
				margin: 0;
				overflow: hidden;
				height: 60px;
			}
			.bar .tab {
				text-align: center;
				font-size: 40px;
				cursor: pointer;
				padding: 10px 15px;
				height: 100%;
				width: 70px;
			}
			.bar .tab:hover {
				background-color: #DBDBDB;
			}
			.bar .active {
				background-color: #DBDBDB;
			}
			.content_content .tabContent {
				display: none;
				overflow: auto;
			}
			.tabContent div {
				clear: left;
			}
			div#profile img {
				float: right;
				height: 200px;
				border: 1px solid #BDBDBD;
				padding: 10px;
				margin-left: 30px;
				background-color: #f4f4f4;
				border-radius: 50%;
			}
			input:not([type=checkbox]):nth-child(n+3) {
				display: block;
				width: 100%;
				margin: 10px 0;
			}
			div#remove {
				overflow-x: auto;
			}
			div#profile, div#remove, div#create {
				overflow: auto;
			}
			input[name=fname], input[name=lname] {
				width: 49%;
			}
			input[name=lname] {
				float: left;
			}
			tr td, tr th {
				text-align: right;
			}
			div#posts table .static_submit {
				width: 100px;
				display: block;
				margin: 5px auto;
			}
			.empty {
				text-align: center;
				font-style: italic;
				color: #B0B0B0;
			}
			@media screen and (max-width: 700px){
				.content_content div#create {
					padding-left: 5%;
					padding-right: 5%;
				}
				div#profile {
					text-align: center;
				}
				div#profile img {
					float: none;
					margin: 0;
				}
				label {
					float: none;
				}
				.tabContent div {
					display: inline;
				}
			}
		</style>
	</head>
	<body>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<div class="content_content">
			<div class="bar">
				<i class="fa fa-user tab active" onclick="location.hash = 'profile';" id="profile"></i>
			<?php if ($data->is_admin){ ?>
				<i class="fa fa-user-plus tab" onclick="location.hash = 'create';" id="create"></i>
				<i class="fa fa-user-minus tab" onclick="location.hash = 'users';" id="users"></i>
			<?php } ?>
				<i class="fa fa-edit tab" onclick="location.hash = 'posts';" id="posts"></i>
			</div>
			<div class="tabContent" id="profile" style="display: block;">
				<img src="/img/user_default.png">
				<p>الاسم: <?= $_SESSION["name"] ?></p>
				<p>اسم المستخدم: <?= $_SESSION["uname"] ?></p>
				<p>البريد الإلكتروني: <?= $_SESSION["email"] ?></p>
				<p>التحقق: <?php if ($_SESSION["verified"]){ echo "نعم"; } else { echo "لا. <a href=\"/verify.php\">تحقق؟</a>"; } ?></p>
			</div>
		<?php if ($data->is_admin){ ?>
			<div class="tabContent" id="create">
				<form onsubmit="event.preventDefault(); addUser(this);">
					<input type="text" placeholder="الاسم الاول" name="fname" required><input type="text" placeholder="الاسم الاخير" name="lname" required>
					<input type="text" placeholder="اسم المستخدم" name="uname" required>
					<input type="email" placeholder="البريد الإلكتروني" name="email" required>
					<input type="password" placeholder="كلمة السر" name="pword" required><br>
					<input type="checkbox" name="is_admin" value="1"> صلاحيات المدير<br>
					<input type="checkbox" name="can_post" value="1"> صلاحيات النشر<br><br>
					<input type="submit" class="static_submit" value="ارسال">
				</form>
			</div>
			<div class="tabContent" id="users">
				<table>
					<thead>
						<tr>
							<th>الاسم</th>
							<th>اسم المستخدم</th>
							<th>البريد الإلكتروني</th>
							<th>التحقق</th>
							<th>صلاحيات النشر</th>
							<th>صلاحيات المدير</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data->getUsers()->fetchAll() as $row){ ?>
						<tr>
							<td><?= $row["fname"]." ".$row["lname"] ?></td>
							<td><?= $row["uname"] ?></td>
							<td><?= $row["email"] ?></td>
							<td><?php if ($row["verified"]){ echo "نعم"; } else { echo "لا"; } ?></td>
							<td><?php if ($data->roles[$row["uname"]]["can_post"]){ echo "نعم"; } else { echo "لا"; } ?></td>
							<td><?php if ($data->roles[$row["uname"]]["is_admin"]){ echo "نعم"; } else { echo "لا"; } ?></td>
							<td><button class="static_submit" type="button"<?php if ($_SESSION["uname"] == $row["uname"]){ echo " disabled"; } else { echo " onclick=\"delUser(this.parentElement.parentElement, '".$row["uname"]."');\""; } ?>>حذف</button></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		<?php } ?>
			<div class="tabContent" id="posts">
				<input type="hidden" name="csrf" value="<?= $_SESSION["csrf"]; ?>">
				<table>
					<thead>
						<tr>
							<th>العنوان</th>
							<th>الكاتب</th>
							<th>التاريخ</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php if ($data->getPosts()->rowCount() == 0){ ?>
						<tr>
							<td colspan="5" class="empty">فارغ</td>
						</tr>
					<?php } else { foreach ($data->getPosts()->fetchAll() as $row){ ?>
						<tr>
							<td style="white-space: nowrap;"><?= $row["title"] ?></td>
							<td style="white-space: nowrap;"><?= $row["author"] ?></td>
							<td><?= $row["date"] ?></td>
							<td>
								<button class="static_submit" type="button" onclick="window.location='<?= "/posts/?id=".$row["id"] ?>';">المعاينة</button>
								<button class="static_submit" type="button"<?php if ($_SESSION["uname"] == $row["author"] || $data->is_admin){ echo " onclick=\"delPost(this.parentElement.parentElement, '".$row["id"]."');\""; } else { echo " disabled"; } ?>>حذف</button>
							</td>
						</tr>
					<?php }} ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
		<script>
			var tabContent = document.getElementsByClassName("tabContent");
			function clear(){
				for (var i = 0; i < document.getElementsByClassName('tab').length; i++){
					document.getElementsByClassName('tab')[i].classList.remove('active');
				}
				for (var i = 0; i < tabContent.length; i++){
					tabContent[i].style.display = "none";
				}
			}
			if (location.hash){
				clear();
				document.querySelector("div"+location.hash).style.display = "block";
				document.querySelector("i"+location.hash).classList.add('active');
			}
			window.onhashchange = function(){
				clear();
				document.querySelector("div"+location.hash).style.display = "block";
				document.querySelector("i"+location.hash).classList.add('active');
			};
			function delUser(element, uname){
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						let response = JSON.parse(this.responseText);
						if (response.status){
							notification_show("success", response.msg);
							var parent = element.parentElement;
							parent.removeChild(element);
							if (parent.childElementCount === 0){
								let tr = document.createElement("tr");
								let td = document.createElement("td");
								td.innerHTML = "فارغ";
								td.setAttribute("colspan", "5");
								td.setAttribute("class", "empty");
								tr.appendChild(td);
								parent.appendChild(tr);
							}
						} else {
							notification_show("failure", response.msg);
						}
					}
				};
				xhttp.open("POST", "/static/functions.php?f=delUser", true);
				xhttp.send("uname="+uname);
			}
			function delPost(element, id){
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						let response = JSON.parse(this.responseText);
						if (response.status){
							notification_show("success", response.msg);
							var parent = element.parentElement;
							parent.removeChild(element);
							if (parent.childElementCount === 0){
								let tr = document.createElement("tr");
								let td = document.createElement("td");
								td.innerHTML = "فارغ";
								td.setAttribute("colspan", "5");
								td.setAttribute("class", "empty");
								tr.appendChild(td);
								parent.appendChild(tr);
							}
						} else {
							notification_show("failure", response.msg);
						}
					}
				};
				xhttp.open("POST", "/static/functions.php?f=delPost", true);
				xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhttp.send("id="+id);
			}
			function addUser(element){
				var form = new FormData(element);
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						let response = JSON.parse(this.responseText);
						if (response.status){
							notification_show("success", response.msg);
							window.location.href = "/dashboard.php#users";
						} else {
							notification_show("failure", response.msg);
						}
					}
				};
				xhttp.open("POST", "/static/functions.php?f=addUser", true);
				xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhttp.send(form);
			}
		</script>
	</body>
</html>
