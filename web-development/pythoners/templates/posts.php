<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<link rel="stylesheet" type="text/css" href="/css/posts.css">
		<title>Pythoners | <?= $post["title"] ?></title>
	</head>
	<body>
		<script type="text/javascript">var id = "<?= $post["id"] ?>";</script>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<div class="content_content">
			<center>
				<h1><?= $post["title"] ?></h1>
				<span style="font-size: 15px; display: inline-block; margin-left: 10px;"><i class="fa fa-pen"></i> <?= $post["author"] ?></span>
				<span style="font-size: 15px; display: inline-block; margin-right: 10px;"><i class="fa fa-clock"></i> <?= date("M. d, Y g:i A", strtotime($post["date"])) ?></span>
			</center>
			<div style="padding: 40px; background-color: #FFFFFF; border: 1px solid #AAAAAA;">
				<div style="margin-bottom: 40px;"><?= file_get_contents($post["link"]) ?></div>
				<?php require $_SERVER["DOCUMENT_ROOT"]."/static/comment.php"; ?>
			</div>
		</div>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
