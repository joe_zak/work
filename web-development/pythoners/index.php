<?php
require $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
$posts = $data->getPosts();
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>Pythoners | Home</title>
		<style type="text/css">
			.date {
				margin-right: 10px;
			}
			.author {
				margin-left: 10px;
			}
			.info {
				font-size: 15px;
				display: inline-block;
			}
			.container {
				background-color: #FFFFFF;
				border: 1px solid #DBDBDB;
				padding: 20px;
			}
			.container h2 {
				font-weight: normal;
			}
			.post {
                margin: 30px auto;
                width: 90%;
                box-shadow: 10px 10px 10px #B0B0B0;
				border: 1px solid #B0B0B0;
				padding: 20px 25px;
            }
			.post span {
				font-size: 20px;
			}
			@media screen and (max-width: 700px) {
				.post {
					width: 100%;
				}
			}
		</style>
	</head>
	<body>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<div class="content_content" dir="rtl">
			<h2>مرحبا بكم في بايثونرس</h2>
			<div class="container" onload="getPostsData(this);">
				<h2>المنشورات :</h2>
				<script>
					var offset = 0;
					var limit = 10;
					var element = document.getElementsByClassName("container")[0];
					static_getPostsData(element);
				</script>
			</div>
		</div>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
