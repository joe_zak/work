<?php
require $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
if ($data->is_logged){
	header("Location: /dashboard.php");
	exit();
}
// unset($_SESSION["csrf"]);
// $_SESSION["csrf"] = bin2hex(random_bytes(20));
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>Pythoners | login</title>
		<style type="text/css">
			.content_content {
				width: unset;
				display: inline-block;
				padding: 30px 6%;
			}
			input {
				width: 100%;
				margin: 10px 0;
			}
		</style>
	</head>
	<body>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<center>
			<div class="content_content">
				<form style="width: 250px;" method="post" onsubmit="event.preventDefault(); login(this);">
					<p style="text-align: right; color: #404040; font-size: 20px; margin: 10px 0 30px 0; padding-bottom: 20px; border-bottom: 1px solid #d4d4d4;">تسجيل الدخول</p>
					<input type="text" name="uname" placeholder="اسم المستخدم او البريد الالكتروني" required><br>
					<input type="password" name="pword" placeholder="كلمة السر" required><br>
					<!-- <input type="hidden" name="csrf" value="<?= $_SESSION["csrf"][0] ?>"> -->
					<input class="static_submit" type="submit" value="دخول">
				</form>
				<p style="color: #404040; font-size: 13px;">غير مسجل؟ <a href="/signup.php">سجل حساب جديد</a></p>
				<a href="/forgotten.php" style="font-size: 13px;">نسيت كلمة السر؟</a>
			</div>
		</center>
		<script>
			function login(form){
				var xhttp = new XMLHttpRequest();
				form = new FormData(form);
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						let response = JSON.parse(this.responseText);
						if (response.status){
							window.location.href = "/dashboard.php";
						} else {
							notification_show("failure", response.msg);
						}
					}
				};
				xhttp.open("POST", "/static/functions.php?f=login", true);
				xhttp.send(form);
			}
		</script>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
