<?php
require $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>Pythoners | contact us</title>
		<style type="text/css">
			p {
				margin: 10px 0;
			}
			.content_content {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<div class="content_content" dir="rtl">
			<h2>تواصل معنا</h2><br>
			<p><i class="fab fa-facebook"></i> <strong>فيسبوك :</strong> <a href="https://www.facebook.com/Pythoners-723752094501093/" target="_blank">Pythoners <i class="fa fa-external-link-alt"></i></a></p>
            <p><i class="fab fa-youtube"></i> <strong>يوتيوب :</strong> <a href="https://www.youtube.com/channel/UCzNqP6CBtN7-DVtYORvhgiA" target="_blank">Pythoners <i class="fa fa-external-link-alt"></i></a></p>
		</div>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
	</body>
</html>
