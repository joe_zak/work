<style type="text/css">
	.notification_notification {
		position: fixed;
		z-index: 3;
		cursor: default;
	}
	.notification_notification span {
		cursor: default;
	}
	.notification_notification a {
		border: 2px solid #FFFFFF;
		padding: 10px;
		color: #FFFFFF;
	}
	.notification_alert * {
		color: #FFFFFF;
	}
	.notification_alert {
		padding: 20px;
		opacity: 0;
		transition: opacity 0.6s;
		margin-bottom: 15px;
		display: none;
	}
	.notification_failure {
		background-color: #f44336;
	}
	.notification_success {
		background-color: #4CAF50;
	}
	.notification_info {
		background-color: #2196F3;
	}
	@media screen and (max-width: 700px){
		.notification_notification {
			left: 20px;
		}
	}
</style>
<div class="notification_notification">
	<div class="notification_alert notification_failure">
		<span></span>
	</div>
	<div class="notification_alert notification_success">
		<span></span>
	</div>
	<div class="notification_alert notification_info">
		<span></span>
	</div>
</div>
<script>
	function notification_show(type, text){
		var div = document.getElementsByClassName("notification_"+type)[0];
		div.getElementsByTagName("span")[0].innerHTML = text;
		div.style.display = "block";
		div.style.opacity = "1";
		setTimeout(function(){
			div.style.opacity = "0";
			setTimeout(function(){
				div.style.display = "none";
			}, 600);
		}, 5000);
	}
</script>
