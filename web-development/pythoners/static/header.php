<link rel="shortcut icon" type="image/png" href="favicon.png">
<link rel="shortcut icon" type="image/png" href="favicon.png">
<link rel="apple-touch-icon" href="favicon.png">
<style type="text/css">
	.header_header {
		z-index: 2;
		background: #373737;
		background: linear-gradient(#373737 85%, #808080);
		position: fixed;
		top: 0;
		left: 0;
		right: 0;
		padding: 0;
		border-bottom: 1px solid #808080;
	}
	.header_header .header_container {
		position: relative;
	}
	.header_header .header_logo {
		height: 50px;
		margin: 2px;
		float: left;
		display: block;
		-webkit-filter: drop-shadow(0 0 10px rgb(150, 150, 150));
		filter: drop-shadow(0 0 10px rgb(150, 150, 150));
	}
	.header_header .header_logo:hover {
		-webkit-filter: drop-shadow(0 0 10px rgb(255, 255, 255));
		filter: drop-shadow(0 0 10px rgb(255, 255, 255));
	}
	.header_header .header_icon {
		display: none;
		padding: 5px;
		float: right;
		cursor: pointer;
	}
	.header_icon div {
		width: 35px;
		height: 5px;
		background-color: #808080;
		margin: 6px 0;
		transition: 0.4s;
		border-radius: 20px;
	}
	.header_change .header_bar1 {
		-webkit-transform: rotate(-45deg) translate(-8px, 7.5px);
		transform: rotate(-45deg) translate(-8px, 7.5px);
	}
	.header_change .header_bar2 {
		opacity: 0;
	}
	.header_change .header_bar3 {
		-webkit-transform: rotate(45deg) translate(-8px, -8px);
		transform: rotate(45deg) translate(-8px, -8px);
	}
	.header_header .header_pages {
		float: right;
		padding: 20px;
		text-decoration: none;
		color: #9B9B9B;
		text-align: center;
		border-bottom: 3px solid transparent;
		cursor: pointer;
	}
	.header_header .header_pages:hover, .header_header .header_pages:active, .header_header .header_active {
		color : #ffffff;
		border-bottom: 3px solid #0ac0fc;
	}
	@media screen and (max-width: 700px) {
		.header_header .header_icon {
			display: block;
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			width: 50px;
			text-align: center;
		}
		.header_header .header_pages {
			border-bottom: none;
			display: none;
		}
		.header_responsive {
			bottom: 0;
			overflow-y: auto;
			background: rgba(55, 55, 55, 0.8);
		}
		.header_header.header_responsive .header_logo, .header_header.header_responsive .header_logo:hover {
			-webkit-filter: initial;
			filter: initial;
		}
		.header_header.header_responsive a {
			text-align: right;
			display: block;
			float: none;
		}
		.header_header .header_pages:active {
			border-bottom: none;
		}
		.header_header.header_responsive .header_pages:hover, .header_header.header_responsive .header_active {
			border-bottom: none;
			border-right: 3px solid #0ac0fc;
		}
	}
</style>
<div class="header_header" id="header_header">
	<div class="header_container">
		<a href="/" style="content: url(/img/w_logo.png);" class="header_logo"></a>
		<span class="header_icon" onclick="header_nav(this);">
			<center>
				<div class="header_bar1"></div>
				<div class="header_bar2"></div>
				<div class="header_bar3"></div>
			</center>
		</span>
	</div>
	<div id="header_pages">
		<?php if(!$data->is_logged) { ?>
			<a href="/login.php" class="header_pages">تسجيل الدخول <i class="fa fa-sign-in-alt"></i></a>
		<?php } else { ?>
			<a href="/static/functions.php?f=logout" class="header_pages">تسجيل الخروج <i class="fa fa-sign-out-alt"></i></a>
			<a href="/dashboard.php" class="header_pages">لوحة التحكم <i class="fa fa-tachometer-alt"></i></a>
			<a href="/posts/" class="header_pages">إنشاء منشور <i class="fa fa-share-square"></i></a>
		<?php } ?>
		<a href="/contact-us.php" class="header_pages">تواصل معنا <i class="fa fa-envelope"></i></a>
		<a href="/about-us.php" class="header_pages">عنا <i class="fa fa-address-card"></i></a>
	</div>
</div>
<script>
	function header_nav(element){
		document.getElementById("header_header").classList.toggle("header_responsive");
		element.classList.toggle("header_change");
	}
</script>
