<style type="text/css">
	.comment_section {
		border: 1px solid #AAAAAA;
		background-color: #F0F0F0;
		padding: 20px 20px;
		overflow: auto;
		box-sizing: border-box;
	}
	.comment_section button {
		margin: 5px 0;
	}
	.comment_section input[type=submit] {
		float: right;
	}
	.comment_section textarea {
		resize: none;
		height: 150px;
	}
	.comment_section input[type=text], .comment_section textarea {
		margin-bottom: 5px;
		width: 100%;
	}
	#comment_comments {
		background-color: #FFFFFF;
		border: 1px solid #DBDBDB;
		padding: 20px;
	}
</style>
<div class="comment_section">
	<form>
		<textarea placeholder="التعليق" name="comment" required></textarea>
		<input type="text" name="email" placeholder="البريد الالكتروني" required>
		<input type="submit" class="static_submit" value="تعليق">
	</form><br><br>
	التعليقات :<br>
	<div id="comment_comments">
		<button onclick="getComments();" class="static_submit"><i class="fa fa-sync"></i></button>
	</div>
</div>
<script type="text/javascript">
	function getComments(){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200){}
		};
		xhttp.open("GET", "/static/functions.php?f=getComments&post="+id, true);
		xhttp.send();
	}
</script>
