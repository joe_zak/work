<style type="text/css">
	.footer_footer {
		background-color: #373737;
		margin: 0;
		text-align: center;
		padding: 5px 0;
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		color: #808080;
	}
	.footer_footer a {
		color: #808080;
	}
</style>
<div class="footer_footer">
    <p><?= "حقوق النشر &#169; ".date("Y")." بايثونرس | تصميم و تطوير يوسف زكريا" ?></p>
</div>
