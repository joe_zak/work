<?php
session_start();
if (!isset($_SESSION["is_mobile"])){
	$_SESSION["is_mobile"] = preg_match("/(android|kik|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
class Data {
	protected $host;
	protected $username;
	protected $password;
	protected $db_users;
	protected $db_posts;
	protected $connection;
	public $roles;
	public $is_admin = false;
	public $can_post = false;
	public $is_logged = false;

	public function __construct(){
		$this->roles = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/data/data.json"), true);
		$env = json_decode(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/data/env.json"));
		$this->host = $env->host;
		$this->username = $env->username;
		$this->password = $env->password;
		$this->db_users = $env->databases->users;
		$this->db_posts = $env->databases->posts;
		$this->connection = $env->connection;
		if (isset($_SESSION["uname"])){
			$this->is_logged = true;
			if (array_key_exists($_SESSION["uname"], $this->roles)){
				$this->is_admin = $this->roles[$_SESSION["uname"]]["is_admin"];
				$this->can_post = $this->roles[$_SESSION["uname"]]["can_post"];
			} else {
				$this->logout();
			}
		}
	}

	private function remove_elements($dom, $allowed_tags, $allowed_attrs, $allowed_properties){
		do {
			$old_dom = $dom->saveHTML($dom->documentElement);
			foreach ($dom->getElementsByTagName("*") as $tag){
				if (!in_array($tag->tagName, $allowed_tags)){
					$tag->parentNode->removeChild($tag);
				}
				foreach ($tag->attributes as $attr){
					if ($attr->nodeName === "style"){
						$styles = explode(";", $tag->getAttribute($attr->nodeName));
						$allowed = [];
						foreach ($styles as $style){
							$property = trim(explode(":", $style)[0]);
							if ($property){
								if (in_array($property, $allowed_properties)){
									$allowed[] = $style;
								}
							}
							continue;
						}
						if ($allowed){
							$tag->setAttribute($attr->nodeName, implode(";", $allowed));
						} else {
							$tag->removeAttribute($attr->nodeName);
						}
					} else {
						if (!in_array($attr->nodeName, $allowed_attrs)){
							$tag->removeAttribute($attr->nodeName);
						}
					}
				}
			}
		} while ($old_dom !== $dom->saveHTML($dom->documentElement));
		return $dom;
	}

	private function connect($statement, $database){
		try {
			$conn = new PDO($this->connection.":host=".$this->host.";dbname=".$database, $this->username, $this->password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn->query($statement);
		} catch (PDOExeption $e){
			header("Content-Type: application/json");
			die(json_encode(["status" => 0, "msg" => "An error occurred try again later."]));
		}
	}

	private function array_keys_exist(array $keys, array $arr){
		return !array_diff($keys, array_keys($arr));
	}

	private function check_str(array $keys, array $arr){
		foreach ($keys as $key){
			if (!is_string($arr[$key])){
				return false;
			}
		}
		return true;
	}

	public function login(){
		if ($_SERVER["REQUEST_METHOD"] === "POST"){
			if (is_string($_POST["uname"]) && is_string($_POST["pword"])){
				$uname = htmlspecialchars(trim(strtolower($_POST["uname"])), ENT_QUOTES | ENT_HTML5);
				$pword = htmlspecialchars(trim($_POST["pword"]), ENT_QUOTES | ENT_HTML5);
				$result = $this->connect("SELECT * FROM ".$this->db_users->table." WHERE uname='$uname' OR email='$uname'", $this->db_users->name);
				if ($result->rowCount() === 1){
					$row = $result->fetch(PDO::FETCH_ASSOC);
					if (password_verify($pword, $row["pword"])){
						$_SESSION["name"] = $row["fname"]." ".$row["lname"];
						$_SESSION["uname"] = $row["uname"];
						$_SESSION["email"] = $row["email"];
						$_SESSION["verified"] = $row["verified"];
						$this->is_logged = true;
						die(json_encode(["status" => 1]));
					} else {
						die(json_encode(["status" => 0, "msg" => "Wrong username or password"]));
					}
				} else {
					die(json_encode(["status" => 0, "msg" => "Wrong username or password"]));
				}
				$conn = null;
			} else {
				die(json_encode(["status" => 0, "msg" => "Wrong username or password"]));
			}
		} else {
			die(json_encode(["status" => 0, "msg" => "Method not allowed"]));
		}
	}

	public function getUsers($uname=null){
		if ($uname){
			return $this->connect("SELECT fname, lname, uname, email, verified FROM ".$this->db_users->table." WHERE uname='$uname'", $this->db_users->name);
		}
		return $this->connect("SELECT fname, lname, uname, email, verified FROM ".$this->db_users->table, $this->db_users->name);
	}

	public function getPosts(array $args=["select" => ["*"]]){
		if (!isset($args["select"])){
			$args["select"] = ["*"];
		}
		$query = sprintf("SELECT %s FROM %s", join(", ", $args["select"]), $this->db_posts->table);
		if (isset($args["id"])){
			$query = sprintf("%s WHERE id='%s'", $query, $args["id"]);
		}
		if (isset($args["limit"])){
			$query = sprintf("%s LIMIT %s OFFSET %s", $query, $args["limit"][0], $args["limit"][1]);
		}
		return $this->connect($query, $this->db_posts->name);
	}

	public function getPostsData(){
		if ($_SERVER["REQUEST_METHOD"] === "GET" && $this->array_keys_exist(["limit", "offset"], $_GET) && $this->check_str(["limit", "offset"], $_GET)){
			$args = array();
			$args["limit"] = [$_GET["limit"], $_GET["offset"]];
			$args["select"] = ["id", "author", "date", "title"];
			$posts = $this->getPosts($args);
			$num = $posts->rowCount();
			$posts = $posts->fetchAll(PDO::FETCH_ASSOC);
			die(json_encode(["status" => 1, "posts" => $posts, "num" => $num]));
		} else {
			die(json_encode(["status" => 0, "msg" => "Invalid request"]));
		}
	}

	public function addUser(){
		if ($_SERVER["REQUEST_METHOD"] === "POST"){
			$keys = ["fname", "lname", "uname", "pword", "email", "is_admin", "can_post"];
			if ($this->array_keys_exist($keys, $_POST) && $this->check_str($keys, $_POST)){
				if (!preg_match("/^[a-zA-Z]*$/", $_POST["fname"]) || !preg_match("/^[a-zA-Z]*$/", $_POST["lname"]) || !preg_match("/^[a-zA-Z0-9]*$/", $_POST["uname"]) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
					die(json_encode(["status" => 0, "msg" => "Invalid Characters"]));
				}
				if ($this->getUsers($_POST["uname"])->rowCount() !== 0){
					die(json_encode(["status" => 0, "msg" => "Username already in use"]));
				}
				$hash = password_hash($_POST["pword"], PASSWORD_BCRYPT);
				$this->connect("INSERT INTO `".$this->db_users->table."` (`fname`, `lname`, `uname`, `email`, `pword`) VALUES ('".$_POST["fname"]."','".$_POST["lname"]."','".$_POST["uname"]."','".$_POST["email"]."','".$_POST["pword"]."')", $this->db_users->name);
				$this->roles[$_POST["uname"]] = ["is_admin" => $_POST["is_admin"], "can_post" => $_POST["can_post"]];
				file_put_contents($_SERVER["DOCUMENT_ROOT"]."/data/data.json", json_encode($this->roles));
				die(json_encode(["status" => 1, "msg" => "Operation successful"]));
			} else {
				die(json_encode(["status" => 0, "msg" => "Invalid data"]));
			}
		}
	}

	public function addPost(){
		if ($_SERVER["REQUEST_METHOD"] === "POST"){
			if (isset($_POST["textarea"]) && isset($_POST["title"]) && is_string($_POST["textarea"]) && is_string($_POST["title"])){
				$title = htmlspecialchars(trim($_POST["title"]));
				$id = bin2hex(microtime(true));
				$link = $_SERVER["DOCUMENT_ROOT"]."/posts/".$id."/".strtolower(preg_replace("/[^a-zA-Z0-9ء-ي_\-+]/", "_", trim($_POST["title"]))).".html";
				if (!isset($title) && !$this->is_logged && !isset($_POST["textarea"]) && empty($_POST["textarea"]) && empty($title)){
					die(json_encode(["status" => 0, "msg" => "Invalid values"]));
				}
				try {
					if (mkdir($_SERVER["DOCUMENT_ROOT"]."/posts/".$id, 0700)){
						$allowed_tags = ["a", "div", "span", "s", "strike", "b", "strong", "i", "u", "br", "img", "p", "font"];
						$allowed_attrs = ["align", "href", "src", "size", "style"];
						$allowed_properties = ["text-align"];
						$html = $this->strip(trim($_POST["textarea"]), $allowed_tags, $allowed_attrs, $allowed_properties);
						file_put_contents($link, $html);
						$this->connect("INSERT INTO `".$this->db_posts->table."` (`id`, `link`, `title`, `author`) VALUES ('$id', '$link', '$title', '".$_SESSION["name"]."')", $this->db_posts->name);
						die(json_encode(["status" => 1, "msg" => "Operation successful", "link" => "/posts/?id=$id"]));
					} else {
						die(json_encode(["status" => 0, "msg" => "An error occurred"]));
					}
					die(json_encode(["status" => 0, "msg" => "Title already exists"]));
				} catch (Throwable $e){
					die(json_encode(["status" => 0, "msg" => "An error occurred"]));
				}
			}
		} else {
			die(json_encode(["status" => 0, "msg" => "Method not allowed"]));
		}
	}

	public function delUser(string $uname){
		try {
			if ($_SERVER["REQUEST_METHOD"] === "POST"){
				$uname = $_POST["uname"];
				if ($_SESSION["uname"] !== $uname && $this->is_admin){
					if ($this->getUsers($uname)->rowCount() === 1){
						unset($this->roles[$uname]);
						file_put_contents($_SERVER["DOCUMENT_ROOT"]."/data/data.json", json_encode($this->roles));
						$result = $this->connect("DELETE FROM ".$this->db_users->table." WHERE uname='$uname'", $this->db_users->name);
						if ($result->rowCount() === 1){
							die(json_encode(["status" => 1, "msg" => "User deleted successfully"]));
						} else {
							die(json_encode(["status" => 0, "msg" => "User deletion unsuccessful"]));
						}
					} else {
						die(json_encode(["status" => 0, "msg" => "User not found"]));
					}
				} else {
					die(json_encode(["status" => 0, "msg" => "Permission Denied"]));
				}
			} else {
				die(json_encode(["status" => 0, "msg" => "Method not allowed"]));
			}
		} catch (Throwable $e){
			die(json_encode(["status" => 0, "msg" => "An error was thrown"]));
		}
	}

	public function delPost(){
		try {
			if ($_SERVER["REQUEST_METHOD"] === "POST"){
				$id = $_POST["id"];
				if (is_string($id)){
					$post = $this->getPosts(["id" => $id]);
					$row = $post->fetch(PDO::FETCH_ASSOC);
					if ($post->rowCount() === 1 && ($row["author"] === $_SESSION["uname"] || $this->is_admin)){
						unlink($row["link"]);
						rmdir($_SERVER["DOCUMENT_ROOT"]."/posts/".$id);
						$result = $this->connect("DELETE FROM ".$this->db_posts->table." WHERE id='$id'", $this->db_posts->name);
						if ($result->rowCount() === 1){
							die(json_encode(["status" => 1, "msg" => "Post deleted successfully"]));
						} else {
							die(json_encode(["status" => 0, "msg" => "Post deletion unsuccessful"]));
						}
					} else {
						die(json_encode(["status" => 0, "msg" => "Post not found"]));
					}
				} else {
					die(json_encode(["status" => 0, "msg" => "Invalid data"]));
				}
			} else {
				die(json_encode(["status" => 0, "msg" => "Method not allowed"]));
			}
		} catch (Throwable $e){
			die(json_encode(["status" => 0, "msg" => "An error occurred"]));
		}
	}

	public function strip($text, $allowed_tags=array(), $allowed_attrs=array("style"), $allowed_properties=array()){
		if (!strlen($text)){
			return false;
		}
		$allowed_tags = array_unique($allowed_tags);
		$allowed_attrs = array_unique($allowed_attrs);
		$allowed_properties = array_unique($allowed_properties);
		$dom = new DOMDocument();
		libxml_use_internal_errors(true);
		if (LIBXML_VERSION >= 20708){
			if ($dom->loadHTML($text, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDT)){
				$this->remove_elements($dom, $allowed_tags, $allowed_attrs, $allowed_properties);
			}
			return utf8_decode($dom->saveHTML($dom->documentElement));
		} elseif (LIBXML_VERSION >= 20706){
			$allowed_tags[] = "html";
			$allowed_tags[] = "body";
			$allowed_tags[] = "head";
			if ($dom->loadHTML($text)){
				$this->remove_elements($dom, $allowed_tags, $allowed_attrs, $allowed_properties);
			}
			return preg_replace("@</*?!*?(?:doctype|body|html|head).*?>@si", "", utf8_decode($dom->saveHTML($dom->documentElement)));
		} else {
			return false;
		}
	}

	public function browse(){
		if ($this->is_logged && ($this->can_post || $this->is_admin)){
			$dir = new DirectoryIterator($_SERVER["DOCUMENT_ROOT"]."/posts/img");
			$return = ["status" => 1, "links" => []];
			foreach ($dir as $file){
				if (!$file->isDot() && $file->getFilename() != ".htaccess"){
					$return["links"][] = "/posts/img/".$file->getFilename();
				}
			}
			die(json_encode($return));
		} else {
			die(json_encode(["status" => 0]));
		}
	}

	public function upload(){
		if ($this->can_post){
			$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
			$translate = array(IMAGETYPE_PNG => "png", IMAGETYPE_JPEG => "jpg", IMAGETYPE_GIF => "gif");
			$filetype = exif_imagetype("php://input");
			if (in_array($filetype, $allowedTypes)){
				$dir = new DirectoryIterator($_SERVER["DOCUMENT_ROOT"]."/posts/img");
				$filename = bin2hex(random_bytes(20)).".".$translate[$filetype];
				foreach ($dir as $file){
					if (!$file->isDot() && $file->getFilename() != ".htaccess"){
						if (hash_file("sha256", $file->getPathname()) == hash_file("sha256", "php://input")){
							if (file_put_contents($_SERVER["DOCUMENT_ROOT"]."/posts/img/".$filename, file_get_contents("php://input"))){
								die(json_encode(["status" => 1, "link" => "/posts/img/".$file->getFilename()]));
							} else {
								die(json_encode(["status" => 0]));
							}
						}
					}
				}
				if (file_put_contents($_SERVER["DOCUMENT_ROOT"]."/posts/img/".$filename, file_get_contents("php://input"))){
					die(json_encode(["status" => 1, "link" => "/posts/img/".$filename]));
				} else {
					die(json_encode(["status" => 0]));
				}
			} else {
				die(json_encode(["status" => 0]));
			}
		} else {
			die(json_encode(["status" => 0]));
		}
	}

	public function logout(){
		session_start();
		session_destroy();
		header("Location: /login.php");
		exit();
	}

	public function getComments(){}
}
$data = new Data();
if (isset($_GET["f"]) && is_string($_GET["f"]) && method_exists($data, $_GET["f"])){
	header("Content-Type: application/json");
	call_user_func(array($data, $_GET["f"]));
}
?>
