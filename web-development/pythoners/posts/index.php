<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/static/functions.php";
if (isset($_GET["id"]) && is_string($_GET["id"]) && strlen($_GET["id"]) === 26){
	$post = $data->getPosts(["id" => $_GET["id"]])->fetch(PDO::FETCH_ASSOC);
	require $_SERVER["DOCUMENT_ROOT"]."/templates/posts.php";
} else {
	if (!$data->can_post){
		header("Location: /");
		exit();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/styles.php"; ?>
		<title>Pythoners | Posts</title>
		<style type="text/css">
			#text {
				margin: 0;
				width: 100%;
				resize: none;
				padding: 20px;
				overflow: auto;
				border-top: none;
				min-height: 200px;
				font-size: medium;
				border: 1px solid #DBDBDB;
				background-color: #FFFFFF;
			}
			#text img {
				max-height: 500px;
			}
			.content_content {
				padding-left: 20%;
				padding-right: 20%;
			}
			.content_content center {
				direction: rtl;
			}
			.content_content .active {
				background-color: #DBDBDB;
			}
			.content_content .container {
				margin-top: 20px;
				position: relative;
				border: 1px solid #DBDBDB;
				background-color: #F0F0F0;
				overflow: auto;
			}
			.container button {
				overflow: auto;
				float: left;
				border: 0;
				cursor: pointer;
				font-size: 16px;
				padding: 10px 20px;
				background-color: transparent;
			}
			.container img {
				height: 16px;
			}
			.container button:hover {
				background-color: #DBDBDB;
			}
			input[type=submit] {
				width: 100%;
			}
			#insertimage img {
				transform: scale(1.5);
				-ms-transform: scale(1.5);
				-webkit-transform: scale(1.5);
			}
			.content_content #model {
				top: 50%;
				left: 50%;
				z-index: 4;
				position: fixed;
				text-align: center;
				transform: translate(-50%, -50%);
				overflow-y: auto;
				max-height: 100%;
				max-width: 100%;
			}
			#model #progress {
				background-color: #FFFFFF;
				padding: 10px 20px;
			}
			input[type=file] {
				background-color: #FFFFFF;
			}
			#model button {
				padding: 10px 20px;
				width: 50%;
			}
			.content_content #cover {
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				z-index: 3;
				position: fixed;
				background-color: rgba(0, 0, 0, 0.5);
			}
			#browse #imgs {
				text-align: center;
				background-color: #FFFFFF;
				padding: 10px;
				max-height: 200px;
				overflow: auto;
				display: flex;
				flex: 50%;
				flex-wrap: wrap;
			}
			#imgs p {
				font-style: italic;
				color: #CBCBCB;
			}
			#imgs img {
				height: 100px;
				padding: 5px;
				cursor: pointer;
				vertical-align: middle;
				margin: auto;
			}
			#imgs img:hover {
				background-color: #DBDBDB;
			}
			#preview img {
				max-height: 100%;
				max-width: 100%;
			}
			@media screen and (max-width: 700px) {
				.content_content {
					padding-left: 5%;
					padding-right: 5%;
				}
			}
		</style>
	</head>
	<body>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/header.php"; ?>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/notification.php"; ?>
		<div class="content_content">
			<form onsubmit="event.preventDefault(); addPost(this);">
				<center style="text-align: right;">
					<p>عنوان:</p>
					<input type="text" style="width: 100%;" name="title" maxlength="100" required value="<?= $title ?>"><br>
					<div class="container" dir="ltr">
						<button id="bold" type="button" onclick="design(this.id, 'text');" title="bold"><i class="fa fa-bold"></i></button><button id="italic" type="button" onclick="design(this.id, 'text');" title="italic"><i class="fa fa-italic"></i></button><button id="underline" type="button" onclick="design(this.id, 'text');" title="underline"><i class="fa fa-underline"></i></button><button id="strikethrough" type="button" onclick="design(this.id, 'text');" title="strikethrough"><i class="fa fa-strikethrough"></i></button><button id="justifyleft" type="button" onclick="design(this.id, 'text');" title="align left"><i class="fa fa-align-left"></i></button><button id="justifycenter" type="button" onclick="design(this.id, 'text');" title="align center"><i class="fa fa-align-center"></i></button><button id="justifyright" type="button" onclick="design(this.id, 'text');" title="align right"><i class="fa fa-align-right"></i></button><button id="createlink" type="button" title="insert link" onclick="insertLink('text');"><i class="fa fa-link"></i></button><button id="insertimage" type="button" onclick="show('browse'); browse();"><i class="fa fa-image"></i></button>
					</div>
					<div id="text" contenteditable="true"><?= $content ?></div>
					<textarea name="textarea" style="display: none;" required></textarea>
					<input type="submit" value="ادخال" class="static_submit" onclick="(function(){ document.getElementsByName('textarea')[0].value = document.getElementById('text').innerHTML; })();">
				</center>
				<div id="cover" style="display: none;"></div>
				<div id="model" style="display: none;">
					<div id="browse">
						<div id="imgs"></div>
						<button class="static_submit" type="button" onclick="show('upload');">اضافة</button><button class="static_submit" type="button" onclick="hide();"><i class="fa fa-times"></i></button>
					</div>
					<div id="preview" style="display: none;">
						<img src="">
						<button class="static_submit" type="button" onclick="insertImage('text', document.querySelector('#preview > img').src); hide();">تأكيد</button><button class="static_submit" type="button" onclick="hide();"><i class="fa fa-times"></i></button>
					</div>
					<div id="upload" style="display: none;">
						<input type="file" accept=".jpg,.png,.gif"><br>
						<button class="static_submit" type="button" onclick="show('progress'); uploadImage('text');"><i class="fa fa-upload"></i></button><button class="static_submit" type="button" onclick="hide();"><i class="fa fa-times"></i></button>
					</div>
					<div id="progress" style="display: none;">
						<progress></progress>
						<span id="percentage"></span>
					</div>
				</div>
			</form>
		</div>
		<?php require $_SERVER["DOCUMENT_ROOT"]."/static/footer.php"; ?>
		<script type="text/javascript">
			function design(action, id, value=null){
				var text = document.getElementById(id);
				text.focus();
				document.execCommand(action, false, value);
			}
			function insertLink(id){
				var link = prompt("Enter URL:", "https://");
				if (link){
					var text = document.getElementById(id);
					text.focus();
					if (document.getSelection().toString() !== ""){
						document.execCommand("insertHTML", false, "&nbsp;<a href='"+link+"'>"+document.getSelection().toString()+"</a>&nbsp;");
					} else {
						document.execCommand("insertHTML", false, "&nbsp;<a href='"+link+"'>"+link+"</a>&nbsp;");
					}
				}
			}
			function uploadImage(id){
				var xhttp = new XMLHttpRequest();
				xhttp.upload.addEventListener("progress", function(evt){
					var progress = document.getElementsByTagName("progress")[0];
					if (evt.lengthComputable){
						progress.max = evt.total;
						progress.value = evt.loaded;
						document.getElementById("percentage").innerHTML = Math.round((evt.loaded / evt.total) * 100) + "%";
					}
				}, false);
				xhttp.onreadystatechange = function(){
					if (xhttp.readyState == 4 && xhttp.status == 200){
						var response = JSON.parse(xhttp.responseText);
						if (response["status"]){
							console.log(xhttp.responseText);
							hide();
							document.querySelector('#preview > img').src = response["link"];
							show('preview');
						}
					}
				}
				xhttp.open("POST", "/static/functions.php?f=upload", true);
				xhttp.send(document.querySelector("input[type=file]").files[0]);
			}
			function insertImage(id, link){
				document.getElementById(id).focus();
				document.execCommand("insertHTML", false, "<img src='"+link+"'>");
			}
			function show(id){
				var divs = document.querySelectorAll("#model > *");
				for (var i = 0; i < divs.length; i++){
					if (divs[i].id != id){
						divs[i].style.display = "none";
					}
				}
				document.getElementById(id).style.display = "inline-block";
				document.getElementById("model").style.display = "inline-block";
				document.getElementById("cover").style.display = "block";
			}
			function hide(){
				var divs = document.querySelectorAll("#model > *");
				for (var i = 0; i < divs.length; i++){
					divs[i].style.display = "none";
				}
				document.getElementById("model").style.display = "none";
				document.getElementById("cover").style.display = "none";
				document.querySelector("input[type=file]").value = "";
				var imgs = document.getElementById("browse").children[0];
				while (imgs.lastChild){
					imgs.removeChild(imgs.lastChild);
				}
			}
			function browse(){
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (xhttp.readyState == 4 && xhttp.status == 200){
						var response = JSON.parse(xhttp.responseText);
						if (response["status"]){
							if (response["links"].length > 0){
								for (var i = 0; i < response["links"].length; i++){
									var img = document.createElement("img");
									img.src = response["links"][i];
									img.onclick = function(){
										document.querySelector("#preview > img").src = this.src;
										show("preview");
									};
									document.getElementById("browse").children[0].appendChild(img);
								}
							} else {
								var p = document.createElement("p");
								p.innerHTML = "Empty";
								document.getElementById("browse").children[0].appendChild(p);
							}
						}
					}
				};
				xhttp.open("GET", "/static/functions.php?f=browse", true);
				xhttp.send();
			}
			function addPost(element){
				var form = new FormData(element);
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						let response = JSON.parse(this.responseText);
						if (response.status){
							notification_show("success", response.msg);
							window.location.href = response.link;
						} else {
							notification_show("failure", response.msg);
						}
					}
				};
				xhttp.open("POST", "/static/functions.php?f=addPost", true);
				xhttp.send(form);
			}
		</script>
	</body>
</html>
<?php } ?>
