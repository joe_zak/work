use actix_web::{HttpServer, App, web, HttpResponse, Responder};
use tera::{Tera, Context};
use serde::{Serialize, Deserialize};

#[derive(Debug, Deserialize)]
struct LoginUser {
	username: String,
	password: String,
}

async fn login(tera: web::Data<Tera>) -> impl Responder {
	let mut data = Context::new();
	data.insert("title", "Login");

	let rendered = tera.render("login.html", &data).unwrap();
	HttpResponse::Ok().body(rendered)
}

async fn process_login(data: web::Form<LoginUser>) -> impl Responder {
	println!("{:?}", data);
	HttpResponse::Ok().body(format!("Logged in: {}", data.username))
}

#[derive(Debug, Deserialize)]
struct User {
	username: String,
	email: String,
	password: String,
}

async fn process_signup(data: web::Form<User>) -> impl Responder {
	println!("{:?}", data);
	HttpResponse::Ok().body(format!("Successfully saved user: {}", data.username))
}

#[derive(Serialize)]
struct Post {
	title: String,
	link: String,
	author: String,
}

async fn index(tera: web::Data<Tera>) -> impl Responder {
	let mut data = Context::new();

	let posts = [
		Post {
			title: String::from("This is the first link"),
			link: String::from("https://example.com"),
			author: String::from("Bob"),
		},
		Post {
			title: String::from("The Second Link"),
			link: String::from("https://example.com"),
			author: String::from("Alice"),
		}
	];

	data.insert("title", "Hacker Clone");
	data.insert("posts", &posts);

	let rendered = tera.render("index.html", &data).unwrap();
	HttpResponse::Ok().body(rendered)
}

async fn signup(tera: web::Data<Tera>) -> impl Responder {
	let mut data = Context::new();
	data.insert("title", "Sign Up");

	let rendered = tera.render("signup.html", &data).unwrap();
	HttpResponse::Ok().body(rendered)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
    	let tera = Tera::new("templates/**/*").unwrap();
    	App::new()
    		.data(tera)
    		.route("/", web::get().to(index))
    		.route("/signup", web::get().to(signup))
    		.route("/signup", web::post().to(process_signup))
    		.route("/login", web::get().to(login))
    		.route("/login", web::post().to(process_login))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}
