<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
if (!isset($_SESSION["is_logged"])){
	header("Location: login.php");
	exit();
}
if (!isset($_SESSION["is_phone"])){
	header("Location: /");
	exit();
}
if (isset($_SESSION["username"])){
	$info = json_decode(file_get_contents("status.json"), true);
	$is_member = (bool)$info[$_SESSION["username"]]["is_member"];
	if (!$is_member){
		header("Location: logout.php");
		exit();
	}
	$is_admin = (bool)$info[$_SESSION["username"]]["is_admin"];
}
if ($_SERVER["REQUEST_METHOD"] == "GET"){
    if (isset($_GET["hash"])){
        if ($_GET["hash"] != hash_file("sha256", "jobs.json")){
            echo 1;
        }
    } else if (isset($_GET["add"]) && isset($_GET["key"]) && $is_admin){
        $data = json_decode(file_get_contents("jobs.json"));
        $data->{$_GET["key"]} = array("category" => "new", "username" => "");
        file_put_contents("jobs.json", json_encode($data));
    } else if (isset($_GET["delete"]) && isset($_GET["key"]) && $is_admin){
        $data = json_decode(file_get_contents("jobs.json"));
        unset($data->{$_GET["key"]});
        file_put_contents("jobs.json", json_encode($data));
    } else if (isset($_GET["move"]) && isset($_GET["key"])){
        $data = json_decode(file_get_contents("jobs.json"));
        if ($_SESSION["username"] == $data->{$_GET["key"]}->{"username"} && $data->{$_GET["key"]}->{"category"} == "ongoing"){
            $data->{$_GET["key"]}->{"category"} = "done";
            file_put_contents("jobs.json", json_encode($data));
        }
    } else if (isset($_GET["take"]) && isset($_GET["key"])){
        $data = json_decode(file_get_contents("jobs.json"));
        $data->{$_GET["key"]}->{"username"} = $_SESSION["username"];
        $data->{$_GET["key"]}->{"category"} = "ongoing";
        file_put_contents("jobs.json", json_encode($data));
    }
}
?>