<?php
# we need to change this one to 0 when we are done
error_reporting(E_ALL ^ E_NOTICE);
session_start();
if (!isset($_SESSION["is_logged"])){
	header("Location: login.php");
	exit();
}
if (!isset($_SESSION["is_phone"])){
	header("Location: /");
	exit();
}
if (isset($_SESSION["username"])){
	$info = json_decode(file_get_contents("status.json"), true);
	$is_member = (bool)$info[$_SESSION["username"]]["is_member"];
	if (!$is_member){
		header("Location: logout.php");
		exit();
	}
	$is_admin = (bool)$info[$_SESSION["username"]]["is_admin"];
}
$new = $ongoing = $done = "";
foreach (json_decode(file_get_contents("jobs.json")) as $key => $value){
	if ($value->category == "new"){
		$new .= '<div class="jobs">'.$key.'<br><button class="dropbtn" onclick="send(\'take\', \''.$key.'\');">TAKE</button>';
		if ($is_admin){
			$new .= '<button class="dropbtn" onclick="send(\'delete\', \''.$key.'\');">DELETE</button>';
		}
		$new .= '</div><br>';
	} else if ($value->category == "ongoing"){
		if ($is_admin || $_SESSION["username"] == $value->username){
			$ongoing .= '<fieldset class="jobs"><legend>'.$value->username.'</legend>'.$key.'<br>';
			if ($_SESSION["username"] == $value->username){
				$ongoing .= '<button class="dropbtn" onclick="send(\'move\', \''.$key.'\');">MOVE</button>';
			}
			if ($is_admin){
				$ongoing .= '<button class="dropbtn" onclick="delete(\''.$key.'\');">DELETE</button>';
			}
			$ongoing .= '</fieldset><br>';
		} else {
			$ongoing .= '<fieldset class="jobs"><legend>'.$value->username.'</legend>'.$key.'<br></fieldset><br>';
		}
	} else if ($value->category == "done"){
		if ($is_admin){
			$done .= '<fieldset class="jobs"><legend>'.$value->username.'</legend>'.$key.'<br><button class="dropbtn" onclick="send(\'delete\', \''.$key.'\');">DELETE</button></fieldset><br>';
		} else {
			$done .= '<fieldset class="jobs"><legend>'.$value->username.'</legend>'.$key.'</fieldset><br>';
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>CyberNetics&trade; | Jobs</title>
		<?php if ($_SESSION["is_phone"]){ ?>
			<link rel="stylesheet" type="text/css" href="css/m_static.css">
		<?php } else { ?>
			<link rel="stylesheet" type="text/css" href="css/static.css">
		<?php } ?>
		<style>
			fieldset {
				border: 1px solid #b8b8b8;
				display: inline-block;
				white-space: normal;
				vertical-align: top;
			}
			
			.jobs {
				padding: 15px;
				border: 1px solid #b8b8b8;
				width: 80%;
				float: left;
				margin-bottom: 10px;
			}

			.dropbtn {
				background-color: #3498DB;
				color: #ffffff;
				padding: 10px;
				font-size: 16px;
				border: none;
				cursor: pointer;
				float: right;
			}

			.dropbtn:hover, .dropbtn:focus {
				background-color: #2980B9;
			}
		</style>
		<link rel="icon" href="img/old_logo.png">
		<script src="js/static.js"></script>
	</head>
	<body>
		<div class="header">
			<div class="div1" onclick="(function(){window.location = 'home.php';})()" onmouseover="over('img/logo.jpg', 'logo');" onmouseout="out('img/logo.png', 'logo');">
				<img id="logo" src="img/logo.png">
				<span>CyberNetics&trade;</span>
			</div>
			<div class="div2" onclick="(function(){window.location = 'dashboard.php';})()">
				<img id="user" src="img/user.png" title="<?php echo $_SESSION["username"]; ?>">
			</div>
			<?php if ($is_admin){ ?>
				<div class="div2" onclick="(function(){window.location = 'edit.php';})()">
					<img id="edit" src="img/edit.png" title="CyberNetics&trade; | Edit">
				</div>
			<?php } ?>
		</div>
		<div style="margin: 100px 0 0 0; overflow: scroll; position: absolute; top: 0; left: 0; right: 0; bottom: 0; white-space: nowrap; padding: 20px;">
			<fieldset>
				<legend>NEW</legend>
			<?php echo $new; ?>
			<?php if ($is_admin){ ?>
				<input id="job" type="text" style="border: 1px solid #b8b8b8; height: 30px; float: left; font-size: 16px; padding: 5px; margin-top: 10px;" placeholder="description...">
				<button class="dropbtn" style="margin-top: 10px;" onclick="send('add', document.getElementById('job').value);">ADD</button>
			<?php } ?>
			</fieldset>
			<fieldset>
				<legend>ONGOING</legend>
			<?php echo $ongoing; ?>
			</fieldset>
			<fieldset>
				<legend>DONE</legend>
			<?php echo $done; ?>
			</fieldset>
		</div>
		<img class="logout" src="img/logout.png" title="CyberNetics&trade; | Logout" onclick="(function(){window.location = 'logout.php'})()">
		<script>
			function send(action, key){
				var xhttp = new XMLHttpRequest();
				xhttp.open("GET", "change.php?"+action+"=1&key="+key);
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						location.reload(true);
					}
				}
				xhttp.send();
			}
			var xhttp = new XMLHttpRequest();
			setInterval(function(){
				xhttp.open("GET", "change.php?hash=<?php echo hash_file("sha256", "jobs.json"); ?>");
				xhttp.onreadystatechange = function(){
					if (this.readyState == 4 && this.status == 200){
						if (this.responseText == "1"){
							location.reload(true);
						}
					}
				};
				xhttp.send();
			}, 1000);
		</script>
	</body>
</html>