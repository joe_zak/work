// Express to run server and routes
const express = require("express");

// Start up an instance of app
const app = express();

/* Dependencies */
const bodyParser = require("body-parser");
/* Middleware */
// Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// Cors for cross origin allowance
const cors = require("cors");
app.use(cors());

app.use(express.static("static"));

const port = 8080;

const server = app.listen(port, () => {
	console.log(`running on http://localhost:${port}`);
});
const appData = {};
app.get("/", (req, res) => {
	res.send("hello world");
});
app.get("/all", (req, res) => {
	res.send(appData);
});

app.post("/", (req, res) => {
	res.send("POST received");
});

const data = [];

app.post("/add", addMovie);

function add(req, res) {
	console.log(req.body);
	data.push(req.body);
}
