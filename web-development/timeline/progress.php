<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
	$host = "localhost";
	$uname = "joe";
	$pword = "Izzyalex2017";
	$dbname = "orders";

	try {
		$conn = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pword);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$progress = (int) $_POST["progress"];
		$id = (int) $_POST["id"];

		$query = $conn->prepare("UPDATE orders SET progress=:progress WHERE id=:id");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->bindParam(":progress", $progress);
		$query->bindParam(":id", $id);
		$query->execute();

		if ($query->rowCount() < 1) {
			echo json_encode(["status" => "error", "message" => "Problem updating data"]);
			exit();
		}

		$conn = null;
	} catch (TypeError $e) {
		echo json_encode(["status" => "error", "message" => "Invalid request"]);
		exit();
	} catch (PDOException $e) {
		echo json_encode(["status" => "error", "message" => "Connection problem"]);
		exit();
	}

	echo json_encode(["status" => "success"]);
} else {
	echo json_encode(["status" => "error", "message" => "Invalid request"]);
}