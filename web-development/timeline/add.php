<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
	$host = "localhost";
	$uname = "joe";
	$pword = "Izzyalex2017";
	$dbname = "orders";

	try {
		$conn = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pword);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$field = $_POST["field"];
		$id = (int) $_POST["id"];

		if (!($field && $field["title"])) {
			echo json_encode(["status" => "error", "message" => "Invalid request"]);
			exit();
		}

		$result = $conn->query("SELECT fields FROM orders WHERE id=$id", PDO::FETCH_ASSOC);
		if ($result->rowCount() < 1) {
			echo json_encode(["status" => "error", "message" => "Not found"]);
			exit();
		}

		$result = json_decode($result->fetch()["fields"], true);
		array_push($result, $field);
		$result = json_encode($result);

		$query = $conn->prepare("UPDATE orders SET fields=:fields WHERE id=:id");
		$query->setFetchMode(PDO::FETCH_ASSOC);
		$query->bindParam(":fields", $result);
		$query->bindParam(":id", $id);
		$query->execute();

		if ($query->rowCount() < 1) {
			echo json_encode(["status" => "error", "message" => "Problem updating data"]);
			exit();
		}

		$conn = null;
	} catch (TypeError $e) {
		echo json_encode(["status" => "error", "message" => "Invalid request"]);
		exit();
	} catch (PDOException $e) {
		echo json_encode(["status" => "error", "message" => "Connection problem"]);
		exit();
	}

	echo json_encode(["status" => "success"]);
} else {
	echo json_encode(["status" => "error", "message" => "Invalid request"]);
}