<?php
$host = "localhost";
$uname = "joe";
$pword = "Izzyalex2017";
$dbname = "orders";

try {
	$conn = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pword);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$query = $conn->prepare("SELECT * FROM orders");
	$query->setFetchMode(PDO::FETCH_ASSOC);
	$query->execute();

	if ($query->rowCount() < 1) {
		header("HTTP/1.1 404 Not Found");
		exit("404 Page Not Found");
	}

	$result = $query->fetchAll();
} catch (PDOException $e) {
	echo "Error: ".$e->getMessage();
}

$conn = null;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>
			Alter order fields
		</title>
		<meta name="robots" content="index, follow" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link
			rel="stylesheet"
			href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
			integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
			crossorigin="anonymous"
		/>
		<link rel="stylesheet" href="global.css" />
		<link rel="stylesheet" href="timeline.css" />
		<script
			src="https://code.jquery.com/jquery-3.5.1.min.js"
			integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			crossorigin="anonymous"
		></script>
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
			crossorigin="anonymous"
		></script>
	</head>
	<body>
		<div class="loader invisible position-absolute">
			<div class="spinner-border" style="height: 10rem; width: 10rem;"></div>
		</div>
		<div class="container py-5">
			<table class="table table-bordered mb-5">
				<thead class="thead-light">
					<th>ID</th>
					<th>Title</th>
					<th>Description</th>
					<th>Progress</th>
				</thead>
				<tbody></tbody>
			</table>
			<div class="m-auto w-75">
				<div>
					<p class="text-danger" id="error"></p>
					<div class="form-group">
						<label for="id">ID:</label>
						<select id="id" class="form-control">
							<option default>None</option>
						</select>
					</div>
					<div class="form-group">
						<input id="title" type="text" placeholder="Title" class="form-control" />
					</div>
					<div class="form-group">
						<textarea id="description" placeholder="Description" class="form-control"></textarea>
					</div>
					<button id="add" type="submit" class="btn btn-primary d-inline-block w-100 mt-3 mb-5"><span class="spinner-border spinner-border-sm invisible" aria-hidden="true"></span> Add</button>
				</div>
			</div>
		</div>
		<script src="functions.js"></script>
		<script>
			let data = <?= json_encode($result) ?>;

			let id = $("#id");

			for (let i = 0; i < data.length; i++) {
				data[i].fields = JSON.parse(data[i].fields);
				let option = $(`<option value="${data[i].id}">${data[i].id}</option>`);
				id.append(option);
			}

			buildTable(data);

			let title = $("#title");
			let description = $("#description");
			let add = $("#add");
			let error = $("#error");

			let loader = add.find(".spinner-border");

			add.click(function() {
				loader.removeClass("invisible");
				error.text("");
				field = {title: title.val(), description: description.val()}
				$.post("add.php", {
					id: id.val(),
					field: field
				}, function(res, status) {
					res = JSON.parse(res);
					if (res.status === "error") {
						error.text(`* ${res.message}`);
						return;
					}
					let i = data.findIndex((obj) => obj.id == id.val());
					data[i].fields.push(field);
					buildTable(data);
				})
				loader.addClass("invisible");
			})
		</script>
	</body>
</html>