function buildTable(data) {
	$("table tbody *").remove();
	let fragment = document.createDocumentFragment();
	for (let i = 0; i < data.length; i++) {
		let row = $(`
			<tr>
				<td rowspan="${data[i].fields.length + 1}">${data[i].id}</td>
			</tr>
			${data[i].fields.map(function (obj, index) {
				return `<tr>
					<td>${obj["title"]}</td>
					<td>${obj["description"]}</td>
					<td><input name="${
						data[i].id
					}" type="radio" value="${index}" onchange="setProgress(this.value, this.name)" ${index == data[i].progress && "checked"}/></td>
				</tr>`;
			})}
		`);
		row.appendTo(fragment);
	}
	$("table tbody").append(fragment);
}

function setProgress(value, id) {
	$.post(
		"progress.php",
		{
			id: id,
			progress: value,
		},
		function (res, status) {
			res = JSON.parse(res);
			if (res.status === "error") {
				error.text(`* ${res.message}`);
				return;
			}
			let i = data.findIndex((obj) => obj.id == id);
			data[i].fields.push(field);
			buildTable(data);
		}
	);
}
