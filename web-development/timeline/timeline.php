<?php
$host = "localhost";
$uname = "joe";
$pword = "Izzyalex2017";
$dbname = "orders";

$id = isset($_GET["id"]) ? (int) $_GET["id"] : null;
if (!$id) {
	header("HTTP/1.1 404 Not Found");
	exit("404 Page Not Found");
}

try {
	$conn = new PDO("mysql:host=$host;dbname=$dbname", $uname, $pword);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$query = $conn->prepare("SELECT fields, progress, date FROM orders WHERE id=:id");
	$query->setFetchMode(PDO::FETCH_ASSOC);
	$query->bindParam(":id", $id);
	$query->execute();

	if ($query->rowCount() != 1) {
		header("HTTP/1.1 404 Not Found");
		exit("404 Page Not Found");
	}

	$result = $query->fetch();
	$fields = json_decode($result["fields"], true);
	$progress = (int) $result["progress"];
	$date = $result["date"];
	var_dump($date);
} catch (PDOException $e) {
	echo "Error: ".$e->getMessage();
}

$conn = null;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>
			Order #<?= $id ?> Progress
		</title>
		<meta name="robots" content="index, follow" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link
			rel="stylesheet"
			href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
			integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
			crossorigin="anonymous"
		/>
		<link rel="stylesheet" href="timeline.css" />
		<script
			src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
			integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
			crossorigin="anonymous"
		></script>
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
			crossorigin="anonymous"
		></script>
	</head>
	<body>
		<section style="background: #f5f5f5">
			<div class="container py-5">
				<div class="main-timeline">
					<?php
					foreach ($fields as $field) {
						echo '
						<div class="timeline-container">
							<div class="timeline">
								<div class="card">
									<div class="card-body p-4">
										<h3>'.$field["title"].'</h3>
										<p class="mb-0">'.$field["description"].'</p>
									</div>
								</div>
							</div>
						</div>';
					}
					?>
				</div>
			</div>
		</section>
		<script>
			let timeline = $(".timeline");
			timeline.odd().addClass("left");
			timeline.even().addClass("right");

			let timeline_container = $(".timeline-container");
			let length = <?= $progress ?>;
			for (let i = 0; i <= length; i++) {
				timeline_container[i].classList.add("active");
			}
		</script>
	</body>
</html>
