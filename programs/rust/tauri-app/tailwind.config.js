const { colors } = require("tailwindcss/colors");

module.exports = {
	mode: "jit",
	purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
	darkMode: "media", // or 'media' or 'class'
	theme: {
		extend: {
			colors: {
				primary: {
					light: "#4fb3bf",
					default: "#00838f",
					dark: "#005662",
				},
				secondary: {
					light: "#6a4f4b",
					default: "#3e2723",
					dark: "#1b0000",
				},
			},
		},
	},
	variants: {},
	plugins: [],
};
