#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

#[tauri::command]
fn my_custom_handler(text: String) -> String {
  if text == "Ping" {
    "Pong!".into()
  } else {
    "Ping".into()
  }
}

fn main() {
  tauri::Builder::default()
    .invoke_handler(tauri::generate_handler![my_custom_handler])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
