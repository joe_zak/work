enum PieceColor {
    White,
    Black,
}

enum PieceType {
    King,
    Queen,
    Rook,
    Bishop,
    Knight,
    Pawn,
    None,
}

fn main() {
    println!("Hello, world!");
}
