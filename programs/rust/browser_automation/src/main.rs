use playwright::Playwright;
use tokio;

#[tokio::main]
async fn main() -> Result<(), playwright::Error> {
    let pw = Playwright::initialize().await?;
    pw.prepare()?;
    let chromium = pw.chromium();
    let browser = chromium.launcher().headless(false).launch().await?;
    let context = browser.context_builder().build().await?;
    let page = context.new_page().await?;
    page.goto_builder("https://example.com/").goto().await?;
    let s: String = page.eval("() => location.href").await?;
    assert_eq!(s, "https://example.com/");
    page.click_builder("a").click().await?;
    Ok(())
}
