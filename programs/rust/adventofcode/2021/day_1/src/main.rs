use std::{fs, path::Path};

fn part_1(input: &Vec<&str>) -> u32 {
    let mut last = 0;
    let mut count = 0;
    for depth in input {
        let depth: u32 = depth.parse().expect("Failed to parse");
        if depth > last {
            count += 1;
        }
        last = depth;
    }
    count - 1
}

fn part_2(input: &Vec<&str>) -> u32 {
    let mut last = 0;
    let mut count = 0;
    let mut sum = 0;
    for i in 0..(input.len() - 2) {
        sum = input[i].parse::<u32>().unwrap()
            + input[i + 1].parse::<u32>().unwrap()
            + input[i + 2].parse::<u32>().unwrap();
        if sum > last {
            count += 1;
        }
        last = sum;
    }
    count - 1
}

fn main() {
    let content = fs::read_to_string("input.txt").expect("Error");
    let content: Vec<&str> = content.split('\n').collect();
    println!("{}", part_1(&content));
    println!("{}", part_2(&content));
}
