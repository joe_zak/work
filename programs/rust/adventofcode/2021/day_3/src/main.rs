use std::{cmp::max, collections::HashMap, fs};

fn part_1(input: &Vec<&str>) -> i32 {
    let mut result = vec![vec![0; 2]; 12];
    for record in input {
        for (index, bit) in record.chars().enumerate() {
            result[index][bit as usize - 0x30] += 1;
        }
    }
    let mut gamma: i32 = 0;
    let mut epsilon: i32 = 0;
    for (index, bit) in result.iter().rev().enumerate() {
        if bit[0] > bit[1] {
            epsilon += 2i32.pow(index as u32)
        } else {
            gamma += 2i32.pow(index as u32)
        }
    }
    gamma * epsilon
}

fn part_2(input: &Vec<&str>) -> u32 {
    let mut temp = input.clone();
    let mut result = vec![0; 2];
    for line in temp.iter() {
        let bit = line.chars().next().unwrap();
        result[bit as usize - 0x30] += 1;
    }
    if result[0] > result[1] {
        temp = temp
            .into_iter()
            .filter(|line| line.starts_with('0'))
            .collect();
    } else {
        temp = temp
            .into_iter()
            .filter(|line| line.starts_with('1'))
            .collect();
    }
    println!("{:?}", temp);
    0
}

fn main() {
    // let content = fs::read_to_string("input.txt").expect("Failed to read file");
    let content = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
    let content: Vec<&str> = content.lines().collect();
    println!("{:?}", content);
    // println!("{}", part_1(&content));
    println!("{}", part_2(&content));
}
