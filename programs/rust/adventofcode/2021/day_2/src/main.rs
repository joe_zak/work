use std::fs;

fn part_1(input: &Vec<&str>) -> u32 {
    let mut x: u32 = 0;
    let mut y: u32 = 0;
    for i in input {
        let a: Vec<&str> = i.split_whitespace().collect();
        match a[0] {
            "up" => x -= a[1].parse::<u32>().unwrap(),
            "forward" => y += a[1].parse::<u32>().unwrap(),
            "down" => x += a[1].parse::<u32>().unwrap(),
            _ => (),
        }
    }
    x * y
}

fn part_2(input: &Vec<&str>) -> u32 {
    let mut x: u32 = 0;
    let mut y: u32 = 0;
    let mut aim: u32 = 0;
    for i in input {
        let a: Vec<&str> = i.split_whitespace().collect();
        match a[0] {
            "up" => {
                aim -= a[1].parse::<u32>().unwrap();
            }
            "forward" => {
                y += a[1].parse::<u32>().unwrap();
                x += aim * a[1].parse::<u32>().unwrap();
            }
            "down" => {
                aim += a[1].parse::<u32>().unwrap();
            }
            _ => (),
        }
    }
    x * y
}

fn main() {
    let content = fs::read_to_string("input.txt").expect("Failed to read file");
    let content: Vec<&str> = content.lines().collect();
    println!("{}", part_1(&content));
    println!("{}", part_2(&content));
}
