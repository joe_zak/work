#[macro_use]
extern crate peroxide;
use peroxide::prelude::*;

fn main() {
    let a = ml_matrix("1 2;3 4");

    let b = matrix(c!(1, 2, 3, 4), 2, 2, Row);

    let mut z = zeros(2, 2);
    z[(0, 0)] = 1.0;
    z[(0, 1)] = 2.0;
    z[(1, 0)] = 3.0;
    z[(1, 1)] = 4.0;

    let c = a * b;

    c.print();

    c.det().print();
    c.inv().print();
}
