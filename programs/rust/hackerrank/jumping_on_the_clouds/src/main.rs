use text_io::read;

fn main() {
    let n: u32 = read!();
    let mut c: Vec<u32> = Vec::new();
    for _ in 0..n {
        let i: u32 = read!();
        c.push(i);
    }
    let mut steps: u32 = 0;
    let mut i = 0;
    while i < c.len() - 1 {
        if i + 2 >= c.len() {
            steps += 1;
            break;
        }
        if c[i + 2] == 1 { i += 1 } else { i += 2 }
        steps += 1;
    }
    println!("{}", steps);
}
