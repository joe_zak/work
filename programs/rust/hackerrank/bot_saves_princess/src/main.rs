use text_io::read;

fn display_path_to_princess(_n: i32, grid: Vec<String>) {
    let mut result = String::from("");
    let mut princess: Vec<i32> = vec![];
    let mut machine: Vec<i32> = vec![];
    for (i, row) in grid.iter().enumerate() {
        if let Some(p) = row.find("p") {
            princess = vec![i as i32, p as i32];
        }
        if let Some(m) = row.find("m") {
            machine = vec![i as i32, m as i32];
        }
    }
    let vert = princess[0] - machine[0];
    let hori = princess[1] - machine[1];
    if vert > 0 {
        result.push_str("DOWN\n".repeat(vert as usize).as_ref());
    } else if vert < 0 {
        result.push_str("UP\n".repeat((vert * -1) as usize).as_ref());
    }
    if hori > 0 {
        result.push_str("RIGHT\n".repeat(hori as usize).as_ref());
    } else if hori < 0 {
        result.push_str("LEFT\n".repeat((hori * -1) as usize).as_ref());
    }
    println!("{:?}", result);
}

fn main() {
    let m: i32 = read!();
    let mut grid: Vec<String> = vec![];
    for _ in 0..m {
        grid.push(read!());
    }
    display_path_to_princess(m, grid);
}
