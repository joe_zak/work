use text_io::read;

fn main() {
    let s: String = read!();
    let n: usize = read!();
    let repeats = n / s.len();
    let diff = n - repeats * s.len();
    let rest: &usize = &s[..diff].chars().map(|c| if c == 'a' { 1 } else { 0 }).sum();
    println!("{}", (s.matches('a').count() * repeats) + rest);
}
