module vezeeta_scraper

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.0
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/antchfx/htmlquery v1.2.3 // indirect
	github.com/antchfx/xmlquery v1.3.6 // indirect
	github.com/antchfx/xpath v1.2.0 // indirect
	github.com/chromedp/cdproto v0.0.0-20210625233425-810000e4a4fc // indirect
	github.com/chromedp/chromedp v0.7.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/headzoo/surf v1.0.0 // indirect
	github.com/headzoo/ut v0.0.0-20181013193318-a13b5a7a02ca // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/headzoo/surf.v1 v1.0.0
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
