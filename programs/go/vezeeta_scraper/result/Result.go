package Result

type Result struct {
	Id                int          `json:"id"`
	Key               string       `json:"key"`
	IsNew             bool         `json:"isNew"`
	Img               string       `json:"img"`
	OffersHomeVisits  bool         `json:"offersHomeVisits"`
	Fees              string       `json:"fees"`
	DoctorRating      float64      `json:"doctorRating"`
	FacilityRating    float64      `json:"facilityRating"`
	RatingsCount      int          `json:"ratingsCount"`
	WaitingTime       int          `json:"waitingTime"`
	IsTopRated        bool         `json:"isTopRated"`
	Views             int          `json:"views"`
	FacilityImages    []string     `json:"facilityImages"`
	Gender            bool         `json:"gender"`
	RequirePrePayment bool         `json:"requirePrePayment"`
	Appointments      Appointments `json:"appointments"`
	Ar                Data         `json:"ar"`
	En                Data         `json:"en"`
}

type Appointments struct {
	Days []Day `json:"Days"`
}

type Day struct {
	Name        string `json:"DayName"`
	IsAvailable bool   `json:"DayIsAvailable"`
	Slots       []Slot
}

type Slot struct {
	BookingTime string
	From        string
	To          string
}

type Data struct {
	Title  string `json:"title"`
	Name   string `json:"name"`
	Vision string `json:"vision"`
	// ProfessionalAffiliations []string            `json:"professionalAffiliations"`
	DetailedDescription string              `json:"fullDescription"`
	ShortDescription    string              `json:"shortDescription"`
	ProfessionalTitle   string              `json:"professionalTitle"`
	Specialties         []Specialty         `json:"specialties"`
	FacilityTitle       string              `json:"facilityTitle"`
	DoctorServices      []Service           `json:"doctorServices"`
	Tags                []string            `json:"tags"`
	InsuranceProviders  []InsuranceProvider `json:"insuranceProviders"`
	SpokenLanguages     []Language          `json:"spokenLanguages"`
}

type Specialty struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
	Main bool   `json:"isMain,omitempty"`
}

type Service struct {
	Name       string `json:"name"`
	Slug       string `json:"slug"`
	IsBookable bool   `json:"isBookable"`
	FixedPrice string `json:"price"`
	OfferPrice string `json:"offerPrice"`
}

type InsuranceProvider struct {
	Name        string `json:"name"`
	NameEnglush string `json:"nameEN"`
	NameArabic  string `json:"nameAR"`
	Slug        string `json:"slug"`
	Image       string `json:"img"`
	Longitude   string `json:"longitude"`
	Latitude    string `json:"latitude"`
}

type Language struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}
