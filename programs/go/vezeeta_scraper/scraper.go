package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	Data "vezeeta_scraper/data"
	Result "vezeeta_scraper/result"

	"github.com/gocolly/colly"
)

const (
	DELAY                = 3
	APPOINTMENTS_BASEURL = "https://vezeeta-web-gateway.vezeetaservices.com/api/Schedule/Next?lastLoadeddate=%s&entityListContactId=%d&relatedBranchesContactIds="
	DATE_LAYOUT          = "02/01/2006"
)

// Check if a specific argument (flag) was passed
func isFlagPassed(name string) bool {
	found := false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}

func getData(link string, resume *bool) {
	s := strings.Split(link, "/")
	fname := s[len(s)-1]

	if *resume {
		if _, err := os.Stat(fmt.Sprintf("out/%s.json", fname)); !os.IsNotExist(err) {
			return
		}
	}

	var result Result.Result

	lang := &result.En
	var profile *Data.Profile

	c := colly.NewCollector()

	c.OnHTML("#__NEXT_DATA__", func(e *colly.HTMLElement) {
		var data Data.Data

		profile = &data.Props.InitialState.Profiles.DoctorProfileData.Profile

		err := json.Unmarshal([]byte(e.Text), &data)
		if err != nil {
			log.Fatalln(err)
		}

		lang.Name = profile.Name
		lang.Title = profile.Title
		lang.Vision = profile.Vision
		// lang.ProfessionalAffiliations = profile.ProfessionalAffiliations
		lang.DetailedDescription = profile.DetailedDescription
		lang.ShortDescription = profile.ShortDescription
		lang.ProfessionalTitle = profile.ProfessionalTitle
		lang.Specialties = []Result.Specialty{{
			Name: profile.MainSpecialty,
			Slug: profile.MainSpecialtySlug,
			Main: true,
		}}
		for _, specialty := range profile.SecondarySpecialties {
			lang.Specialties = append(lang.Specialties, Result.Specialty(specialty))
		}
		lang.FacilityTitle = profile.DoctorRatingViewModel.FacilityTitle
		for _, service := range profile.DoctorServices {
			lang.DoctorServices = append(lang.DoctorServices, Result.Service(service))
		}
		lang.Tags = profile.DoctorTags
		var InsuranceProviders []Data.InsuranceProvider
		if profile.InsuranceProvidersList != nil {
			InsuranceProviders = profile.InsuranceProvidersList
		} else {
			InsuranceProviders = profile.InsuranceProviders
		}
		for _, insurance := range InsuranceProviders {
			lang.InsuranceProviders = append(lang.InsuranceProviders, Result.InsuranceProvider(insurance))
		}
		for _, language := range profile.SpokenLanguages {
			lang.SpokenLanguages = append(lang.SpokenLanguages, Result.Language(language))
		}
	})
	c.OnScraped(func(r *colly.Response) {
		lang = &result.Ar
		link = strings.Replace(link, "/en/", "/ar/", 1)
		c.Visit(link)
	})

	c.Limit(&colly.LimitRule{
		Delay: DELAY * time.Second,
	})

	c.OnError(func(r *colly.Response, err error) {
		log.Println("Request URL:", r.Request.URL, "\nError:", err)
	})

	c.Visit(link)

	c.Wait()

	if profile == nil {
		return
	}

	result.Id = profile.Id
	result.Key = profile.Key
	result.IsNew = profile.IsNewDoctor
	result.Img = profile.Image
	result.OffersHomeVisits = profile.OffersHomeVisits
	result.Fees = profile.Fees
	result.DoctorRating = profile.DoctorRatingViewModel.DoctorOverallRating
	result.FacilityRating = profile.DoctorRatingViewModel.FacilityOverallRating
	result.WaitingTime = profile.DoctorRatingViewModel.WaitingTime
	result.RatingsCount = profile.DoctorRatingViewModel.RatingsCount
	result.IsTopRated = profile.DoctorRatingViewModel.IsTopRated
	result.Views = profile.Views
	result.FacilityImages = profile.FacilityImages
	result.Gender = profile.Gender
	result.RequirePrePayment = profile.RequirePrePayment

	// Get Appointments
	date := time.Now().AddDate(0, 0, 1)
	for times := 1; times <= 3; times++ {
		var appointment Result.Appointments
		for counter := 1; counter <= 4; counter++ {
			req, err := http.NewRequest("GET", fmt.Sprintf(APPOINTMENTS_BASEURL, date.Format(DATE_LAYOUT), result.Id), nil)
			if err != nil {
				log.Fatalln(err)
			}
			req.Header.Add("language", "en-EG")
			// req.Header.Add("accept", "application/json, text/plain, */*")
			// req.Header.Add("accept-language", "en-US,en;q=0.9")
			// req.Header.Add("brandkey", "7B2BAB71-008D-4469-A966-579503B3C719")
			// req.Header.Add("origin", "https://www.vezeeta.com")
			// req.Header.Add("referer", "https://www.vezeeta.com/")
			// req.Header.Add("regionid", "Africa/Cairo")
			// req.Header.Add("sec-fetch-dest", "empty")
			// req.Header.Add("sec-fetch-mode", "cors")
			// req.Header.Add("sec-fetch-site", "cross-site")
			// req.Header.Add("sec-gpc", "1")
			// req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36")
			client := &http.Client{}
			response, err := client.Do(req)
			if err != nil {
				log.Fatalln(err)
			}

			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatalln(err)
			}

			err = json.Unmarshal(body, &appointment)
			if err != nil {
				log.Fatalln(response.Request.URL, err)
			}
			response.Body.Close()

			if len(appointment.Days) == 0 {
				continue
			}
			result.Appointments.Days = append(result.Appointments.Days, appointment.Days...)
			date = date.AddDate(0, 0, 3)
			break
		}
	}
	if len(result.Appointments.Days) != 0 {
		result.Appointments.Days = result.Appointments.Days[:len(result.Appointments.Days)-2]
	}

	// Done
	res, err := json.Marshal(result)
	if err != nil {
		log.Fatalln(err)
	}

	err = ioutil.WriteFile(fmt.Sprintf("out/%s.json", fname), res, 0777)
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	search := flag.String("s", "", "query/ies to search for")
	filename := flag.String("f", "", "filename of file containing queries")
	deliminator := flag.String("d", "\n", "deliminator between each query in case of multiple queries")
	resume := flag.Bool("r", false, "continue where it left off")

	flag.Parse()

	var query string
	var queries []string

	if isFlagPassed("s") {
		query = strings.ToLower(*search)
	} else if isFlagPassed("f") {
		f, err := ioutil.ReadFile(*filename)
		if err != nil {
			log.Fatalln(err)
		}
		query = strings.ToLower(string(f))
	}
	query = strings.ReplaceAll(query, " ", "-")
	queries = strings.Split(query, *deliminator)

	for _, q := range queries {
		done := false
		page := 1
		for !done {
			if *resume {
				if _, err := os.Stat(fmt.Sprintf("out/%s_%d", q, page)); !os.IsNotExist(err) {
					page++
					continue
				}
			}

			url := fmt.Sprintf("https://www.vezeeta.com/en/doctor/%s/egypt?page=%d", q, page)

			c := colly.NewCollector()

			c.OnRequest(func(r *colly.Request) {
				log.Println("Visiting", r.URL)
			})
			c.OnError(func(r *colly.Response, err error) {
				log.Fatal("Request URL:", r.Request.URL, " failed with response:", string(r.Body), "\nError:", err)
			})
			c.OnHTML("[class*=NoResults]", func(_ *colly.HTMLElement) {
				done = true
			})
			c.OnHTML("[data-testid*=doctor-card]", func(doctorCard *colly.HTMLElement) {
				doctorCard.ForEach("[data-testid*=name--name]", func(i int, name *colly.HTMLElement) {
					getData(doctorCard.Request.AbsoluteURL(name.Attr("href")), resume)
				})
			})
			c.OnScraped(func(r *colly.Response) {
				log.Println("Finished", r.Request.URL)
				time.Sleep(DELAY * time.Second)
			})

			c.SetRequestTimeout(20 * time.Second)

			c.Visit(url)
			if !done {
				os.Create(fmt.Sprintf("out/%s_%d", q, page))
			}
			page++
		}
	}
}
