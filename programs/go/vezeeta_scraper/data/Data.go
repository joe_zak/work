package Data

type Data struct {
	Props Props `json:"props"`
}

type Props struct {
	InitialState InitialState `json:"initialState"`
}

type InitialState struct {
	Profiles Profiles `json:"profiles"`
}

type Profiles struct {
	DoctorProfileData DoctorProfileData `json:"doctorProfileData"`
}

type DoctorProfileData struct {
	Profile Profile `json:"Profile"`
}

type Profile struct {
	Id          int    `json:"EntityId"`
	Key         string `json:"EntityKey"`
	IsNewDoctor bool
	Vision      string
	// ProfessionalAffiliations []string
	DetailedDescription    string
	Name                   string `json:"EntityName"`
	ShortDescription       string
	Image                  string `json:"ImageUrl"`
	OffersHomeVisits       bool
	Fees                   string
	Title                  string `json:"DoctorNamePrefix"`
	ProfessionalTitle      string
	MainSpecialty          string `json:"MainSpecialityName"`
	MainSpecialtySlug      string `json:"MainSpecialtyUrl"`
	SecondarySpecialties   []Specialty
	DoctorTags             []string
	DoctorRatingViewModel  DoctorRatingViewModel
	DoctorServices         []Service
	Views                  int `json:"ViewedCount"`
	InsuranceProviders     []InsuranceProvider
	InsuranceProvidersList []InsuranceProvider
	FacilityImages         []string
	SpokenLanguages        []Language
	Gender                 bool // false male | true female
	RequirePrePayment      bool
}

type Specialty struct {
	Name string
	Slug string `json:"NameUrl"`
	Main bool
}

type DoctorRatingViewModel struct {
	DoctorOverallRating   float64 `json:"DoctorOverallRatingPercentage"`
	FacilityOverallRating float64
	FacilityTitle         string `json:"FacilityPrefixTitle"`
	WaitingTime           int    `json:"WaitingTimeTotalMinutesOverallRating"`
	RatingsCount          int
	IsTopRated            bool
	Contacts              []Contact
}

type Service struct {
	Name       string `json:"ServiceName"`
	Slug       string `json:"ServiceUrl"`
	IsBookable bool
	FixedPrice string
	OfferPrice string
}

type InsuranceProvider struct {
	Name        string
	NameEnglush string
	NameArabic  string
	Slug        string `json:"Url"`
	Image       string `json:"ImageUrl"`
	Longitude   string
	Latitude    string
}

type Contact struct {
	Type                 string `json:"ContactType"`
	Address              string
	CityName             string
	CityEnName           string
	AreaName             string
	AreaEnName           string
	AcceptOnlinePayment  bool
	AcceptGlobalBooking  bool
	FollowUpDetails      FollowUpDetails
	AcceptLoyaltyPayment bool
}

type FollowUpDetails struct {
	Fees     int `json:"FollowUpFees"`
	Duration int `json:"FollowUpDuration"`
}

type Language struct {
	Key  string `json:"LanguageKey"`
	Name string `json:"LanguageName"`
}

type Appointments struct {
	Days []Day
}

type Day struct {
	Name        string `json:"DayName"`
	IsAvailable bool   `json:"DayIsAvailable"`
}
