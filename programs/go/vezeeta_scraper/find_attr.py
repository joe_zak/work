import json
import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--attribute", required=True)
args = parser.parse_args()

if args.attribute:
	for fname in glob.glob("out/*.json"):
		with open(fname) as f:
			data = json.load(f)
			if data["en"][args.attribute]:
				print(fname)
