package main

import (
	"fmt"
	"image/color"
	"os"
	"strconv"

	"gocv.io/x/gocv"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Usage:\n\tfacial_detection [camera ID] [classifier XML file]")
		return
	}
	deviceID, _ := strconv.Atoi(os.Args[1])
	xmlFile := os.Args[2]
	webcam, err := gocv.VideoCaptureDevice(deviceID)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer webcam.Close()
	window := gocv.NewWindow("Face Detect")
	defer window.Close()
	img := gocv.NewMat()
	defer img.Close()
	blue := color.RGBA{0, 0, 255, 0}
	classifier := gocv.NewCascadeClassifier()
	defer classifier.Close()
	if !classifier.Load(xmlFile) {
		fmt.Printf("Error reading cascade file: %v\n", xmlFile)
		return
	}
	fmt.Printf("Start reading camera device: %v\n", deviceID)
	for {
		if ok := webcam.Read(&img); !ok {
			fmt.Printf("Cannot read device %d\n", deviceID)
			return
		}
		if img.Empty() {
			continue
		}
		rects := classifier.DetectMultiScale(img)
		// fmt.Printf("Found %d faces\n", len(rects))
		for _, r := range rects {
			gocv.Rectangle(&img, r, blue, 3)
			// size := gocv.GetTextSize("Human", gocv.FontHersheyPlain, 1.2, 2)
			// pt := image.Pt(r.Min.X+(r.Min.X/2)-(size.X/2), r.Min.Y-2)
			// gocv.PutText(&img, "Human", pt, gocv.FontHersheyPlain, 1.2, blue, 2)
		}
		window.IMShow(img)
		if window.WaitKey(1) >= 0 {
			break
		}
	}
}
