package main

import (
	"math"
)

func part1(intercept map[float64]float64) int {
	var smallest float64
	first := true
	for x, y := range intercept {
		x = math.Abs(x)
		y = math.Abs(y)
		if x+y < smallest || first {
			smallest = x + y
		}
		if first {
			first = false
		}
	}
	return int(smallest)
}
