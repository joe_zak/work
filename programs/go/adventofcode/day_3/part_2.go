package main

func part2(intercept map[float64]float64, s1 *Caple, s2 *Caple) int {
	var result float64
	first := true
	for x, y := range intercept {
		point := []float64{x, y}
		// fmt.Println(point)
		dist := s1.distance(point) + s2.distance(point)
		// fmt.Println(s1.distance(point), s2.distance(point))
		if dist < result || first {
			result = dist
		}
		if first {
			first = false
		}
	}
	return int(result)
}
