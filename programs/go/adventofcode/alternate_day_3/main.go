package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

type caple struct {
	points map[string]struct{}
}

func (s *caple) add(x int64, y int64) {
	s.points[fmt.Sprintf("%d:%d", x, y)] = struct{}{}
}

func (s *caple) intersect(s2 *caple) map[float64]float64 {
	result := make(map[float64]float64)
	for k := range s2.points {
		if s.has(k) {
			point := strings.Split(k, ":")
			x, _ := strconv.ParseFloat(point[0], 64)
			y, _ := strconv.ParseFloat(point[1], 64)
			result[x] = y
		}
	}
	return result
}

func (s *caple) has(point string) bool {
	_, ok := s.points[point]
	return ok
}

func newCaple() *caple {
	s := &caple{}
	s.points = make(map[string]struct{})
	return s
}

func parse(caple []string) *caple {
	points := newCaple()
	var x, y int64
	for _, value := range caple {
		num, _ := strconv.ParseInt(value[1:], 0, 0)
		points.add(x, y)
		switch value[0] {
		case 'U':
			for step := int64(1); step <= num; step++ {
				y++
				fmt.Println(y)
				points.add(x, y)
			}
		case 'R':
			for step := int64(1); step <= num; step++ {
				x++
				fmt.Println(x)
				points.add(x, y)
			}
		case 'D':
			for step := int64(1); step <= num; step++ {
				y--
				fmt.Println(y)
				points.add(x, y)
			}
		case 'L':
			for step := int64(1); step <= num; step++ {
				x--
				fmt.Println(x)
				points.add(x, y)
			}
		}
		fmt.Println()
	}
	return points
}

func main() {
	start := time.Now()
	// args := os.Args[1:]
	// file, _ := os.Open("input_3.txt")
	// defer file.Close()
	// scanner := bufio.NewScanner(file)
	// var caples []string
	// for scanner.Scan() {
	// 	caples = append(caples, scanner.Text())
	// }
	caples := []string{"R8,U5,L5,D3", "U7,R6,D4,L4"}
	// caples := []string{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"}
	caple1 := parse(strings.Split(caples[0], ","))
	caple2 := parse(strings.Split(caples[1], ","))
	intercept := caple1.intersect(caple2)
	fmt.Println(caple1)
	var smallest float64
	first := true
	for x, y := range intercept {
		x = math.Abs(x)
		y = math.Abs(y)
		if x+y < smallest || first {
			smallest = x + y
		}
		if first {
			first = false
		}
	}
	fmt.Println(int(smallest))
	fmt.Println(time.Since(start))
}
