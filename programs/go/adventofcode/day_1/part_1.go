package main

import (
	"math"
	"strconv"
)

func part1(models []string) int {
	var result int
	for _, value := range models {
		num, _ := strconv.Atoi(value)
		result += int(math.Floor(float64(num/3))) - 2
	}
	return result
}
