package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	args := os.Args[1:]
	file, err := os.Open("input_1.txt")
	if err != nil {
		panic(err)
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	file.Close()
	var models []string = strings.Split(string(data), "\n")
	var result int
	if args[0] == "1" {
		result = part1(models)
	} else if args[0] == "2" {
		result = part2(models)
	}
	fmt.Println(result)
}
