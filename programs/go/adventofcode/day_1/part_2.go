package main

import (
	"math"
	"strconv"
)

func part2(models []string) int {
	var result int
	for _, value := range models {
		num, _ := strconv.Atoi(value)
		fuel := int(math.Floor(float64(num/3))) - 2
		for fuel > 0 {
			result += fuel
			fuel = int(math.Floor(float64(fuel/3))) - 2
		}
	}
	return result
}
