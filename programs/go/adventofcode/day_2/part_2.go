package main

func part2(code []int) int {
	inst := make([]int, len(code))
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			copy(inst, code)
			inst[1] = noun
			inst[2] = verb
			for i := 0; i < len(inst); i += 4 {
				switch inst[i] {
				case 1:
					inst[inst[i+3]] = inst[inst[i+1]] + inst[inst[i+2]]
				case 2:
					inst[inst[i+3]] = inst[inst[i+1]] * inst[inst[i+2]]
				case 99:
					break
				default:
					break
				}
			}
			if inst[0] == 19690720 {
				return 100*noun + verb
			}
		}
	}
	return -1
}
