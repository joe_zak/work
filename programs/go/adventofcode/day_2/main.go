package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func toInt(str []string) []int {
	var nums []int
	for _, value := range str {
		num, err := strconv.Atoi(value)
		if err != nil {
			panic(err)
		}
		nums = append(nums, num)
	}
	return nums
}

func main() {
	args := os.Args[1:]
	data, err := ioutil.ReadFile("input_2.txt")
	if err != nil {
		panic(err)
	}
	code := toInt(strings.Split(string(data), ","))
	var result int
	if args[0] == "1" {
		result = part1(code)
	} else if args[0] == "2" {
		result = part2(code)
	}
	fmt.Println(result)
}
