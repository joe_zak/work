package main

func part1(code []int) int {
	code[1] = 12
	code[2] = 2
	for i := 0; i < len(code); i += 4 {
		switch code[i] {
		case 1:
			code[code[i+3]] = code[code[i+1]] + code[code[i+2]]
		case 2:
			code[code[i+3]] = code[code[i+1]] * code[code[i+2]]
		case 99:
			break
		default:
			break
		}
	}
	return code[0]
}
