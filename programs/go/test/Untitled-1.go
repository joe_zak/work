package main

import (
	"fmt"
	"io/ioutil"
)

type Person struct {
	name  string
	addr  string
	phone string
}

func main() {
	// := declare and assign (infers dtype)
	// ptr := new(float64) // <-- returns a pointer
	// *ptr = 1
	// //fmt.Printf("%.2f\n", *ptr)
	// //fmt.Println("Hello world")
	// //if 6 > 5 {
	// //	fmt.Println("True")
	// //}
	// //for i := 0; i < 5; i++ {
	// //	fmt.Println(i)
	// //}
	// // for {
	// // 	println("infinite loop")
	// // }
	// var x int = 1
	// switch x {
	// case 1:
	// 	println("one")
	// case 2:
	// 	println("two")
	// default:
	// 	println("unknown")
	// }
	// switch {
	// case x > 1:
	// 	println("greater")
	// case x < 1:
	// 	println("smaller")
	// default:
	// 	println("one")
	// }
	// var appleNum int
	// fmt.Println("Number of Apples?")
	// fmt.Scan(&appleNum)
	// fmt.Println(appleNum)
	// y := [...]string{"1", "2", "3"}
	// for i, v := range y {
	// 	fmt.Printf("index: %d, value: %s\n", i, v)
	// }
	// fmt.Println(len(y))
	// fmt.Printf("%T\n", y[0])
	// array := []int{1, 2, 3, 4, 5}
	// fmt.Println(array)
	// array = append(array, 6)
	// fmt.Println(array)
	// var p1 Person
	// p2 := new(Person)
	// _ = p2
	// p3 := Person{name: "joe", addr: "idk", phone: "123456"}
	// p1.name = "joe"
	// fmt.Println(p1.name)
	// fmt.Println(p3)
	// test := false
	// if !test {
	// 	fmt.Println("true")
	// }
	// list := "arbitrary bytes"
	// fmt.Printf("%t\n", list[0] == 'a')
	// fmt.Printf("%T\n", struct{}{})
	// text, err := http.Get("http://www.uci.edu")
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(text)
	// conn, _ := net.Dial("tcp", "uci.edu:80")
	// fmt.Println(conn)
	// barr, err := json.Marshal(p3)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(barr)
	// error := json.Unmarshal(barr, &p1)
	// if err != nil {
	// 	panic(error)
	// }
	dat, err := ioutil.ReadFile("test.txt")
	if err != nil {
		panic(err)
	}
	fmt.Println(dat)
	// dat = []byte("hello, world\n")
	// e := ioutil.WriteFile("test.txt", dat, 0777)
	// if e != nil {
	// 	panic(e)
	// }
	// f, _ := os.Open("test.txt")
	// barr := make([]byte, 5)
	// nb, _ := f.Read(barr)
	// f.Close()
	// fmt.Println(nb)
	// fmt.Println(barr)
	// f, _ := os.Create("outfile.txt")
	// barr := []byte{1, 2, 3}
	// nb, _ := f.Write(barr)
	// n, _ := f.WriteString("Hi")
	// _ = nb
	// _ = n
	// nsli := []int{4, 2, 1, 3}
	// sort.IntSlice(nsli).Sort()
	// fmt.Printf("%d %T\n", nsli, nsli)
}
