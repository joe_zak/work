import glob
import json

for fname in glob.glob("./out/*.json"):
	with open(fname, "r+") as file:
		try:
			data = json.load(file)
			data["meta_data"] += [
				{
					"key": "is_fbs",
					"value": data["is_fbs"]
				},
				{
					"key": "isinternationalseller",
					"value": data["isInternationalSeller"]
				},
				{
					"key": "seller_note",
					"value": data["seller_note"]
				},
				{
					"key": "souq_id",
					"value": data["souq_id"]
				}
			]
			del data["is_fbs"]
			del data["isInternationalSeller"]
			del data["seller_note"]
			del data["souq_id"]
			file.seek(0)
			json.dump(data, file)
			file.truncate()
		except KeyError:
			print(fname)
			pass