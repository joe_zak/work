import glob
import json

ids = {}
fnames = {}
for fname in glob.glob("out/*.json"):
	with open(fname) as f:
		data = json.load(f)
	if data["id"] in ids.keys():
		ids[data["id"]].add(fname)
	else:
		ids[data["id"]] = set([fname])

for i in ids:
	if len(ids[i]) > 1:
		print(ids[i])

# print(ids)