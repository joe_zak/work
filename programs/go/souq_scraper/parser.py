import json
import glob
import argparse
import os

from api import post

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--filename", type=str, default="", help="upload files containing FILENAME")
parser.add_argument("-c", "--category", type=str, default="", help="upload files from CATEGORY")
parser.add_argument("-l", "--limit", type=int, default=500, help="maximum number of files to upload")
args = parser.parse_args()

counter = 1
for fname in glob.glob(f"./out/*{args.filename}*.json"):
	if not os.path.isfile(done := f"{fname[:-5]}_posted"):
		print(f"Loading {fname}")
		with open(fname) as file:
			data = json.load(file)
			if args.category and data["categories"][0]["name"] == args.category:
				if counter > args.limit:
					break
				post([data])
				os.mknod(done)
				print(f"{fname} done\n")
				counter += 1
	else:
		print(f"{fname} already done\n")
