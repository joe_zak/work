# Souq Scraper written in Golang

This is a Golang program that uses Souq's search api to find products data and stores the result in json format.

## Structure

```
.
├── api.py
├── count.py
├── go.mod
├── go.sum
├── out
│   └── *.json
├── parser.py
├── README.md
├── run.ps1
├── run.sh
├── scraper.go
└── UPLOAD.md
```

`count.py`: shows the number of products in each category and the total

`api.py`: is responsible for connecting and uploading data to the API

`parser.py`: reads the files and passes the data to `api.py`

`run.*`: responsible for running the script until it exits successfully

## Usage

```
Usage of ./scraper:
  -d, --deliminator string
    	deliminator between each query in case of multiple queries (default "\n")
  -f, --filename string
    	filename of file containing queries
  -r, --resume	continue where it left off
  -s, --search string
    	query/ies to search for
```

## Notes

The filename is the unit id which seems to be unique while the souq id attribute is its item id which to be duplicated in case of same product but different seller.
