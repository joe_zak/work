# Uploading data

A script that uploads data inside out folder to the WooCommerce server using it's API

## Usage

```
usage: parser.py [-h] [-f FILENAME] [-c CATEGORY] [-l LIMIT]

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        upload files containing FILENAME
  -c CATEGORY, --category CATEGORY
                        upload files from CATEGORY
  -l LIMIT, --limit LIMIT
                        maximum number of files to upload
```

# Data format

```json
{
	"name": string,
	"regular_price": string e.g. 344.00,
	"sale_price": string e.g. 269.00,
	"shipping_class": string {"paid" | "free"},
	"categories": [{ "name": string }],
	"meta_data": [
		{ "key": "product_brand_in_english", "value": string },
		{ "key": "product_brand_in_arabic", "value": string },
		{ "key": "souq_id", "value": int },
		{ "key": "is_fbs", "value": string {"true" | "false"} },
		{ "key": "isinternationalseller", "value": string {"true" | "false"} },
		{ "key": "seller_note", "value": string },
		{ "key": "average_rating", "value": string },
		{ "key": "average_rating_total", "value": string },
		{ "key": "seller", "value": string },
		{ "key": "estimated_delivery", "value": string },
		{ "key": "condition", "value": string },
		{ "key": "product_specifications_in_english", "value": html string },
		{ "key": "product_title_in_arabic", "value": string },
		{ "key": "product_specifications_in_arabic", "value": html string },
		{ "key": "product_description_in_arabic", "value": string },
		{ "key": "product_short_description_in_arabic", "value": html string }
	],
	"attributes": [
		{
			"name": string,
			"visible": boolean,
			"variation": boolean,
			"options": [string]
		}
	],
	"tags": null | [{ "name": string }],
	"images": [{ "src": url string }],
	"description": html string,
	"short_description": html string,
	"stock_status": string {"instock" | "outofstock"}
}
```
