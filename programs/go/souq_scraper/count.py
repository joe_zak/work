import glob
import json

result = {}
for fname in glob.glob("out/*.json"):
	with open(fname) as f:
		data = json.load(f)
	categories = data["categories"][0]
	if result.get(categories["name"]):
		result[categories["name"]] += 1
	else:
		result[categories["name"]] = 1

print(result)
print(sum(result.values()))