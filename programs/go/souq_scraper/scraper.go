package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

const (
	BASEURL = "https://egypt.souq.com/eg-en/%s/s/?as=1&action=page&page=%d&section=%d"
	// Custom fields
	PRODUCT_TITLE_IN_ARABIC             = "product_title_in_arabic"
	PRODUCT_DESCRIPTION_IN_ARABIC       = "product_description_in_arabic"
	PRODUCT_SHORT_DESCRIPTION_IN_ARABIC = "product_short_description_in_arabic"
	PRODUCT_SPECIFICATIONS_IN_ARABIC    = "product_specifications_in_arabic"
	PRODUCT_SPECIFICATIONS_IN_ENGLISH   = "product_specifications_in_english"
	PRODUCT_BRAND_IN_ENGLISH            = "product_brand_in_english"
	PRODUCT_BRAND_IN_ARABIC             = "product_brand_in_arabic"
	SOUQ_ID                             = "souq_id"
	SOUQ_SELLER                         = "souq_seller"
	SELLER_NOTE                         = "seller_note"
	IS_FBS                              = "is_fbs"
	IS_INTERNATIONAL_SELLER             = "isinternationalseller"
	AVERAGE_RATING                      = "average_rating"
	AVERAGE_RATING_TOTAL                = "average_rating_total"
	CONDITION                           = "condition"
	ESTIMATED_DELIVERY                  = "estimated_delivery"
)

type Data struct {
	Success  bool     `json:"success"`
	JsonData JsonData `json:"jsonData"`
}

type JsonData struct {
	Units []Unit `json:"units"`
}

type Unit struct {
	PrimaryLink             string      `json:"primary_link"`
	ItemID                  int         `json:"item_id"`
	UnitID                  int         `json:"unit_id"`
	FreeShippingEligibility bool        `json:"free_shipping_eligibility"`
	IsFBS                   bool        `json:"is_fbs"`
	Title                   interface{} `json:"title"`
	AverageRating           interface{} `json:"average_rating"`
	AverageRatingTotal      int         `json:"average_rating_total"`
	Category                string      `json:"item_type_label"`
	Price                   string      `json:"price_formatted"`
	MarketPrice             string      `json:"market_price_formatted"`
	Manufacturer            interface{} `json:"manufacturer"`
	Shipping                bool        `json:"shipping"`
	IsInternationalSeller   bool        `json:"isInternationalSeller"`
}

type Result struct {
	Name string `json:"name"`
	// AverageRating      interface{} `json:"average_rating"`
	// AverageRatingTotal int         `json:"average_rating_total"`
	RegularPrice  string      `json:"regular_price"`
	SalePrice     string      `json:"sale_price"`
	ShippingClass string      `json:"shipping_class"`
	Categories    []Category  `json:"categories"`
	MetaData      []MetaData  `json:"meta_data"`
	Attributes    []Attribute `json:"attributes"`
	Tags          []Tag       `json:"tags"`
	Images        []Image     `json:"images"`
	// EstimatedDelivery  string      `json:"estimated_delivery"`
	// Condition          string      `json:"condition"`
	// Seller             string      `json:"seller"`
	Description      string `json:"description"`
	ShortDescription string `json:"short_description"`
	StockStatus      string `json:"stock_status"`
}

type Category struct {
	Name string `json:"name"`
}

type MetaData struct {
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

func (r *Result) appendMeta(m MetaData) {
	r.MetaData = append(r.MetaData, m)
}

type Attribute struct {
	Name      string   `json:"name"`
	Visible   bool     `json:"visible"`
	Variation bool     `json:"variation"`
	Options   []string `json:"options"`
}

func (r *Result) appendAttr(a Attribute) {
	r.Attributes = append(r.Attributes, a)
}

type Tag struct {
	Name string `json:"name"`
}

func (r *Result) appendTag(t Tag) {
	r.Tags = append(r.Tags, t)
}

type Image struct {
	Src string `json:"src"`
}

func (r *Result) appendImg(img Image) {
	r.Images = append(r.Images, img)
}

// Check if a specific argument (flag) was passed
func isFlagPassed(name string) bool {
	found := false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}

// check if a string contains numbers
func isNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

// Get english product data
func getEngData(link string, res *Result, id int) {
	rep := strings.NewReplacer(
		" For ", " ",
		"&Amp; ", "",
		" And ", " ",
		" Of ", " ",
		" Or ", " ",
		" With ", " ",
		" To ", " ",
		" From ", " ",
		", ", " ",
		",", "",
		"- ", "",
		"(", "",
		")", "",
		"،", "",
	)

	c := colly.NewCollector()

	c.OnResponse(func(r *colly.Response) {
		log.Println(r.Request.URL)
	})

	// parse product tags
	c.OnHTML("meta[name='keywords']", func(e *colly.HTMLElement) {
		content := e.Attr("content")
		content = strings.Title(content)
		content = rep.Replace(content)
		content = strings.TrimSpace(content)

		tags := strings.Split(content, " ")

		var stack string
		for _, tag := range tags {
			if len(tag) > 0 {
				if isNumeric(tag) {
					stack = tag
				} else if len(stack) > 0 {
					res.appendTag(Tag{Name: stack + " " + tag})
				} else {
					res.appendTag(Tag{Name: tag})
				}
			}
		}
	})

	// parse product page
	c.OnHTML("div.product_content", func(product_content *colly.HTMLElement) {
		images := product_content.DOM.Find("div.product-header div.img-bucket img")
		images.Each(func(i int, image *goquery.Selection) {
			dataUrl, exists := image.Attr("data-url")
			if exists {
				res.appendImg(Image{Src: dataUrl})
			} else {
				src, exists := image.Attr("src")
				if exists {
					res.appendImg(Image{Src: src})
				}
			}
		})

		product_actions := product_content.DOM.Find("div.product-actions")
		if product_actions.Length() > 0 {
			res.StockStatus = "instock"
			res.appendMeta(MetaData{
				Key:   ESTIMATED_DELIVERY,
				Value: strings.TrimSpace(product_actions.Find("div.estimated-delivery small.estimated span").Text()),
			})
			res.appendMeta(MetaData{
				Key:   CONDITION,
				Value: product_actions.Find("dl.condition-box dd.unit-condition").Text(),
			})
			res.appendMeta(MetaData{
				Key:   SOUQ_SELLER,
				Value: product_actions.Find("span.unit-seller-link a").Text(),
			})
			res.appendMeta(MetaData{
				Key:   SELLER_NOTE,
				Value: product_actions.Find("dl.seller-note span").Text(),
			})
		} else {
			res.StockStatus = "outofstock"
		}

		product_connections := product_content.DOM.Find("div.product-info div.product-connections > *")
		var current string
		product_connections.Each(func(i int, child *goquery.Selection) {
			class, _ := child.Attr("class")
			if strings.Contains(class, "connection-title") {
				current = child.Text()
			} else if strings.Contains(class, "connection-stand") {
				res.appendAttr(Attribute{
					Name:      current,
					Visible:   true,
					Variation: true,
					Options:   strings.Split(strings.TrimSpace(child.Text()), "\n"),
				})
			}
		})

		product_details := product_content.DOM.Find("section.band div.product-details")
		specs := product_details.Find("div#specs-full")
		if specs.Length() > 0 {
			dl, _ := specs.Find("dl").Html()
			res.appendMeta(MetaData{
				Key:   PRODUCT_SPECIFICATIONS_IN_ENGLISH,
				Value: fmt.Sprintf("<div class='specs_tab_content'>%s</div>", dl),
			})
		}
		children := specs.Find("dl > *")
		children.Each(func(i int, child *goquery.Selection) {
			if child.Is("dt") {
				current = child.Text()
			} else if child.Is("dd") {
				res.appendAttr(Attribute{
					Name:      current,
					Visible:   true,
					Variation: false,
					Options:   []string{child.Text()},
				})
			}
		})

		description := product_details.Find("div#description-full")
		d, _ := description.Html()
		res.Description = strings.TrimSpace(d)
		short_description := product_details.Find("div#description-short")
		sd, _ := short_description.Html()
		res.ShortDescription = strings.TrimSpace(sd)
	})

	c.OnError(func(r *colly.Response, err error) {
		log.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
		// Some pages keeped redirecting too many times
		if r.StatusCode == 301 {
			os.Create(fmt.Sprintf("out/%d_locked", id))
		}
		os.Exit(1)
	})

	c.SetRequestTimeout(20000000000)

	c.Visit(link)
}

// Get arabic product data
func getAraData(link string, res *Result, id int) {
	c := colly.NewCollector()

	c.OnResponse(func(r *colly.Response) {
		log.Println(r.Request.URL)
	})

	c.OnHTML("div.product_content", func(product_content *colly.HTMLElement) {
		product_header := product_content.DOM.Find("div.product-header")
		var title string
		if product_header.Length() > 0 {
			title = product_header.Find("h1").Text()
		} else {
			title = product_header.Find("div.product-title h6").Text()
		}
		res.appendMeta(MetaData{
			Key:   PRODUCT_TITLE_IN_ARABIC,
			Value: title,
		})

		product_details := product_content.DOM.Find("section.band div.product-details")
		specs := product_details.Find("div#specs-full")
		if specs.Length() > 0 {
			dl, _ := specs.Find("dl").Html()
			res.appendMeta(MetaData{
				Key:   PRODUCT_SPECIFICATIONS_IN_ARABIC,
				Value: fmt.Sprintf("<div class='specs_tab_content'>%s</div>", strings.TrimSpace(dl)),
			})
		}

		description := product_details.Find("div#description-full")
		d, _ := description.Html()
		res.appendMeta(MetaData{
			Key:   PRODUCT_DESCRIPTION_IN_ARABIC,
			Value: strings.TrimSpace(d),
		})
		short_description := product_details.Find("div#description-short")
		sd, _ := short_description.Html()
		res.appendMeta(MetaData{
			Key:   PRODUCT_SHORT_DESCRIPTION_IN_ARABIC,
			Value: strings.TrimSpace(sd),
		})
	})

	c.OnError(func(r *colly.Response, err error) {
		log.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
		// Some pages keeped redirecting too many times
		if r.StatusCode == 301 {
			os.Create(fmt.Sprintf("out/%d_locked", id))
		}
		os.Exit(1)
	})

	c.SetRequestTimeout(20000000000)

	c.Visit(link)
}

// parse product data
func parse(data *Data, resume *bool) {
	for _, unit := range data.JsonData.Units {
		// exit if product in locked
		if _, err := os.Stat(fmt.Sprintf("out/%d_locked", unit.UnitID)); !os.IsNotExist(err) {
			return
		}
		// exit if product already downloaded
		if *resume {
			if _, err := os.Stat(fmt.Sprintf("out/%d.json", unit.UnitID)); !os.IsNotExist(err) {
				return
			}
		}
		var res Result

		res.appendMeta(MetaData{
			Key:   SOUQ_ID,
			Value: fmt.Sprint(unit.ItemID),
		})
		res.appendMeta(MetaData{
			Key:   IS_FBS,
			Value: fmt.Sprint(unit.IsFBS),
		})

		switch v := unit.Title.(type) {
		case float64:
			res.Name = strconv.Itoa(int(v))
		case string:
			res.Name = v
		}

		res.appendMeta(MetaData{
			Key:   AVERAGE_RATING,
			Value: unit.AverageRating,
		})
		res.appendMeta(MetaData{
			Key:   AVERAGE_RATING_TOTAL,
			Value: unit.AverageRatingTotal,
		})
		if len(unit.MarketPrice) > 0 {
			res.RegularPrice = unit.MarketPrice
			if len(unit.Price) > 0 {
				res.SalePrice = unit.Price
			}
		} else {
			res.RegularPrice = unit.Price
		}
		if unit.FreeShippingEligibility {
			res.ShippingClass = "free"
		} else {
			res.ShippingClass = "paid"
		}
		res.appendMeta(MetaData{
			Key:   IS_INTERNATIONAL_SELLER,
			Value: fmt.Sprint(unit.IsInternationalSeller),
		})

		res.Categories = []Category{{Name: unit.Category}}

		var manufacturer string
		switch v := unit.Manufacturer.(type) {
		case float64:
			manufacturer = strconv.Itoa(int(v))
		case string:
			manufacturer = v
		}
		res.appendMeta(MetaData{
			Key:   PRODUCT_BRAND_IN_ENGLISH,
			Value: manufacturer,
		})
		res.appendMeta(MetaData{
			Key:   PRODUCT_BRAND_IN_ARABIC,
			Value: manufacturer,
		})
		res.Attributes = []Attribute{{
			Name:      "brand",
			Visible:   true,
			Variation: false,
			Options:   []string{manufacturer},
		}}

		en := unit.PrimaryLink
		getEngData(en, &res, unit.UnitID)

		ar := strings.Replace(unit.PrimaryLink, "/eg-en", "/eg-ar", 1)
		getAraData(ar, &res, unit.UnitID)

		// convert struct to json string
		result, err := json.Marshal(res)
		if err != nil {
			log.Fatalln(err)
		}
		// write json data to file
		err = ioutil.WriteFile(fmt.Sprintf("out/%d.json", unit.UnitID), result, 0777)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func main() {
	search := flag.String("s", "", "query/ies to search for")
	filename := flag.String("f", "", "filename of file containing queries")
	deliminator := flag.String("d", "\n", "deliminator between each query in case of multiple queries")
	resume := flag.Bool("r", false, "continue where it left off")

	flag.Parse()

	var query string
	var queries []string

	if isFlagPassed("s") {
		query = strings.ToLower(*search)
	} else if isFlagPassed("f") {
		f, err := ioutil.ReadFile(*filename)
		if err != nil {
			log.Fatalln(err)
		}
		query = strings.ToLower(string(f))
	}
	query = strings.ReplaceAll(query, " ", "-")
	queries = strings.Split(query, *deliminator)

	var data Data
	for _, q := range queries {
		page := 1
		// each page contains two sections
		sections := []int{1, 2}
	loop:
		for {
			for _, section := range sections {
				if _, err := os.Stat(fmt.Sprintf("out/%s_%d_%d", q, page, section)); err == nil && *resume {
					continue
				}
				req, err := http.NewRequest("GET", fmt.Sprintf(BASEURL, q, page, section), nil)
				if err != nil {
					log.Fatalln(err)
				}
				req.Header.Add("X-Requested-With", "XMLHttpRequest")
				client := &http.Client{}
				res, err := client.Do(req)
				if err != nil {
					log.Fatalln(err)
				}

				body, err := ioutil.ReadAll(res.Body)
				if err != nil {
					log.Fatalln(err)
				}
				res.Body.Close()

				err = json.Unmarshal(body, &data)
				if err != nil {
					log.Fatalln(res.Request.URL, err)
				}

				if len(data.JsonData.Units) == 0 {
					break loop
				}

				parse(&data, resume)

				log.Printf("%d:%d done", page, section)
				os.Create(fmt.Sprintf("out/%s_%d_%d", q, page, section))
			}
			page++
		}
	}
}
