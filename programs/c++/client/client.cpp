#include <winsock2.h>
#include <iostream>

int main(int argc, char **argv){
    WSADATA wsadata;
    SOCKET s;
    SOCKADDR_IN server_addr, this_sender_info;
    unsigned int port = 8080;
    int ret_code;
    char buffer[1024] = "this is a test string";
    int byte_sent, nlen;
    WSAStartup(MAKEWORD(2, 2), &wsadata);
    std::cout << "Client: Winsock DLL status is " << wsadata.szSystemStatus << std::endl;
    s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (s == INVALID_SOCKET){
        std::cout << "Client: socket() failed! Error code: " << WSAGetLastError() << std::endl;
        WSACleanup();
        return -1;
    }
    else
        std::cout << "Client: socket() is OK!\n";
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    ret_code = connect(s, (SOCKADDR *)&server_addr, sizeof(server_addr));
    if (ret_code != 0){
        std::cout << "Client: connect() failed! Error code: " << WSAGetLastError() << std::endl;
        closesocket(s);
        WSACleanup();
        return -1;
    }
    else {
        std::cout << "Client: connect() is OK, got connected...\n";
        std::cout << "Client: Ready for sending and/or receiving data...\n";
    }
    getsockname(s, (SOCKADDR *)&server_addr, (int *)sizeof(server_addr));
    std::cout << "Client: Receiver IP(s) used: " << inet_ntoa(server_addr.sin_addr) << std::endl;
    std::cout << "Client: Receiver port used: " << htons(server_addr.sin_port) << std::endl;
    byte_sent = send(s, buffer, strlen(buffer), 0);
    if (byte_sent == SOCKET_ERROR)
        std::cout << "Client: send() error " << WSAGetLastError() << std::endl;
    else {
        std::cout << "Client: send() is OK - bytes sent: " << byte_sent << std::endl;
        memset(&this_sender_info, 0, sizeof(this_sender_info));
        nlen = sizeof(this_sender_info);
        getsockname(s, (SOCKADDR *)&this_sender_info, &nlen);
        std::cout << "Client: Sender IP(s) used: " << inet_ntoa(this_sender_info.sin_addr) << std::endl;
        std::cout << "Client: Sender port used: " << htons(this_sender_info.sin_port) << std::endl;
        std::cout << "Client: Those bytes represent: \"" << buffer << "\"\n";
    }
    if (shutdown(s, SD_SEND) != 0)
        std::cout << "Client: Well, there is something wrong with the shutdown(). The error code: " << WSAGetLastError() << std::endl;
    else
        std::cout << "Client: shutdown() looks OK...\n";
    if (closesocket(s) != 0)
        std::cout << "Client: Cannot close socket. Error code: " << WSAGetLastError() << std::endl;
    else
        std::cout << "Client: Closing socket...\n";
    if (WSACleanup() != 0)
        std::cout << "Client: WSACleanup() failed!...\n";
    else
        std::cout << "Client: WSACleanup() is OK..\n";
    return 0;
}
