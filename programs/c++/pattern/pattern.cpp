#include <iostream>
#include <string>

using namespace std;

int main() {
	string input;
	cout << "Text: ";
	cin >> input;
	int length = input.size();
	cout << endl;
	cout << string(length+10, '-') << endl;
	cout << '|' << "    " << string(length, '*') << "    " << '|' << endl;
	cout << '|' << "  " << "**" << input << "**" << "  " << '|' << endl;
	cout << '|' << "      " << string(length-4, '*') << "      " << '|' << endl;
	cout << string(length+10, '-') << endl;
}