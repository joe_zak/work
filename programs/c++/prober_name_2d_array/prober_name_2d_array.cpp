#include <iostream>
#include <string.h>

using namespace std;

int main() {
	// ["Ahmad"
	//  "Ayman"
	//  "Youssef"
	//  "Joe"
	//  "Mohammed"]
	char input[5][50];
	char proper_in[5][50];
	for (int i = 0; i < 5; i++) {
		// read input
		cout << "Name: ";
		cin >> input[i];
		int len = strlen(input[i]);
		int c = 0;
		// remove special characters
		for (int j = 0; j < len; j++) {
			if (isalnum(input[i][j])) {
				proper_in[i][c] = input[i][j];
				c++;
			}
		}
		// add terminator
		proper_in[i][c] = '\0';
		// make the first character upper case
		proper_in[i][0] = toupper(proper_in[i][0]);
		cout << proper_in[i] << endl;
	}
}