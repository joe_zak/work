#include <chrono>
#include <cstdlib>
#include <iostream>
#include <string.h>

void show(int range=95, int add=32){
	std::cout << (char)((rand() % range) + add);
}

int main(int argc, char **argv){
	srand((unsigned int)std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());
	if (argc == 1){
		for (int i = 1; i <= 20; i++){
			show();
		}
	}
	else if (argc > 1){
		unsigned int length = 20;
		bool s = false;
		bool n = false;
		bool a = false;
		for (int i = 1; i < argc; i++){
			if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--length")){
				length = strtol(argv[i+1], NULL, 0);
			} else if (!strcmp(argv[i], "-s") || !strcmp(argv[i], "--special")){
				s = true;
			} else if (!strcmp(argv[i], "-n") || !strcmp(argv[i], "--numric")){
				n = true;
			} else if (!strcmp(argv[i], "-a") || !strcmp(argv[i], "--alphabet")){
				a = true;
			} else {
				std::cout << "Usage: " << argv[0] << " [option/s]\n\noptions:\n\t-l, --length\tspecify the number of characters (default -l 20)\n\t-s, --special\tfor special characters (default is true)\n\t-n, --numric\tfor numric characters (default is true)\n\t-a, --alphabet\tfor alphabet characters (default is true)\n";
				return 0;
			}
		}
		for (int i = 1; i <= length; i++){
			if (s && n && a){
				show();
			}
			else if (s && !n && !a){
				switch (rand() % 4){
					case 0:
						show(16, 32);
						break;
					case 1:
						show(7, 58);
						break;
					case 2:
						show(6, 91);
						break;
					case 3:
						show(4, 123);
						break;
				}
			}
			else if (!s && n && !a){
				show(10, 48);
			}
			else if (!s && !n && a){
				switch (rand() % 2){
					case 0:
						show(26, 65);
						break;
					case 1:
						show(26, 97);
						break;
				}
			}
			else if (s && n && !a){
				switch (rand() % 3){
					case 0:
						show(33, 32);
						break;
					case 1:
						show(6, 91);
						break;
					case 2:
						show(4, 123);
						break;
				}
			}
			else if (s && !n && a){
				switch (rand() % 2){
					case 0:
						show(16, 32);
						break;
					case 1:
						show(69, 58);
						break;
				}
			}
			else if (!s && n && a){
				switch (rand() % 3){
					case 0:
						show(10, 48);
						break;
					case 1:
						show(26, 65);
						break;
					case 2:
						show(26, 97);
						break;
				}
			}
			else {
				show();
			}
		}
	}
	std::cout << std::endl;
	return 0;
}
