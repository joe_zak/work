#include <winsock2.h>
#include <iostream>

int recv_timeout_tcp(SOCKET socket, long sec, long usec){
    struct timeval timeout;
    struct fd_set fds;
    timeout.tv_sec = sec;
    timeout.tv_usec = usec;
    FD_ZERO(&fds);
    FD_SET(socket, &fds);
    return select(0, &fds, 0, 0, &timeout);
}

int main(int argc, char **argv){
    WSADATA wsadata;
    SOCKET s, conn;
    SOCKADDR_IN server_addr, sender_addr;
	int port = 8080;
	char buffer[1024];
	int byte_recv, nlen, select_timing;
	if (WSAStartup(MAKEWORD(2, 2), &wsadata) != 0){
        std::cout << "Server: WSAStartup failed with error " << WSAGetLastError() << std::endl;
        return 1;
	}
    else {
        std::cout << "Server: The Winsock DLL found!\n";
        std::cout << "Server: The current status is " << wsadata.szSystemStatus << std::endl;
    }
    if (LOBYTE(wsadata.wVersion) != 2 || HIBYTE(wsadata.wVersion) != 2){
        std::cout << "Server: The DLL do not support the Winsock version " << LOBYTE(wsadata.wVersion) << "." << HIBYTE(wsadata.wVersion) << std::endl;
        WSACleanup();
        return 1;
    }
    else {
        std::cout << "Server: The DLL supports the Winsock version " << LOBYTE(wsadata.wVersion)<< "." << HIBYTE(wsadata.wVersion) << std::endl;
        std::cout << "Server: The highest version this DLL can support is " << LOBYTE(wsadata.wHighVersion) << "." << HIBYTE(wsadata.wHighVersion) << std::endl;
    }
    s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (s == INVALID_SOCKET){
        std::cout << "Server: Error at socket(), error code: " << WSAGetLastError() << std::endl;
        WSACleanup();
        return 1;
    }
    else
        std::cout << "Server: socket() is OK!\n";
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    if (bind(s, (SOCKADDR *)&server_addr, sizeof(server_addr)) == SOCKET_ERROR){
        std::cout << "Server: bind() failed! Error code: " << WSAGetLastError() << std::endl;
        closesocket(s);
        WSACleanup();
        return 1;
    }
    else
        std::cout << "Server: bind() is OK!\n";
    if (listen(s, 5) == SOCKET_ERROR){
        std::cout << "Server: listen(): Error listening on socket " << WSAGetLastError() << std::endl;
        closesocket(s);
        return 1;
    }
    else
        std::cout << "Server: listen() is OK, I'm listening for connections...\n";
    //select_timing = recv_timeout_tcp(s, 10, 10);
    select_timing = 1;
    switch (select_timing){
        case 0:
            std::cout << "\nServer: Timeout lor while waiting you retard client!...\n";
            break;
        case -1:
            std::cout << "\nServer: Some error encountered with code number: " << WSAGetLastError() << std::endl;
            break;
        default:
            while (1){
                conn = SOCKET_ERROR;
                while (conn == SOCKET_ERROR){
                    conn = accept(s, NULL, NULL);
                    std::cout << "\nServer: accept() is OK...\n";
                    std::cout << "Server: New client got connected, ready to receive and send data...\n";
                    byte_recv = recv(conn, buffer, sizeof(buffer), 0);
                    if (byte_recv > 0){
                        std::cout << "Server: recv() looks fine....\n";
                        getsockname(s, (SOCKADDR *)&server_addr, (int *)sizeof(server_addr));
                        std::cout << "Server: Receiving IP(s) used: " << inet_ntoa(server_addr.sin_addr) << std::endl;
                        std::cout << "Server: Receiving port used: " << htons(server_addr.sin_port) << std::endl;
                        std::cout << "Server: Bytes received: " << byte_recv << std::endl;
                        std::cout << "Server: Those bytes are: \"";
                        for (int i = 0; i < byte_recv; i++)
                            std::cout << buffer[i];
                        std::cout << "\"";
                    }
                    else if (byte_recv == 0)
                        std::cout << "Server: Connection closed!\n";
                    else
                        std::cout << "Server: recv() failed with error code: " << WSAGetLastError() << std::endl;
                }
                if (shutdown(conn, SD_SEND) != 0)
                    std::cout << "\nServer: Well, there is something wrong with the shutdown(). The error code: %ld\n" << WSAGetLastError() << std::endl;
                else
                    std::cout << "\nServer: shutdown() looks OK...\n";
                //if (recv_timeout_tcp(s, 15, 0) == 0)
                //    break;
            }
    }
    std::cout << "\nServer: The listening socket is timeout...\n";
    if (closesocket(s) != 0)
        std::cout << "Server: Cannot close socket. Error code: " << WSAGetLastError() << std::endl;
    else
        std::cout << "Server: Closing socket...\n";
    if (WSACleanup() != 0)
        std::cout << "Server: WSACleanup() failed! Error code: " << WSAGetLastError() << std::endl;
    else
        std::cout << "Server: WSACleanup() is OK...\n";
    return 0;
}
