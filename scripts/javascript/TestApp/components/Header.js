import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

export default function Header(props) {
  return (
    <>
      <Text>Header</Text>
    </>
  );
}

Header.propTypes = {
  style: PropTypes.object,
};
