import { ThemeProvider } from '@react-navigation/native';
import React from 'react-native';
import { SafeAreaView, ScrollView, StatusBar, Text, View } from 'react-native';
import Header from './Header';

export default function HomeScreen(props) {
  return (
    <ThemeProvider>
      <Header />
      <View>
        <Text>Hello world</Text>
      </View>
    </ThemeProvider>
  );
}
