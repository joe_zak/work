const puppeteer = require("puppeteer-core");
const cheerio = require("cheerio");
const fs = require("fs");
// const argv = require("yargs")(process.argv.slice(2)).boolean(["r"]);

let queries = fs.readFileSync("cats.txt", "utf8").split(/\r?\n/);

(async () => {
	const browser = await puppeteer.launch({
		executablePath: "/usr/bin/brave",
		headless: false,
	});
	const main = await browser.newPage();

	for (query of queries) {
		let done = false;
		let page = 1;
		while (!done) {
			if (fs.existsSync(`out/${query}/${page}`)) {
				page++;
				continue;
			}

			let url = `https://www.vezeeta.com/en/doctor/${query}/egypt?page=${page}`;

			await main.goto(url);
			await main.waitForSelector("[data-testid*=name--name]", { timeout: 0 });
			let links = await main.$$eval("[data-testid*=name--name]", (a) =>
				a.map((e) => `https://www.vezeeta.com${e.getAttribute("href")}`)
			);

			// each search page contains 10 doctors
			for (let i = 0; i <= 10; i++) {
				let doctor = await browser.newPage();

				await doctor.goto(links[i], {
					waitUntil: "networkidle0",
					timeout: 0,
				});

				let result = {};

				await doctor.waitForSelector("#__NEXT_DATA__", { timeout: 0 });
				let data = await doctor.$eval("#__NEXT_DATA__", (e) => e.textContent);
				data = JSON.parse(data);
				data = data.props.initialState.profiles.doctorProfileData.Profile;

				result.id = data.EntityId;
				result.key = data.EntityKey;
				result.isNewDoctor = data.IsNewDoctor;

				console.log(result);

				await doctor.close();
			}

			if (!done) {
				fs.closeSync(fs.openSync(`out/${query}_${page}`, "w"));
			}
			done = true;
			page++;
		}
	}
})();
