const PizZip = require("pizzip");
const DocxTemplater = require("docxtemplater");

const fs = require("fs");
const path = require("path");

let content = fs.readFileSync(
	path.resolve(
		__dirname,
		"مؤسسات/لائحة النظام الأساسي للمؤسسات - New 2021.docx"
	),
	"binary"
);
let zip = new PizZip(content);
let doc;
try {
	doc = new DocxTemplater(zip, { paragraphLoop: true, linebreaks: true });
} catch (err) {
	console.log(err);
}

doc.setData({
	name: "حبة الخردل الاجتماعية",
	registration_number: "6533",
	registration_day: "4",
	registration_month: "6",
	registration_year: "2006",
	management: "الزيتون",
	headquarters: "105 ش طومان باي الزيتون",
	main_activity: "رعاية الفئات الخاصة والمعوقين",
	scope: "الجمهورية",
	branches: [
		{ name: "1", registration_number: "1", governorate: "القاهرة" },
		{ name: "2", registration_number: "2", governorate: "القاهرة" },
	],
	fields: ["a", "b", "c", "d"],
	activities: ["a", "b", "c", "d"],
});

try {
	doc.render();
} catch (err) {
	console.log(err);
}

let buf = doc.getZip().generate({ type: "nodebuffer" });

fs.writeFileSync(path.resolve(__dirname, "output.docx"), buf);
