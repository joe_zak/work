import os
import sys

import pyperclip
from PySide2.QtCore import QSettings, Qt, Slot
from PySide2.QtGui import QIcon, QKeySequence
from PySide2.QtWidgets import *
from YT.Windows.sewindow import Settings

from db.database import DataBase
from ui.table import Ui_MainWindow
import faulthandler

class Main(QMainWindow, Ui_MainWindow):
	def __init__(self):
		super().__init__()
		self.setupUi(self)
		fg = self.frameGeometry()
		cp = QDesktopWidget().availableGeometry().center()
		fg.moveCenter(cp)
		self.move(fg.topLeft())
		self.setWindowIcon(QIcon("icons/logo.ico"))
		self.setMinimumSize(500, 500)
		exit_ = QAction("Exit", shortcut=QKeySequence("Ctrl+q"), triggered=lambda: self.close())
		self.addAction(exit_)
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.ytwindows, self.dlwindows, self.stwindows = [], [], []
		self.dlWindow, self.ytWindow, self.sThread, self.cThread, self.tray_icon = None, None, None, None, None
		self.db = DataBase()
		data = self.db.getAllData()
		self.tableWidget.setRowCount(0)
		self.tableWidget.setColumnHidden(0, True)
		self.tableWidget.setColumnHidden(1, True)
		for r, r1 in enumerate(data):
			self.tableWidget.insertRow(r)
			for col_n, value in enumerate(r1):
				self.tableWidget.setItem(r, col_n, QTableWidgetItem(str(value)))
		self.tableWidget.setContextMenuPolicy(Qt.CustomContextMenu)
		self.tableWidget.customContextMenuRequested.connect(self.context_menu)
		self.pasteYt_B.clicked.connect(self.open_new_window)
		self.resume_B.setEnabled(False)
		self.resume_B.clicked.connect(self.resumeDownload)
		self.pause_B.setEnabled(False)
		self.pause_B.clicked.connect(self.pauseDownload)
		self.deleteCompl_B.clicked.connect(self.deleteAllCompleted)
		self.delete_B.setEnabled(False)
		self.delete_B.clicked.connect(self.delete)
		self.setMouseTracking(True)
		self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint)
		self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
		self.tableWidget.selectionModel().selectionChanged.connect(self.on_selection_changed)
		self.trayIcon()
		self.settings = QSettings("settings.ini", QSettings.IniFormat)
		if self.settings.value("remove") == "1":
			self.deleteAllCompleted

	def showSettings(self):
		pass

	def open_new_window(self):
		pass

	def context_menu(self):
		menu = QMenu()
		menu.setStyleSheet("""
			QMenu { border: 1px solid black; background-color: #171b1d; color: #ffffff; }
			QMenu::item:enabled:selected { background: #00b3dc; color: #ffffff; }
			QMenu::item:disabled { color: #000000; }
		""")
		open_file_location = menu.addAction("Open File Location")
		delete_file = menu.addAction("Delete")
		resume_download = menu.addAction("Resume Download")
		pause_download = menu.addAction("Pause Download")
		copy_download_link = menu.addAction("Copy Download Link")
		if self.tableWidget.selectionModel().selectedRows():
			row = self.tableWidget.currentRow()
			id = self.tableWidget.item(row, 0).text()
			info = self.Db.getSelectedById(id)
			if info["status"] == "Finished":
				open_file_location.setEnabled(True)
				resume_download.setEnabled(False)
				pause_download.setEnabled(False)
			elif info["status"] == "Downloading":
				open_file_location.setEnabled(False)
				resume_download.setEnabled(False)
				pause_download.setEnabled(True)
			else:
				open_file_location.setEnabled(False)
				resume_download.setEnabled(True)
				pause_download.setEnabled(False)
			copy_download_link.triggered.connect(self.copyDownloadLink)

	def trayIcon(self):
		self.tray_icon = QSystemTrayIcon(self)
		self.tray_icon.setIcon(QIcon("icons/logo.ico"))
		show_action = QAction("Show", self)
		quit_action = QAction("Exit", self)
		hide_action = QAction("Hide", self)
		show_action.triggered.connect(self.show)
		hide_action.triggered.connect(self.hide)
		quit_action.triggered.connect(self.quit)
		tray_menu = QMenu()
		tray_menu.addAction(show_action)
		tray_menu.addAction(hide_action)
		tray_menu.addAction(quit_action)
		self.tray_icon.setContextMenu(tray_menu)
		self.tray_icon.activated.connect(self.restore_window)

	def restore_window(self, reason):
		if reason == QSystemTrayIcon.DoubleClick:
			self.show()

	def quit(self):
		qApp.quit()

	def deleteAllCompleted(self):
		pass

	def delete(self):
		pass

	@Slot()
	def resumeDownload(self):
		try:
			row = self.tableWidget.currentRow()
			Id = self.tableWidget.item(row, 0).text()
			found = False
			if len(self.dlwindows) > 0:
				for i, w in enumerate(self.dlwindows):
					if w["id"] == Id:
						self.dlwindows[i]["windows"].toggleDownload()
						found = True
						break
				if not found:
					self.dlWindow = DownloadWindow(self)
					self.dlWindow.resumeDownload(self)
					self.dlwindows.append({"id": Id, "windows": self.dlWindow})
			else:
				self.dlWindow = DownloadWindow(self)
				self.dlWindow.resumeDownload(self)
				self.dlwindows.append({"id": Id, "windows": self.dlWindow})
			self.tableWidget.clearSelection()
		except Exception as err:
			msg = QMessageBox()
			msg.setIcon(QMessageBox.Critical)
			msg.setText("Error")
			msg.setWindowTitle("Error")
			msg.setInformativeText(" ".join(err.args))
			msg.exec()

	@Slot()
	def pauseDownload(self):
		row = self.tableWidget.currentRow()
		Id = self.tableWidget.item(row, 0).text()
		if len(self.dlwindows) > 0:
			for i, w in enumerate(self.dlwindows):
				if w["id"] == Id:
					print("Paused")
					self.dlwindows[i]["windows"].toggleDownload()
		self.tableWidget.clearSelection()

	@Slot()
	def openFileLocation(self):
		location = self.settings.value("location") if self.settings.value("location") != "" else os.getcwd()

	@Slot(str)
	def copyDownloadLink(self, link):
		pyperclip.copy(link)

	def on_selection_changed(self):
		if self.tableWidget.selectionModel().selectedRows():
			self.delete_B.setEnabled(True)
			row = self.tableWidget.currentRow()
			Id = self.tableWidget.item(row, 0).text()
			info = self.db.getSelectedById(Id)
			if info["status"] == "Downloading":
				self.resume_B.setEnabled(False)
				self.pause_B.setEnabled(True)
			elif info["status"] == "Finished":
				self.resume_B.setEnabled(False)
				self.pause_B.setEnabled(False)
			else:
				self.resume_B.setEnabled(True)
				self.pause_B.setEnabled(False)
		else:
			self.resume_B.setEnabled(False)
			self.pause_B.setEnabled(False)
			self.delete_B.setEnabled(False)

if __name__ == "__main__":
	faulthandler.enable()
	app = QApplication(sys.argv)
	main = Main()
	main.show()
	sys.exit(app.exec_())
