from time import ctime
import sqlite3

class DataBase():
	conn = sqlite3.connect("data.db")
	c = conn.cursor()
	def __init__(self):
		self.create()

	def create(self):
		query = """
			CREATE TABLE "Downloader" (
				"id"          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
				"Url"         TEXT,
				"File_name"   TEXT,
				"Size"        TEXT,
				"Date_time"   TEXT,
				"Progress"    TEXT,
				"Quality"     INTEGER,
				"Remain"      TEXT,
				"Status"      TEXT,
				"Only"        TEXT,
				"Merge"       TEXT
			)
		"""
		try:
			self.c.execute(query)
		except Exception:
			pass
	
	def insert(self, url, name, size, progress, quality, remain, status, only, merge):
		query = "INSERT INTO Downloader (Url, File_name, Size, Date_time, Progress, Quality, Remain, Status, Only, Merge) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
		self.c.execute(query, (url, name, size, progress, quality, remain, status, only, merge))
		self.conn.commit()

	def update(self, url, name, size, progress, quality, remain, status, only, id):
		query = "UPDATE Downloader SET Url=?, File_name=?, Size=?, Date_time=?, Progress=?, Quality=?, Remain=?, Status=?, Only=? WHERE id=?"
		self.c.execute(query, (url, name, size, ctime(), progress, quality, remain, status, only, id))
		self.conn.commit()

	def updateDownloadStatus(self, status, id):
		pass

	def getAllData(self):
		query = "SELECT id, Url, File_name, Size, Date_time, Progress, Remain FROM Downloader"
		return self.c.execute(query)

	def getLastId(self):
		query = "SELECT last_insert_rowid()"
		result = self.c.execute(query)
		return result.fetchone()[0]

	def getSelectedById(self, id):
		query = "SELECT * FROM Downloader WHERE id=?"
		return self.c.execute(query, (id,))

	def getSelectedByUrl(self, url, quality):
		query = "SELECT * FROM Downloader WHERE Url=? and Quality=?"
		return self.c.execute(query, (url, quality))
	
	def checkExist(self, url, quality):
		result = self.getSelectedByUrl(url, quality).fetchall()
		return len(result)

	def deleteById(self, id):
		query = "DELETE FROM Downloader WHERE id=?"
		self.c.execute(query, (id,))
		self.conn.commit()

	def deleteAllCompleted(self):
		query = "DELETE FROM Downloader WHERE Progress=?"
		self.c.execute(query, ("Completed",))
		self.conn.commit()
