from PySide2 import QtCore, QtGui, QtWidgets

class Ui_NotifyForm(object):
	def setupUi(self, NotifyForm):
		NotifyForm.setObjectName("NotifyForm")
		NotifyForm.resize(300, 100)
		with open("styles/notifications/notifyform.qss") as file:
			NotifyForm.setStyleSheet(file.read())
		self.verticalLayout = QtWidgets.QVBoxLayout(NotifyForm)
		self.verticalLayout.setContentsMargins(0, 0, 0, 0)
		self.verticalLayout.setSpacing(6)
		self.verticalLayout.setObjectName("verticalLayout")
		self.widgetTitle = QtWidgets.QWidget(NotifyForm)
		self.widgetTitle.setMinimumSize(QtCore.QSize(0, 26))
		self.widgetTitle.setObjectName("widgetTitle")
		self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widgetTitle)
		self.horizontalLayout_3.setContentsMargins(10, 0, 0, 0)
		self.horizontalLayout_3.setSpacing(0)
		