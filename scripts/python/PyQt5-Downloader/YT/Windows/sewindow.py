import os
import sys
from PySide2.QtCore import QSettings, Qt
from PySide2.QtWidgets import QDialog, QFileDialog
from ui.settings import Ui_Dialog

class Settings(QDialog, Ui_Dialog):
	def __init__(self):
		super().__init__()
		self.setupUi(self)
		self.setWindowFlags(self.windowFlags() |
							Qt.WindowMinimizeButtonHint |
							Qt.WindowSystemMenuHint)
		self.settings = QSettings("settings.ini", QSettings.IniFormat)
		self.location_B.clicked.connect(self.selectLocation)
		self.save_B.clicked.connect(self.save)
		self.cancel_B.clicked.connect(self.cancel)

	def showWindow(self):
		if self.settings.value("startup") and self.settings.value("startup") == "1":
			self.startup_CB.setChecked(True)
		if self.settings.value("notify") and self.settings.value("notify") == "1":
			self.notify_CB.setChecked(True)
		if self.settings.value("remove") and self.settings.value("remove") == "1":
			self.remove_CB.setChecked(True)
		if self.settings.value("merge") and self.settings.value("merge") == "1":
			self.merge_CB.setChecked(True)
		if self.settings.value("location") and self.settings.value("location") != "":
			self.lineEdit.setText(self.settings.value("location"))
		if self.settings.value("limit") and self.settings.value("limit") != "":
			self.limit_CoB.setCurrentText(self.settings.value("limit"))
		if self.settings.value("subtitles") and self.settings.value("subtitles") == "1":
			self.subtitles_CB.setChecked(True)
		self.show()

	def save(self):
		if self.startup_CB.isChecked():
			self.settings.setValue("startup", "1")
		else:
			self.settings.setValue("startup", "0")
		if self.notify_CB.isChecked():
			self.settings.setValue("notify", "1")
		else:
			self.settings.setValue("notify", "0")
		if self.remove_CB.isChecked():
			self.settings.setValue("remove", "1")
		else:
			self.settings.setValue("remove", "0")
		if self.merge_CB.isChecked():
			self.settings.setValue("merge", "1")
		else:
			self.settings.setValue("merge", "0")
		if self.subtitles_CB.isChecked():
			self.settings.setValue("subtitles", "1")
		else:
			self.settings.setValue("subtitles", "0")
		if self.lineEdit.text() != "":
			self.settings.setValue("location", self.lineEdit.text())
		else:
			self.settings.setValue("location", "")
		self.settings.setValue("limit", self.limit_CoB.currentText())
		self.cancel()

	def cancel(self):
		self.close()

	def selectLocation(self):
		dialog = QFileDialog()
		folder_path = dialog.getExistingDirectory(None, "Select Folder")
		self.lineEdit.setText(folder_path)
		return folder_path
