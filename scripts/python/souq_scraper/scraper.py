import argparse

def checkUrl(url):
	url = url.split("/")[2 if "http" in url or "https" in url else 0]
	if "souq.com" in url:
		from souq_parser import parse
	elif "noon.com" in url:
		from noon_parser import parse
	else:
		return False
	return parse

try:
	parser = argparse.ArgumentParser()
	parser.add_argument("-u", "--url", help="url of the page that will be parsed", type=str)
	parser.add_argument("-x", "--export", help="exported file format json, excel, sqlite, mongodb (default: json)", default="json", choices=("json", "excel", "mongodb"), type=str)
	parser.add_argument("-l", "--limit", help="maximum number of elements to be parser (default: 10)", type=int, default=10)
	parser.add_argument("-f", "--filename", help="name of the exported file", default="out", type=str)
	parser.add_argument("-p", "--provider", help="which provider to choose from", type=str)
	parser.add_argument("-s", "--search", help="search term to use for the provider if api allows", type=str)
	parser.add_argument("-c", "--category", help="category of the data (Note: cannot be combined with search)", type=str)
	args = parser.parse_args()
	if args.provider == "noon":
		from noon_parser import parse
		if args.category:
			data = parse(args.category, args.limit)
		elif args.search:
			data = parse(args.search, args.limit)
	# if parse := checkUrl(args.url):
	# 	data = parse(args.url, args.limit)
		if data:
			if args.export == "json":
				import json_export
				json_export.export(data, args.filename)
			elif args.export == "excel":
				import excel_export
				excel_export.export(data, args.limit, args.filename)
			elif args.export == "sqlite":
				import sqlite_export
				sqlite_export.export(data, args.filename)
			elif args.export == "mongodb":
				import mongodb_export
				mongodb_export.export(data, args.filename)
			print("Exported")
except KeyboardInterrupt:
	print("\nExiting...")
	exit(1)