import json
import os

def export(data, filename):
	data = json.dumps(data)
	with open(f"./out/{filename}.json", "w") as f:
		f.write(data)