import xlsxwriter

def export(data, num, filename):
	workbook = xlsxwriter.Workbook(f"{filename}.xlsx")
	for c in data:
		worksheet = workbook.add_worksheet(c)
		worksheet.set_column(1, 10, 12)
		start = 2
		end = start + num
		wrap_format = workbook.add_format({"text_wrap": True})
		worksheet.set_column(8, 10, 12, wrap_format)
		# worksheet.add_table(start, 1, end, 10, {"banded_rows": True, "banded_columns": False, "first_column": True, "columns": [
		# 	{"header": "Name"},
		# 	{"header": "By"},
		# 	{"header": "Rating"},
		# 	{"header": "Image"},
		# 	{"header": "Old Price"},
		# 	{"header": "Current Price"},
		# 	{"header": "Shipping"},
		# 	{"header": "Variations", "format": wrap_format},
		# 	{"header": "Specs", "format": wrap_format},
		# 	{"header": "Description", "format": wrap_format}
		# ]})
		worksheet.add_table(start, 1, end, 10, {"banded_rows": True, "banded_columns": False, "first_column": True, "columns": [{"header": i, "format": wrap_format} for i in data.keys()]})
		for i in data[c]:
			start += 1
			# worksheet.write_row(start, 1, [
			# 	i["name"],
			# 	i["by"],
			# 	i["rating"],
			# 	i["img"],
			# 	i["price"]["was"],
			# 	i["price"]["is"],
			# 	i["shipping"],
			# 	"\n".join(i["variations"]),
			# 	"\n".join(i["specs"]),
			# 	i["description"]
			# ])
			worksheet.write_row(start, 1, [j for j in i.values()])
	workbook.close()