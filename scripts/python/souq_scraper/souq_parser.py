import requests
import json
from bs4 import BeautifulSoup
import sys
import os

# Meta Data
PRODUCT_TITLE_IN_ARABIC = "product_title_in_arabic"
PRODUCT_DESCRIPTION_IN_ARABIC = "product_description_in_arabic"
PRODUCT_SHORT_DESCRIPTION_IN_ARABIC = "product_short_description_in_arabic"
PRODUCT_SPECIFICATIONS_IN_ARABIC = "product_specifications_in_arabic"
PRODUCT_SPECIFICATIONS_IN_ENGLISH = "product_specifications_in_english"
PRODUCT_BRAND_IN_ENGLISH = "product_brand_in_english"
PRODUCT_BRAND_IN_ARABIC = "product_brand_in_arabic"

def getEngData(link, obj):

	product_page = requests.get(link.replace("/eg-ar/", "/eg-en/", 1))
	product_page = BeautifulSoup(product_page.text, "html.parser")

	keywords = product_page.select_one("meta[name='keywords']")
	keywords = keywords["content"] if keywords and "content" in keywords.attrs else ""
	obj["tags"] = []
	stack = []
	isAfterFor = False
	isAfterAnd = False
	for i, tag in enumerate(tags := keywords.replace("- ", "").replace(",", "").replace("&amp;", "").replace("and", "").split()):
		if isAfterFor:
			isAfterFor = not isAfterFor
			continue
		elif isAfterAnd:
			isAfterAnd = not isAfterAnd
			continue
		elif tag == "for" or isAfterFor:
			obj["tags"].append({"name": f"for {tags[i+1]}"})
			isAfterFor = not isAfterFor
		elif tag.isnumeric():
			stack.append(tag)
		elif len(stack) > 0:
			obj["tags"].append({"name": " ".join(stack) + f" {tag}"})
			stack = []
		else:
			obj["tags"].append({"name": tag})

	product_content = product_page.select_one("div.product_content")

	# product_header = product_content.select_one("div.product-header")
	images = []
	for e in product_content.select("div.product-header div.img-bucket img"):
		image = {}
		if "data-url" in e.attrs:
			image["src"] = e["data-url"]
		else:
			image["src"] = e["src"]
		images.append(image)
	obj["images"] = images

	product_actions = product_content.select_one("div.product-actions")

	if product_actions:
		if estimated_delivery := product_actions.select_one("div.estimated-delivery small.estimated span"):
			obj["estimated_delivery"] = estimated_delivery.text

		obj["condition"] = product_actions.select_one("dl.condition-box dd.unit-condition").text

		obj["seller"] = product_actions.select_one("span.unit-seller-link").text.strip()

		obj["seller-note"] = ""
		if note := product_actions.select_one("dl.seller-note span"):
			obj["seller-note"] = note.text.strip()
	else:
		obj["stock_status"] = "outofstock"

	# product_info = product_content.select_one("div.product-info")

	product_connections = product_content.select("div.product-info div.product-connections > *")
	for child in product_connections:
		if "connection-title" in child["class"]:
			current = child.text.strip()
		elif "connection-stand" in child["class"] or "item-connection" in child["class"]:
			obj["attributes"].append({
				"name": current,
				"visible": True,
				"variation": True,
				"options": [i for i in child.text.strip().split("\n") if i]
			})

	product_details = product_content.select_one("section.band div.product-details")
	specs = product_details.select_one("div#specs-full")
	if specs:
		dl = specs.select_one("dl")
		obj["meta_data"].append({
			"key": PRODUCT_SPECIFICATIONS_IN_ENGLISH,
			"value": f"<div class='specs_tab_content'>{dl.__str__()}</div>"
		})
		children = specs.select("dl > *")
	else:
		children = []
	current = ""
	for child in children:
		if child.tag == "dt":
			current = child.text.strip()
		elif child.tag == "dd":
			obj["attributes"].append({
				"name": current,
				"visible": True,
				"variation": False,
				"options": [child.text.strip()]
			})

	description = product_details.select_one("div#description-full")
	obj["description"] = description.__str__() if description else ""
	description_short = product_details.select_one("div#description-short")
	obj["short_description"] = description_short.__str__() if description_short else ""

	return obj

def getAraData(link):
	meta = []

	product_page = requests.get(link.replace("/eg-en/", "/eg-ar/", 1))
	product_page = BeautifulSoup(product_page.text, "html.parser")

	product_content = product_page.select_one("div.product_content")

	product_header = product_content.select_one("div.product-header")

	if product_header:
		title = product_header.select_one("h1").text
	else:
		title = product_content.select_one("div.product-title h6").text

	meta.append({
		"key": PRODUCT_TITLE_IN_ARABIC,
		"value": title
	})

	product_details = product_content.select_one("section.band div.product-details")
	specs = product_details.select_one("div#specs-full")
	if specs:
		dl = specs.select_one("dl")
		meta.append({
			"key": PRODUCT_SPECIFICATIONS_IN_ARABIC,
			"value": dl.__str__()
		})

	description = product_details.select_one("div#description-full")
	description_short = product_details.select_one("div#description-short")

	meta.append({
		"key": PRODUCT_DESCRIPTION_IN_ARABIC,
		"value": description.__str__() if description else ""
	})
	meta.append({
		"key": PRODUCT_SHORT_DESCRIPTION_IN_ARABIC,
		"value": description_short.__str__() if description_short else ""
	})

	return meta

def parse2(res):
	for unit in res["jsonData"]["units"]:
		if not os.path.isfile(f"{unit['item_id']}.json"):
			link = unit["primary_link"]
			print(link)
			obj = {
				# fullfilled by souq
				"is_fbs": unit["is_fbs"],
				"name": unit["title"],
				"average_rating": unit["average_rating"],
				"average_rating_total": unit["average_rating_total"],
				"regular_price": unit["market_price_formatted"] if unit["market_price_formatted"] else unit["price_formatted"],
				"sale_price": unit["price_formatted"] if unit["price_formatted"] and unit["market_price_formatted"] else "",
				"shipping": "free" if unit["free_shipping_eligiblity"] else "paid",
				"is_international_seller": unit["isInternationalSeller"],
				"categories": [{"name": unit["item_type_label"]}],
				"meta_data": [
					{
						"key": PRODUCT_BRAND_IN_ENGLISH,
						"value": unit["manufacturer"]
					},
					{
						"key": PRODUCT_BRAND_IN_ARABIC,
						"value": unit["manufacturer"]
					}
				],
				"attributes": [{
					"name": "brand",
					"visible": True,
					"variation": False,
					"options": [unit["manufacturer"]]
				}]
			}
			obj.update(getEngData(link, obj))
			obj["meta_data"] += getAraData(link)

			export(obj, unit['item_id'])


if __name__ == "__main__":

	import argparse

	from json_export import export

	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--search", help="query to search for", type=str, required=True)
	parser.add_argument(
		"-x",
		"--export",
		help="exported file format json, excel, mongodb, csv (default: json)",
		default="json",
		choices=("json", "excel", "mongodb", "csv"),
		type=str,
	)
	parser.add_argument(
		"-l",
		"--limit",
		help="maximum number of elements to be parser (default: 10)",
		type=int,
		default=10,
	)
	parser.add_argument(
		"-f", "--filename", help="name of the exported file", default="out", type=str
	)
	# parser.add_argument("-r", "--resume", help="continue scraping", action="store_true")
	args = parser.parse_args()

	queries = args.search.replace(" ", "-").split(",")

	if args.export:
		from json_export import export

	if not os.path.exists("out"):
		os.makedirs("out")

	for query in queries:
		page = 1
		while True:
			if not os.path.isfile(f"out/{query}_{page}"):
				res = requests.get(f"https://egypt.souq.com/eg-en/{query}/s/?as=1&action=page&page={page}").json()
				units = len(res["jsonData"]["units"])
				if units == 0:
					break
				parse2(res)
				print(f"Page {page} done")
				os.mknod(f"out/{query}_{page}")
				print(f"Page {page} posted\n")
			page += 1