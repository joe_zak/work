import json
import glob
import argparse
import os

from api import post

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--contains", type=str)
args = parser.parse_args()

for fname in glob.glob(f"./out/*{args.contains}*.json"):
	with open(fname) as f:
		if not os.path.isfile(done := f"{fname[:-5]}_posted"):
			print(f"Loading {fname}")
			data = json.load(f)
			post(data)
			os.mknod(done)
		print(f"{fname} Done\n")