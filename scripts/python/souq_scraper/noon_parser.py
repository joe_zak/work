from requests_html import HTMLSession
import requests
import shutil
import urllib.parse
from bs4 import BeautifulSoup
import json

BASE_URL = "https://www.noon.com/_svc/catalog/api/u/c/"

def clean(data, session):
	data["url"] = f"https://www.noon.com/{data['sku']}/p?o={data['offer_code']}"
	data["image_url"] = f"https://k.nooncdn.com/t_desktop-pdp-v1/{data['image_key']}.jpg"
	
	## Get description
	## note: Extremely slow
	res = session.get(data["url"])
	data["description"] = res.html.xpath("//div[div[@name='TabArea']]/div[2]", first=True).text

	## Download image
	## note: Extremely slow
	r = requests.get(data["image_url"], stream=True)
	if r.status_code == 200:
		r.raw.decode_content = True
		with open(f"{data['image_key'].split('/')[-1]}.jpg", "wb") as f:
			shutil.copyfileobj(r.raw, f)
	return data

def parse(query, limit):
	# "https://k.nooncdn.com/t_desktop-pdp-v1/v1604582551/N31008851A_1.jpg"
	session = HTMLSession()
	headers = {
		"x-mp": "noon",
		"x-locale": "ar-eg"
	}
	query = urllib.parse.quote(query)
	res = session.get(f"{BASE_URL}{query}?page=1&limit={limit}", headers=headers)
	data = res.json()
	nbPages = data["nbPages"]
	data = list(map(lambda data: clean(data, session), data["hits"]))
	for page in range(2, nbPages+1):
		res = session.get(f"{BASE_URL}{query}?page={page}", headers=headers)
		data += list(map(lambda data: clean(data, session), res.json()["hits"]))
	session.close()
	if res.status_code == 200:
		return data
	return False

def parse_all(limit):
	return False

if __name__ == "__main__":

	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-c", "--category", help="category of the data", type=str)
	parser.add_argument("-a", "--all", help="download data from all categories", action="store_true")
	parser.add_argument("-x", "--export", help="exported file format json, excel, mongodb (default: json)", default="json", choices=("json", "excel", "mongodb"), type=str)
	parser.add_argument("-l", "--limit", help="maximum number of elements to be parser (default: 0)", type=int, default=0)
	parser.add_argument("-f", "--filename", help="name of the exported file", default="out", type=str)
	args = parser.parse_args()

	if "noon.com" in args.url:
		if args.all:
			data = parse_all(args.limit)
		elif args.category:
			data = parse(args.category, args.limit)

		if args.export == "json":
			from json_export import export
		elif args.export == "excel":
			from excel_export import export
		elif args.export == "mongodb":
			from mongodb_export import export
		export(data, args.limit, args.filename)
	else:
		print("Invalid URL")