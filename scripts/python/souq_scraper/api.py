# WooCommerce API
CONSUMER_KEY = "ck_11765d0305f930575a9f713b852ff3744ccaf490"
CONSUMER_SECRET = "cs_30faa142227ffec8748e535d59ce2db672ec9330"

from woocommerce import API
import sys
from html import escape

wcapi = API(url="https://al2uno.com/", consumer_key=CONSUMER_KEY, consumer_secret=CONSUMER_SECRET, timeout=1000)

def post(data):
	for i, obj in enumerate(data):
		if not obj.get("categories") and not obj["categoris"][0]["name"]:
			obj["categories"] = [{"name": "other"}]
		for i2, c in enumerate(obj["categories"]):
			data[i]["categories"][i2]["id"] = createCat(escape(c["name"]))
		if not proExists(obj["name"]):
			res = wcapi.post("products", obj)
			if res.status_code != 201:
				print(res.status_code, res.text)
				sys.exit()

def createCat(cat):
	found, _id = catExists(cat)
	if not found:
		_id = wcapi.post("products/categories", {"name": cat}).json()["id"]
	return _id

def catExists(cat):
	if len(res := wcapi.get("products/categories", params={"search": cat}).json()) > 0:
		return True, res[0]["id"]
	return False, None

def proExists(pro):
	if len(wcapi.get("products", params={"search": pro}).json()) > 0:
		return True
	return False