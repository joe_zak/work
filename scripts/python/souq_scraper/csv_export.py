import csv

def export(data, filename):
	longest = []
	for e in data:
		if len(keys := e.keys()) > len(longest):
			longest = keys
	try:
		with open(f"{filename}.csv", "w") as f:
			writer = csv.DictWriter(f, fieldnames=longest)
			writer.writeheader()
			for e in data:
				writer.writerow(e)
	except IOError:
		print("I/O Error")