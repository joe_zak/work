import json
import glob

def pprint(done: dict):
	for k, v in done.items():
		print(f"{k}: {v}")

done = {}

for fname in glob.glob("./out/*.json"):
	with open(fname) as f:
		data = json.load(f)
	for p in data:
		k = p["categories"][0]["name"] if p.get("categories") else "Other"
		if done.get(k):
			done[k] += 1
		else:
			done[k] = 1

pprint(done)