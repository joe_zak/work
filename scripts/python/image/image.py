import numpy as np
import cv2
import argparse
import timeit
import math

def inverse(img):
	return -1 * img + 255

def flip(img, axis=None):
	return np.flip(img, axis)

def grayscale(img):
	return np.sum(img, axis=2)/3

def edge(img, h, w, weight=1):
	st = args.size_threshold
	if st >= w or st >= h:
		print("Invalid size threshold")
		exit()
	new_img = np.zeros((h//st, w//st))
	img = grayscale(img)
	v_filter = np.array([
		[-1, 0, 1],
		[-1, 0, 1],
		[-1, 0, 1]
	])
	h_filter = np.array([
		[-1, -1, -1],
		[ 0,  0,  0],
		[ 1,  1,  1]
	])
	for row in range(1, h-1, st):
		for col in range(1, w-1, st):
			local_pixels = img[row-1:row+2, col-1:col+2]
			v_score = (v_filter * local_pixels).sum()
			h_score = (h_filter * local_pixels).sum()
			score = math.sqrt(v_score**2 + h_score**2)
			edge_score = weight * score if score > args.threshold else 0
			new_img[row//2-1, col//2-1] = edge_score
	return new_img

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-w", "--weight", type=float, default=1)
	parser.add_argument("-o", "--output")
	parser.add_argument("-i", "--inverse", action="store_true")
	parser.add_argument("-f", "--flip", choices=("h", "v"))
	parser.add_argument("-t", "--threshold", type=float, default=1)
	parser.add_argument("-st", "--size-threshold", type=int, default=1)
	parser.add_argument("filename", nargs=1)
	args = parser.parse_args()
	img = cv2.imread(args.filename[0])
	if args.inverse:
		new_img = inverse(img)
	if args.flip:
		new_img = flip(img, 0 if args.flip == "v" else 1)
	h, w, c = img.shape
	# new_img = edge(img, h, w, args.weight)
	# new_img = grayscale(img)
	# print(new_img[0:3, w-2:w])
	# print(new_img)
	# print(w)
	new_img = edge(img, h, w, args.weight)
	# new_img = inverse(new_img)
	cv2.imwrite(args.output, new_img)