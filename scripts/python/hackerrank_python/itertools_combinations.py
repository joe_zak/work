# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import combinations
S, K = input().split()
S = sorted(S)
K = int(K)
for size in range(1, K+1):
    for comb in combinations(S, size):
        print("".join(comb))