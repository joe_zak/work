#!/usr/bin/python

def displayPathtoPrincess(n, grid):
    result = ""
    princess = [-1, -1]
    machine = [-1, -1]
    for i, row in enumerate(grid):
        p = row.find("p")
        if p > -1:
            princess = [i, p]
        m = row.find("m")
        if m > -1:
            machine = [i, m]
    distance = [princess[0] - machine[0], princess[1] - machine[1]]
    result += ("DOWN\n" if distance[0] > 0 else "UP\n") * abs(distance[0]) + \
        ("RIGHT\n" if distance[1] > 0 else "LEFT\n") * abs(distance[1])
    print(result, end="")


m = int(input())
grid = []
for _ in range(0, m):
    grid.append(input().strip())

displayPathtoPrincess(m, grid)
