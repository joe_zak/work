def minion_game(string):
    vowels = "EUIOA"
    scores = [0, 0]
    for i in range(len(string)):
        scores[0 if string[i] in vowels else 1] += len(string)-i
    if scores[0] > scores[1]:
        print("Kevin {}".format(scores[0]))
    elif scores[0] < scores[1]:
        print("Stuart {}".format(scores[1]))
    else:
        print("Draw")
