#!/bin/sh
if [ "$EUID" -eq 0 ]
then
	if [ $# -eq 1 ]
	then
		ip link set $1 down
		iw dev $1 set type managed
		ip link set $1 up
		systemctl restart NetworkManager
		echo [+] Managed mode enabled for $1
	fi
else
	echo [-] run this script as root
fi
