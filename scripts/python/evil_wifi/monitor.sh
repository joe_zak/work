#!/bin/sh
if [ "$EUID" -eq 0 ]
then
	if [ $# -eq 1 ]
	then
		ip link set $1 down
		airmon-ng check kill
		iw dev $1 set type monitor
		ip link set $1 up
		echo [+] Monitor mode enabeled for $1
	fi
else
	echo [-] run this script as root
fi
