"""
QUESTION 1: do I need to find an opcode 99 to stop working on amp A?

No. Indeed as soon as you hit Opcode 4 you obtain the output and you give it to amp B.
If a computer halts get the output from channel E (I believe they all halt at the same time).

QUESTION 2: I take amp B will work on the same data as amp A was (including any modifications introduced by amp A),
from position ZERO, right?
No, B will be in a fresh state in the first round after A produces an output.
All computers must keep running with their own state (no shared state) and you have to pass the outputs forward.

Question 3: Once back in amp A, based on what I understand I will NOT start from location 0,
but continue from the last location processed by amp A during the first pass (basically as if I had never stopped running amp A).
Is this correct?

This is correct.

QUESTION 4: If at any point I encounter an opcode 99 in amps A, B, C or D,
do I have to move immediately to the next amp? If so, which input should I feed into the next amp,
given no output has been produced before halting on opcode 99?

You cannot give it any more input. Just read the last output from E.
"""
import itertools
import pprint

# with open("input_7.txt") as f:
# 	nums = [int(i) for i in f.read().split(",")]
nums = [int(i) for i in "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5".split(",")]
# all_seq = itertools.permutations([5, 6, 7, 8, 9], 5)
# for y, current_seq in enumerate(all_seq):
current_seq = [9, 8, 7, 6, 5]
trackers = [{"nums": nums[:], "index": 0}, {"nums": nums[:], "index": 0}, {"nums": nums[:], "index": 0}, {"nums": nums[:], "index": 0}, {"nums": nums[:], "index": 0}]
stdin = 0
e_out = 0
break_b = True
print(trackers)
while break_b:
	for x, phase in enumerate(current_seq):
		phase_b = True
		while trackers[x]["index"] < len(trackers[x]["nums"]):
			num = str(trackers[x]["nums"][trackers[x]["index"]]).zfill(5)
			if int(num[3:]) == 1:
				trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]) + (trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]])
				trackers[x]["index"] += 4
			elif int(num[3:]) == 2:
				trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]) * (trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]])
				trackers[x]["index"] += 4
			elif int(num[3:]) == 3:
				if phase_b:
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]] = phase
					phase_b = False
				else:
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]] = stdin
				trackers[x]["index"] += 2
			elif int(num[3:]) == 4:
				stdin = trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]
				if x == 4:
					e_out = stdin
				trackers[x]["index"] += 2
				break
			elif int(num[3:]) == 5:
				if (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]):
					trackers[x]["index"] = trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]]
				else:
					trackers[x]["index"] += 3
			elif int(num[3:]) == 6:
				if not (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]):
					trackers[x]["index"] = trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]]
				else:
					trackers[x]["index"] += 3
			elif int(num[3:]) == 7:
				if (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]) < (trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]]):
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = 1
				else:
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = 0
				trackers[x]["index"] += 4
			elif int(num[3:]) == 8:
				if (trackers[x]["nums"][trackers[x]["index"]+1] if int(num[2]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+1]]) == (trackers[x]["nums"][trackers[x]["index"]+2] if int(num[1]) else trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+2]]):
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = 1
				else:
					trackers[x]["nums"][trackers[x]["nums"][trackers[x]["index"]+3]] = 0
				trackers[x]["index"] += 4
			elif int(num[3:]) == 99:
				break_b = False
				break
			else:
				print(f"Unknown: Position: {trackers[x]['index']} Value: {trackers[x]['nums'][trackers[x]['index']]}")
				break
pprint.pprint(trackers)
print(e_out)
print(139629729)