from bs4 import BeautifulSoup
import requests

soup = BeautifulSoup(requests.get("https://www.facebook.com/login.php", headers={"Accept-Language": "en-US,en;q=0.5", "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0"}).text, "html.parser")
for i in soup.find_all(name="script"):
    if i.has_attr("src"):
        print("lgn" in requests.get(i["src"]).text)
    else:
        print("lgn" in i.text)