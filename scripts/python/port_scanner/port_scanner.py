from socket import AF_INET, SOCK_STREAM, socket, gethostbyname
from threading import Thread
from time import sleep
from sys import argv

service = {
	1: "tcpmux",
	2: "CompressNet",
	3: "CompressNet",
	5: "RJE",
	7: "ECHO",
	9: "Discard",
	11: "systat",
	13: "daytime",
	17: "QOTD",
	18: "MSP",
	19: "CharGen",
	20: "FTP-data",
	21: "FTP",
	22: "SSH",
	23: "Telnet",
	25: "SMTP",
	27: "NSW-FE",
	29: "MSG-ICP",
	31: "MSG-Auth",
	33: "DSP",
	37: "Time",
	38: "RAP",
	39: "RLP",
	41: "Graphics",
	42: "Host Name Server",
	43: "Whois",
	44: "MPM-Flags",
	45: "MPM",
	46: "MPM-Send",
	50: "IPSec",
	51: "IPSec",
	53: "DNS",
	67: "DHCP",
	68: "DHCP",
	69: "TFTP",
	80: "HTTP",
	110: "POP3",
	119: "NNTP",
	123: "NTP",
	135: "NetBIOS",
	136: "NetBIOS",
	137: "NetBIOS",
	138: "NetBIOS",
	139: "NetBIOS",
	143: "IMAP4",
	161: "SNMP",
	162: "SNMP",
	389: "Lightweight Directory Access Protocol",
	443: "HTTPS",
	444: "SNPP",
	445: "Microsoft-DS SMB",
	465: "SMTPS",
	515: "LPD",
	5357: "WSDAPI",
	7547: "CWMP",
	24465: "Tonido Directory Server",
	40000: "SafetyNET P",
	44818: "EtherNet/IP explicit messaging",
	49664: "Task Scheduler",
	49665: "Spooler",
	49666: "Eventlog",
	49667: "Skype"
}

def test(port, ip):
	s = socket(AF_INET, SOCK_STREAM)
	s.settimeout(2)
	try:
		result = s.connect_ex((ip, port))
		serv_name = service[port]
	except KeyError:
		serv_name = "unknown"
	except TimeoutError:
		exit()
	if result == 0:
		print("{} ({}) is open".format(port, serv_name))
		exit()

if len(argv) > 1:
	ip = gethostbyname(argv[1])
	for i in range(1, 65535):
		Thread(target=test, args=(i, ip)).start()
		sleep(0.01)
else:
	print("Usage: {} hostname/ip".format(argv[0]))
	exit()