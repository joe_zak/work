import string
import secrets

for i in range(20):
	print(secrets.choice(string.ascii_letters+string.digits+string.punctuation), end="")
print()