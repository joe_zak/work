# import sys
# #from PySide2.QtWidgets import QApplication, QLabel

# #app = QApplication(sys.argv)
# #label = QLabel("<font color=red size=40>Hello World!</font>")
# #label.show()
# #app.exec_()
# from PySide2.QtWidgets import QApplication
# from PySide2.QtQuick import QQuickView
# from PySide2.QtCore import QUrl

# app = QApplication(sys.argv)
# view = QQuickView()
# url = QUrl("view.qml")
# view.setSource(url)
# # Resize with window
# view.setResizeMode(QQuickView.SizeRootObjectToView)
# view.show()
# app.exec_()

import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QAction, QMenu
from PySide2.QtGui import QKeySequence, QIcon
from PySide2.QtCore import QSize, Qt

class LeftClickMenu(QMenu):
	def __init__(self, parent=None):
		super().__init__("File", parent)
		self.addAction()

class Main(QMainWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		action = QAction("Quit", self)
		action.setShortcut(QKeySequence("Ctrl+q"))
		action.triggered.connect(QApplication.quit)
		self.addAction(action)
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.setMinimumSize(QSize(500, 500))
		self.setWindowTitle("PySide2 Downloader")
		self.setWindowIcon(QIcon("icons/logo.ico"))

if __name__ == "__main__":
	app = QApplication(sys.argv)
	main = Main()
	main.show()
	sys.exit(app.exec_())
