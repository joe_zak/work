#!/usr/bin/env python3
import json
import discord
import os.path

client = discord.Client()

@client.event
async def on_message(message):
	if message.author != client.user:
		print("[+] {0.author}:{0.author.mention} sent a message in {0.server.id}:{0.server.name}->{0.channel}.".format(message))
		splitted = message.content.split(" ")
		try:
			if message.content == "/hello":
				msg = "Hello {0.author.mention}".format(message)
			elif splitted[0] == "/next" and splitted[1] in ["battle", "event"]:
				try:
					with open("events") as p:
						events = json.load(p)[splitted[1]][message.server.id]
						if splitted[1] == "battle":
							if len(events) >= 2:
								msg = "next {} vs. {}".format(events[0], events[1])
							elif message.author.mention in events:
								msg = "{0.author.mention}, We need more participants".format(message)
							else:
								msg = "{0.author.mention}, We need more participants use \"/join battle\" to join".format(message)
						elif splitted[1] == "event":
							if len(events) > 0:
								msg = "{} is next".format(events[0])
							else:
								msg = "{0.author.mention}, The list is empty use \"/join event\" to join".format(message)
				except KeyError:
					msg = "{0.author.mention}, We need more participants use \"/join {1}\" to join".format(message, splitted[1])
			elif splitted[0] == "/join" and splitted[1] in ["battle", "event"]:
				with open("events", "r+") as p:
					try:
						events = json.load(p)
						if message.author.mention in events[splitted[1]][message.server.id]:
							msg = "{0.author.mention}, You are already in the list just wait for your turn".format(message)
						else:
							events[splitted[1]][message.server.id].append(message.author.mention)
							msg = "{0.author.mention} added successfully".format(message)
					except KeyError:
						events[splitted[1]][message.server.id] = [message.author.mention]
						msg = "{0.author.mention} added successfully".format(message)
					p.seek(0)
					json.dump(events, p)
					p.truncate()
			elif splitted[0] == "/leave" and splitted[1] in ["battle", "event"]:
				try:
					with open("events", "r+") as p:
						events = json.load(p)
						del events[splitted[1]][message.server.id][events[splitted[1]][message.server.id].index(message.author.mention)]
						p.seek(0)
						json.dump(events, p)
						p.truncate()
					msg = "{0.author.mention} removed successfully".format(message)
				except (KeyError, ValueError):
					msg = "{0.author.mention} you're not in the list :unamused:".format(message)
			elif splitted[0] == "/queue" and splitted[1] in ["battle", "event"]:
				with open("events") as p:
					events = json.load(p)[splitted[1]][message.server.id]
				msg = ""
				if splitted[1] == "battle":
					i = 0
					x = 1
					while i < len(events):
						try:
							msg += "{}. {} vs. {}\n".format(x, events[i], events[i+1])
						except IndexError:
							pass
						i += 2
						x += 1
				elif splitted[1] == "event":
					for i in range(len(events)):
						msg += "{}. {}\n".format(i+1, events[i])
			elif splitted[0] == "/remove" and splitted[1] in ["battle", "event"] and len(splitted) == 3:
				if message.author.server_permissions.administrator:
					if message.author.mention != splitted[2]:
						try:
							with open("events", "r+") as p:
								events = json.load(p)
								del events[splitted[1]][message.server.id][events[splitted[1]][message.server.id].index(splitted[2])]
								p.seek(0)
								json.dump(events, p)
								p.truncate()
							msg = "{0.author.mention}, {1} removed successfully".format(message, splitted[2])
						except (KeyError, ValueError):
							msg = "{0.author.mention}, {1} is not in the list :unamused:".format(message, splitted[2])
					else:
						msg = "{0.author.mention} just use \"/leave {battle, event}\" :unamused:"
				else:
					msg = "{0.author.mention} permission denied.".format(message)
			elif splitted[0] == "/add" and splitted[1] in ["battle", "events"] and len(splitted) == 3:
				if message.author.server_permissions.administrator:
					if message.author.mention != splitted[2]:
						with open("events", "r+") as p:
							try:
								events = json.load(p)
								if splitted[2] in events[splitted[1]][message.server.id]:
									msg = "{0.author.mention}, {1} is already in the list just wait for your turn".format(message, splitted[2])
								else:
									events[splitted[1]][message.server.id].append(splitted[2])
									msg = "{0.author.mention}, {1} added successfully".format(message, splitted[2])
							except KeyError:
								events[splitted[1]][message.server.id] = [splitted[2]]
								msg = "{0.author.mention}, {1} added successfully".format(message, splitted[2])
							p.seek(0)
							json.dump(events, p)
							p.truncate()
					else:
						msg = "{0.author.mention} just use \"/join {battle, event}\" :unamused:"
				else:
					msg = "{0.author.mention} permission denied.".format(message)
			elif message.content == "/dump":
				if str(message.author) == "joezak#5991":
					with open("events") as p:
						msg = p.read()
					msg = msg if len(msg) > 0 else "Empty"
				else:
					msg = "{0.author.mention} permission denied.".format(message)
			elif message.content == "/about":
				msg = "Bot created for fun by <@501378224509681667> aka Brutal Kitten"
			elif message.content == "/help":
				msg = "Usage:\n\n\t/join {battle, event}\n\t\tto add your username to the list\n\n\t/next {battle, event}\n\t\tshow the next on the list\n\n\t/leave {battle, event}\n\t\tremoves your username from the list\n\n\t/queue {battle, event}\n\t\tsends the whole list\n\n\t/hello\n\t\tdon't think i need to explain this one :smile:\n\n\t/add {battle, event} {username}\n\t\tforce a user in the list (for admins only)\n\n\t/remove {battle, event} {username}\n\t\tforce a user out of the list (for admins only)"
			if msg:
				await client.send_message(message.channel, msg)
		except IndexError:
			msg = "Usage:\n\n\t/join {battle, event}\n\t\tto add your username to the list\n\n\t/next {battle, event}\n\t\tshow the next on the list\n\n\t/leave {battle, event}\n\t\tremoves your username from the list\n\n\t/queue {battle, event}\n\t\tsends the whole list\n\n\t/hello\n\t\tdon't think i need to explain this one :smile:\n\n\t/add {battle, event} {username}\n\t\tforce a user in the list (for admins only)\n\n\t/remove {battle, event} {username}\n\t\tforce a user out of the list (for admins only)"
		except UnboundLocalError:
			pass

@client.event
async def on_ready():
	print("[+] logged in successfully.")
	print("[!] checking for events file.")
	if os.path.isfile("events"):
		print("[+] file exists.")
	else:
		print("[!] creating file.")
		with open("events", "w") as p:
			p.write("{}")
		print("[+] file created successfully.")

client.run("NTMxNDQ4MzIxMjgyMTQ2MzA0.DxOtjg.odiiEKLqpJfUh1DCAeBjB9ibk6s")