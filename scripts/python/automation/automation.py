# # from pynput import mouse
# # from pynput import keyboard

# import shutil
# import argparse
# import os
# import time
# import pickle
# import sys
# if sys.platform == "linux":
# 	import readline
# from typing import IO

# class Listen:
# 	def __init__(self, verbose, task):
# 		self.verbose = verbose
# 		self.task = task
# 		self.mouse = mouse.Listener(on_click=self.on_click, on_scroll=self.on_scroll)
# 		self.keyboard = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
# 		self.record = False
# 		self.pressed = set()
# 		self.combinations = {
# 			"record": {keyboard.Key.ctrl, keyboard.Key.alt, keyboard.KeyCode(char="r")},
# 			"store": {keyboard.Key.ctrl, keyboard.Key.alt, keyboard.KeyCode(char="s")}
# 		}
# 		self.keys = set()
# 		for v in self.combinations.values():
# 			self.keys |= v
# 		self.instructions = []

# 	def on_click(self, x, y, button, pressed):
# 		if self.record:
# 			self.instructions.append({
# 				"device": "mouse",
# 				"command": "click",
# 				"x": x,
# 				"y": y,
# 				"button": button,
# 				"pressed": pressed
# 			})
# 			if self.verbose:
# 				print(self.instructions[-1])

# 	def on_scroll(self, x, y, dx, dy):
# 		if self.record:
# 			self.instructions.append({
# 				"device": "mouse",
# 				"command": "scroll",
# 				"x": x,
# 				"y": y,
# 				"dx": dx,
# 				"dy": dy
# 			})
# 			if self.verbose:
# 				print(self.instructions[-1])

# 	def on_press(self, key):
# 		if key == keyboard.Key.esc:
# 			self.quit()
# 		if key in self.keys:
# 			self.pressed.add(key)
# 		if self.record:
# 			self.instructions.append({
# 				"device": "keyboard",
# 				"command": "press",
# 				"key": key
# 			})
# 			if self.verbose:
# 				print(self.instructions[-1])
# 		if self.pressed in self.combinations.values():
# 			if keyboard.KeyCode(char="r") in self.pressed:
# 				self.record = not self.record
# 			elif keyboard.KeyCode(char="s") in self.pressed:
# 				self.quit()
# 				name = input("task name: ")
# 				Compiler(name, self.instructions)

# 	def on_release(self, key):
# 		self.pressed.discard(key)
# 		if self.record:
# 			self.instructions.append({
# 				"device": "keyboard",
# 				"command": "release",
# 				"key": key
# 			})
# 			if self.verbose:
# 				print(self.instructions[-1])

# 	def quit(self):
# 		self.mouse.stop()
# 		self.keyboard.stop()

# 	def start(self):
# 		self.mouse.start()
# 		self.keyboard.run()

# def Task(args):
# 	mcontroller = mouse.Controller()
# 	kcontroller = keyboard.Controller()
# 	with open(args.run, "br") as file:
# 		instructions = pickle.load(file)
# 	for i in instructions:
# 		if i["device"] == "mouse":
# 			command = i["command"]
# 			x = i["x"]
# 			y = i["y"]
# 			mcontroller.position = (x, y)
# 			if command == "click":
# 				if i["pressed"]:
# 					mcontroller.press(i["button"])
# 				else:
# 					mcontroller.release(i["button"])
# 			elif command == "scroll":
# 				dx = i["dx"]
# 				dy = i["dy"]
# 				mcontroller.scroll(dx, dy)
# 		if i["device"] == "keyboard":
# 			command = i["command"]
# 			key = i["key"]
# 			exec(f"kcontroller.{command}(key)")
# 		time.sleep(args.delay)

# def Parser(file: IO):
# 	with open(file, "br") as file:
# 		return pickle.load(file)

# def Compiler(file: str, obj):
# 	with open(file, "bw") as file:
# 		pickle.dump(obj, file)

# if __name__ == "__main__":
# 	parser = argparse.ArgumentParser()
# 	parser.add_argument("--run", "-r", type=str, metavar="filename")
# 	parser.add_argument("--verbose", "-v", action="store_true")
# 	parser.add_argument("--compile", "-c", type=str, metavar="filename")
# 	parser.add_argument("--output", "-o", type=str, metavar="filename")
# 	parser.add_argument("--delay", type=float, default=.1)
# 	args = parser.parse_args()
# 	if args.run:
# 		Task(args)
# 		exit()
# 	if args.compile:
# 		if not args.output:
# 			args.output = args.compile
# 		obj = Parser(args.compile)
# 		Compiler(args.output, obj)
# 		exit()
# 	auto = Listen(args.verbose)
# 	auto.start()

import keyboard
import mouse
import argparse
import pickle
import os

class Main:
	def __init__(self):
		self.events = []

	def play(self, events: list, speed_factor: float):
		for i in events:
			print(type(i))
			# if i.__module__ is mouse._mouse_event:
			# 	# mouse.play(i, speed_factor=speed_factor)
			# elif i.__module__ is keyboard._keyboard_event:
			# 	# keyboard.play(i, speed_factor=speed_factor)
			# 	print(i)
			# print(isinstance(i, mouse._mouse_event.MoveEvent))

	def run(self, file: str, speed: float):
		with open(file, "br") as file:
			events = pickle.load(file)
		self.play(events, speed)

	def record(self, file: str):
		keyboard.hook(lambda _: self.events.append(_))
		mouse.hook(self.events.append)
		keyboard.wait("esc", trigger_on_release=True, suppress=True)
		with open(file, "bw") as file:
			pickle.dump(self.events, file)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--save", type=str, metavar="FILENAME")
	parser.add_argument("-r", "--run", type=str, metavar="FILENAME")
	parser.add_argument("-f", "--speed-factor", type=float, default=1.0)
	args = parser.parse_args()
	app = Main()
	if args.run:
		if os.path.isfile(args.run):
			app.run(args.run, args.speed_factor)
	elif args.save:
		if os.path.isfile(args.save):
			app.record(args.save)