import math
import pandas_datareader
import numpy
import pandas
from sklearn import preprocessing, model_selection, svm
from sklearn.linear_model import LinearRegression
import datetime
from matplotlib import pyplot
from matplotlib import style

df = pandas_datareader.DataReader("GOOGL", data_source="yahoo")
print(df)
print(df.loc["2018-03-27"]["High"])
df = df[["Open", "High", "Low", "Adj Close", "Volume"]]
df["HL_PCT"] = (df["High"] - df["Low"]) / df["Adj Close"] * 100.0
df["PCT_CHANGE"] = (df["Adj Close"] - df["Open"]) / df["Open"] * 100.0

df = df[["Adj Close", "HL_PCT", "PCT_CHANGE", "Volume"]]
forecast_col = "Adj Close"
df.fillna(value=-99999, inplace=True)
forecast_out = int(math.ceil(0.01 * len(df)))
df["label"] = df[forecast_col].shift(-forecast_out)

x = numpy.array(df.drop(["label"], 1))
x_lately = x[-forecast_out:]
x = x[:-forecast_out]
df.dropna(inplace=True)
y = numpy.array(df["label"])
x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2)

clf = LinearRegression(n_jobs=-1)
clf.fit(x_train, y_train)
confidence = clf.score(x_test, y_test)
print(confidence)

forecast_set = clf.predict(x_lately)

print(forecast_set, confidence, forecast_out)

style.use("ggplot")

df["Forecast"] = numpy.nan

last_date = df.iloc[-1].name
last_unix = last_date.timestamp()
one_day = 86400
next_unix = last_unix + one_day

for i in forecast_set:
    next_date = datetime.datetime.fromtimestamp(next_unix)
    next_unix += 86400
    df.loc[next_date] = [numpy.nan for _ in range(len(df.columns)-1)] + [i]

df["Adj Close"].plot()
df["Forecast"].plot()
pyplot.legend(loc=4)
pyplot.xlabel("Date")
pyplot.ylabel("Price")
pyplot.show()