import pandas_datareader
import numpy
from sklearn.preprocessing import MinMaxScaler
import joblib
from keras.models import Sequential
from keras.layers import Dense, LSTM
from matplotlib import pyplot
import warnings

class Train:
	@staticmethod
	def start(symbol="AAPL", source="yahoo", start=None, end=None, api_key=None):
		data = pandas_datareader.DataReader(symbol, data_source=source, start=start, end=end, api_key=api_key).filter(["Close"])
		dataset = data.values
		training_data_len = len(dataset)
		scaler = MinMaxScaler(feature_range=(0, 1))
		scaled_data = scaler.fit_transform(dataset)
		train_data = scaled_data[:training_data_len]
		x_train = []
		y_train = []
		for i in range(60, training_data_len):
			x_train.append(train_data[i-60:i, 0])
			y_train.append(train_data[i, 0])
		x_train, y_train = numpy.array(x_train), numpy.array(y_train)
		x_train = numpy.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
		model = Sequential()
		model.add(LSTM(50, return_sequences=True, input_shape=(x_train.shape[1], 1)))
		model.add(LSTM(50, return_sequences=False))
		model.add(Dense(25))
		model.add(Dense(1))
		model.compile(optimizer="adam", loss="mean_squared_error")
		model.fit(x_train, y_train, batch_size=1, epochs=1)
		joblib.dump(model, "model.pkl")

if __name__ == "__main__":
	Train().start()