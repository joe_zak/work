import math
import pandas_datareader
import numpy
from sklearn.preprocessing import MinMaxScaler
import joblib
from keras.models import Sequential
from keras.layers import Dense, LSTM
from matplotlib import pyplot
from pandas.plotting import register_matplotlib_converters
import warnings
import os
import datetime

warnings.filterwarnings("ignore", category=DeprecationWarning)
register_matplotlib_converters()
pyplot.style.use("fivethirtyeight")
df = pandas_datareader.DataReader("AAPL", data_source="yahoo")
data = df.filter(["Close"])
dataset = data.values
training_data_len = math.ceil(len(dataset) * 0.8)
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(dataset)
train_data = scaled_data[:training_data_len]
x_train = []
y_train = []
for i in range(60, training_data_len):
	x_train.append(train_data[i-60:i, 0])
	y_train.append(train_data[i, 0])
x_train, y_train = numpy.array(x_train), numpy.array(y_train)
x_train = numpy.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
if not os.path.isfile("model.pkl"):
	from train import Train
	Train.start()
model = joblib.load("model.pkl")
# model.fit(x_train, y_train, batch_size=1, epochs=1)
# joblib.dump(model, "model.pkl")
test_data = scaled_data[training_data_len-60:]
x_test = []
y_test = dataset[training_data_len:, :]
for i in range(60, len(test_data)):
	x_test.append(test_data[i-60:i, 0])
print(x_test)
exit()
x_test = numpy.array(x_test)
x_test = numpy.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))
predictions = model.predict(x_test)
predictions = scaler.inverse_transform(predictions)
print(predictions)
print(type(predictions))
rmse = numpy.sqrt(numpy.mean(((predictions-y_test)**2)))
train = data[:training_data_len]
valid = data[training_data_len:]
valid["Predictions"] = predictions
print(valid)
pyplot.figure(figsize=(16, 8))
pyplot.xlabel("Date", fontsize=12)
pyplot.ylabel("Close Price USD ($)", fontsize=12)
pyplot.plot(train["Close"], linewidth=1)
pyplot.plot(valid[["Close", "Predictions"]], linewidth=1)
pyplot.legend(["Train", "Val", "Predictions"], loc="lower right")
pyplot.show()