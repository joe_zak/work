# import tkinter

# root = tkinter.Tk()
# root.title("Tkinter spreadsheet")

# w = 20
# h = 1
import tkinter

class App:
	def __init__(self, root):
		self.dict = {}
		self.entry = []
		self.sv = []
		self.root = root
		self.canvas = tkinter.Canvas(self.root, background="#ffffff", borderwidth=0)
		self.frame = tkinter.Frame(self.canvas, background="#ffffff")
		self.scrolly = tkinter.Scrollbar(self.root, orient="vertical", command=self.canvas.yview)
		self.scrollx = tkinter.Scrollbar(self.root, orient="horizontal", command=self.canvas.xview)
		self.canvas.configure(yscrollcommand=self.scrolly.set, xscrollcommand=self.scrollx.set)
		self.canvas.create_window((4, 4), window=self.frame, anchor="nw", tags="self.frame")
		self.scrolly.pack(side="left", fill="y")
		self.canvas.pack(side="top", fill="both", expand=True)
		self.scrollx.pack(side="bottom", fill="x")
		self.frame.bind("<Configure>", self.onFrameConfigure)
		alpha = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		for row in range(1, 21):
			self.entry.append([])
			self.sv.append([])
			for col in range(len(alpha)-1):
				if col == 0:
					label1 = tkinter.Label(self.frame, width=3, text=str(row))
					label1.grid(row=row, column=col, padx=2, pady=2)
				self.sv[row-1].append(tkinter.StringVar())
				self.sv[row-1][col].trace("w", lambda name, index, mode, sv=self.sv[row-1][col], row=row, col=col: self.callback(sv, row, col))
				entry = tkinter.Entry(self.frame, textvariable=self.sv[row-1][col])
				entry.grid(row=row+1, column=col)
				cell = "%s%s" % (alpha[col], row)
				self.dict[cell] = entry
				entry.bind("<Button-1>", lambda e, cell=cell: self.click(e, cell))
				entry.bind("<Button-3>", lambda e, cell=cell: self.click(e, cell))
				entry.bind("<Return>", lambda e, cell=cell: self.key_r(e, cell))
				self.entry[row-1].append(entry)
		self.dict["A1"].focus()

	def click(self, event, cell):
		root.title("you clicked mouse button %d in cell %s" % (event.num, cell))
		if event.num == 3:
			obj = self.dict[cell]
			data = obj.get()
			if data.startswith("="):
				eq = data.lstrip("=")
				try:
					result = eval(eq)
					obj.delete(0, "end")
					obj.insert(0, str(result))
				except Exception:
					pass

	def key_r(self, event, cell):
		data = self.dict[cell].get()
		root.title("cell %s contains %s" % (cell, data))

	def onFrameConfigure(self, event):
		self.canvas.configure(scrollregion=self.canvas.bbox("all"))

	def callback(self, sv, column, row):
		print("Column: "+str(column)+", Row: "+str(row)+" = "+sv.get())

root = tkinter.Tk()
App(root)
root.mainloop()