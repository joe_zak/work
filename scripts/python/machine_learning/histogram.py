import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.metrics import r2_score

nums = np.random.uniform(0.0, 10.0, 250)
nums2 = np.random.normal(5, 1, 100000)
x = np.random.normal(5, 1, 500)
y = np.random.normal(10, 2, 500)
x2 = [5,7,8,7,2,17,2,9,4,11,12,9,6]
y2 = [99,86,87,88,111,86,103,87,94,78,77,85,86]
x3 = [1,2,3,5,6,7,8,9,10,12,13,14,15,16,18,19,21,22]
y3 = [100,90,80,60,60,55,60,65,70,70,75,76,78,79,90,99,99,100]

slope, intercept, r, p, std_err = stats.linregress(x2, y2)

def f(x):
	return slope * x + intercept

model = list(map(f, x2))
model2 = np.poly1d(np.polyfit(x3, y3, 3))
line = np.linspace(1, 22, 100)
print(r2_score(y3, model2(x3)))

plt.plot([1,2,3])
plt.subplot(321)
plt.scatter(x, y)
plt.subplot(322)
plt.hist(nums, 10)
plt.subplot(323)
plt.hist(nums2, 100)
plt.subplot(324)
plt.scatter(x2, y2)
plt.plot(x2, model)
plt.subplot(325)
plt.scatter(x3, y3)
plt.plot(line, model2(line))
plt.show()