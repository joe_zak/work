import pandas
from sklearn import linear_model
from sklearn.preprocessing import StandardScaler

scale = StandardScaler()
df = pandas.read_csv("cars2.csv")
x = df[["Weight", "Volume"]]
y = df["CO2"]
scaledX = scale.fit_transform(x)

model = linear_model.LinearRegression()
model.fit(scaledX, y)

scaledP = scale.transform([[2300, 1.3]])
predictedCO2 = model.predict([scaledP[0]])
print(predictedCO2)