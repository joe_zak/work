import json
import requests
from bs4 import BeautifulSoup
import argparse
import os
import logging
from selenium.webdriver.common.keys import Keys
from splinter import Browser
import asyncio

# logging.basicConfig(level=logging.DEBUG)

DELAY = 3
DOMAIN = "https://www.vezeeta.com"

def getData(link, resume: bool):
	pass

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--search", help="query/ies to search for")
	parser.add_argument("-f", "--filename", help="filename of file containing queries")
	parser.add_argument("-d", "--deliminator", default="\n", help="deliminator between each query in case of multiple queries")
	parser.add_argument("-r", "--resume", action="store_true", help="continue where it left off")
	args = parser.parse_args()

	if args.search:
		search = args.search.lower()
	elif args.filename:
		with open(args.filename) as f:
			search = f.read()
	search = search.replace(" ", "-")
	queries = search.split(args.deliminator)

	os.environ["MOZ_FORCE_DISABLE_E10S"] = "1"
	browser = Browser()

	for query in queries:
		done = False
		page = 1
		while not done:
			if args.resume:
				if not os.path.isfile(f"out/{query}_{page}"):
					page += 1
					continue

			url = f"https://www.vezeeta.com/en/doctor/{query}/egypt?page={page}"

			browser.visit(url)
			links = browser.find_by_css("[data-testid*=name--name]")

			for link in links:
				link = link["href"]
				browser.execute_script(f"window.open('{link}', '_blank');")

				browser.windows.current = browser.windows[-1]

				data = browser.find_by_css("#__NEXT_DATA__").first
				browser.html_snapshot("test.html")
				# data = data.text
				# print(data)
				# data = json.loads(data)
				# data = data["props"]["initialState"]["profiles"]["doctorProfileData"]["Profile"]

				# result = {
				# 	"id": data["EntityId"],
				# 	"key": data["EntityKey"],
				# 	"isNewDoctor": data["IsNewDoctor"],
				# 	"vision": data["Vision"],
				# 	"professionalAffiliations": data["ProfessionalAffiliations"],
				# 	"detailedDescription": data["DetailedDescription"],
				# 	"name": data["EntityName"],
				# 	"shortDescription": data["ShortDescription"],
				# 	"image": data["ImageUrl"],
				# 	"offersHomeVisits": data["OffersHomeVisits"],
				# 	"fees": data["Fees"],
				# 	"title": data["DoctorNamePrefix"],
				# 	"professionalTitle": data["ProfessionalTitle"],
				# 	"mainSpecialty": {
				# 		"name": data["MainSpecialityName"],
				# 		"slug": data["MainSpecialtyUrl"],
				# 	},
				# 	"secondarySpecialties": [{
				# 		"name": x["Name"],
				# 		"slug": x["NameUrl"],
				# 	} for x in data["SecondarySpecialties"]],
				# 	"doctorRatingViewModel": {
				# 		"doctorOverallRating": data["DoctorRatingViewModel"]["DoctorOverallRatingPercentage"],
				# 		"facilityOverallRating": data["DoctorRatingViewModel"]["FacilityOverallRating"],
				# 		"waitingTime": data["DoctorRatingViewModel"]["FacilityPrefixTitle"],
				# 		"ratingsCount": data["DoctorRatingViewModel"]["RatingsCount"],
				# 		"isTopRated": data["DoctorRatingViewModel"]["IsTopRated"],
				# 		"doctorTags": data["DoctorRatingViewModel"]["DoctorTags"],
				# 		"contacts": [{
				# 			"type": x["ContactType"],
				# 			"address": x["Address"],
				# 			"cityName": x["CityName"],
				# 			"cityEnName": x["CityEnName"],
				# 			"areaName": x["AreaName"],
				# 			"areaEnName": x["AreaEnName"],
				# 			"acceptOnlinePayment": x["AcceptOnlinePayment"],
				# 			"acceptGlobalBooking": x["AcceptGlobalBooking"],
				# 			"followUpDetails": x["FollowUpDetails"],
				# 			"acceptLoyaltyPayment": x["AcceptLoyaltyPayment"],
				# 		} for x in data["Contacts"]],
				# 	},
				# 	"doctorServices": [{
				# 		"name": x["ServiceName"],
				# 		"slug": x["ServiceUrl"],
				# 		"isBookable": x["IsBookable"],
				# 		"fixedPrice": x["FixedPrice"],
				# 		"offerPrice": x["OfferPrice"],
				# 	} for x in data["DoctorServices"]],
				# 	"views": data["ViewedCount"],
				# 	"insuranceProviders": [{
				# 		"name": x["Name"],
				# 		"nameEn": x["NameEnglish"],
				# 		"nameAr": x["NameArabic"],
				# 		"slug": x["NameUrl"],
				# 		"longitude": x["Longitude"],
				# 		"latitude": x["Latitude"],
				# 	} for x in data["InsuranceProviders"]],
				# 	"insuranceProvidersList": [{
				# 		"name": x["Name"],
				# 		"nameEn": x["NameEnglish"],
				# 		"nameAr": x["NameArabic"],
				# 		"slug": x["NameUrl"],
				# 		"longitude": x["Longitude"],
				# 		"latitude": x["Latitude"],
				# 	} for x in data["InsuranceProvidersList"]],
				# 	"facilityImages": data["FacilityImages"],
				# 	"spokenLanguages": [{
				# 		"key": x["LanguageKey"],
				# 		"name": x["LanguageName"],
				# 	} for x in data["SpokenLanguages"]],
				# 	"gender": data["Gender"], # false male true female
				# 	"requiredPrePayment": data["RequirePrePayment"]
				# }

				# print(result)

				# browser.execute_script("window.close();")

			# if done:
			# 	os.mknod(f"out/{query}_{page}")
			done = True
			page += 1
	
	browser.close()

if __name__ == "__main__":
	main()
