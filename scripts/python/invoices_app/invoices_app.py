import cv2
from pyzbar import pyzbar

class Barcodes:
	def read_barcodes(self, frame):
		self.barcodes = pyzbar.decode(frame)
		for barcode in self.barcodes:
			x, y, w, h = barcode.rect
			barcode_info = barcode.data.decode("utf-8")
			cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
			font = cv2.FONT_HERSHEY_DUPLEX
			cv2.putText(frame, barcode_info, (x+6, y-6), font, 1, (255, 0, 0), 2)
		return frame

def main():
	barcode = Barcodes()

	# dev = cv2.VideoCapture(0)
	# ret, frame = dev.read()
	# while ret:
	# 	ret, frame = dev.read()
	# 	frame = barcode.read_barcodes(frame)
	# 	cv2.imshow("barcode", frame)
	# 	if cv2.waitKey(1) & 0xFF == ord('q'):
	# 		break
	# dev.release()
	# cv2.destroyAllWindows()

	while True:
		img = cv2.imread("barcode.jpg")
		frame = barcode.read_barcodes(img)
		cv2.imshow("barcode", frame)
		if cv2.waitKey(0) & 0xFF == ord('q'):
			break
	cv2.destroyAllWindows()

if __name__ == "__main__":
	main()