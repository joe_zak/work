import numpy
from matplotlib import pyplot
from scipy.io import wavfile

frequency_sampling, audio_signal = wavfile.read("En-us-active.wav")
print(f"signal shape: {audio_signal.shape}")
print(f"signal datatype: {audio_signal.dtype}")
print(f"signal duration: {round(audio_signal.shape[0]/float(frequency_sampling), 2)} seconds")
audio_signal = audio_signal / numpy.power(2, 15)
length_signal = len(audio_signal)
half_length = numpy.ceil((length_signal+1)/2.0).astype(numpy.int)
signal_frequency = numpy.fft.fft(audio_signal)
signal_frequency = abs(signal_frequency[0:half_length]) / length_signal
signal_frequency **= 2
len_fts = len(signal_frequency)
if length_signal % 2:
    signal_frequency[1:len_fts] *= 2
else:
    signal_frequency[1:len_fts-1] *= 2
signal_power = 10 * numpy.log10(signal_frequency)
x_axis = numpy.arange(0, half_length, 1) * (frequency_sampling / length_signal) / 1000.0
pyplot.figure()
pyplot.plot(x_axis, signal_power, color="black")
pyplot.xlabel("Frequency (kHz)")
pyplot.ylabel("Signal power (dB)")
pyplot.show()