#include <vector>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <string.h>
#include <openssl/aes.h>
#include <openssl/rand.h>

std::string hex_to_str(std::string hex){
	std::string str;
	int val;
	for (int i = 0; i < hex.size(); i++){
		std::stringstream ss;
		ss << hex.substr(i, 2);
		ss >> std::hex >> val;
		str.push_back(val);
		i++;
	}
	return str;
}

int main(int argc, char** argv){
	if (argc == 4){
		if (!strcmp(argv[1], "-e")){
			AES_KEY* aes_key = new AES_KEY();
			unsigned char IV[AES_BLOCK_SIZE];
			RAND_bytes(IV, sizeof(IV));
			IV[sizeof(IV)] = '\00';
			//std::cout << IV << "\x73\xef\xbf\xbd\xcc\x87\x66\xef\xbf\xbd\xef\xbf\xbd\xef\xbf\xbd\x22\xef\xbf\xbd" << std::endl;
			for (int i = 0; i < sizeof(IV); i++){
				std::cout << std::setfill('0') << std::setw(2) << std::hex << int(IV[i]);
			}
			std::cout << "73efbfbdcc8766efbfbdefbfbdefbfbd22efbfbd";
			AES_set_encrypt_key((unsigned char*)hex_to_str(argv[3]).c_str(), 256, aes_key);
			std::string txt(hex_to_str(argv[2]));
			int required_padding = (AES_BLOCK_SIZE - (txt.length() % AES_BLOCK_SIZE));
			std::vector<unsigned char> padded_txt(txt.begin(), txt.end());
			for (int i = 0; i < required_padding; i++){
				padded_txt.push_back(0);
			}
			unsigned char* user_data = &padded_txt[0];
			unsigned char encrypted_data[1024] = {0};
			AES_cbc_encrypt(user_data, encrypted_data, (const int)padded_txt.size(), (const AES_KEY*)aes_key, IV, AES_ENCRYPT);
			for (int i = 0; i < sizeof(encrypted_data); i++){
				std::cout << std::setfill('0') << std::setw(2) << std::hex << int(encrypted_data[i]);
			}
			//std::cout << encrypted_data;
		} else if (!strcmp(argv[1], "-d")){
			int start = 0, end = 0;
			std::string delimiter("73efbfbdcc8766efbfbdefbfbdefbfbd22efbfbd");
			end = std::string(argv[2]).find(delimiter);
			auto temp_IV = hex_to_str(std::string(argv[2]).substr(start, end));
			start = end + delimiter.size();
			auto data = hex_to_str(std::string(argv[2]).substr(start));
			AES_KEY* aes_key = new AES_KEY();
			AES_set_decrypt_key((unsigned char*)hex_to_str(argv[3]).c_str(), 256, aes_key);
			unsigned char decrypted_data[1024] = {0};
			AES_cbc_encrypt((unsigned char*)data.c_str(), decrypted_data, (const int)data.size(), (const AES_KEY*)aes_key, (unsigned char*)temp_IV.c_str(), AES_DECRYPT);
			//for (int i = 0; i < sizeof(decrypted_data); i++){
			//	std::cout << std::setfill('0') << std::setw(2) << std::hex << int(decrypted_data[i]);
			//}
			std::cout << decrypted_data;
		}
		return 0;
	} else {
		return 1;
	}
}