import tkinter as tk
import tkinter.ttk as ttk
import tkinter.scrolledtext
from sympy import solveset, symbols, Reals, init_printing, latex, Interval, pi, oo
from sympy.parsing.sympy_parser import parse_expr, standard_transformations, implicit_multiplication_application, convert_equals_signs
from sympy.calculus.util import continuous_domain, function_range
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import numpy
import re
import time

class App:
	def __init__(self):
		# self.root = wx.App()
		# window = wx.Frame(None, title="wxPython Frame", size=(500,500))
		# panel = wx.Panel(window)
		# label = wx.StaticText(panel, label="Hello World", pos=(100,50))
		# window.Show()
		self.root = tk.Tk()
		self.root.geometry("500x500")
		self.root.minsize(600, 600)
		self.root.bind("<Control-q>", lambda event: self.root.destroy())
		self.root.wm_title("Solver v0.3")

		self.style = ttk.Style(self.root)
		self.style.configure("Placeholder.TEntry", foreground="#d5d5d5")

		self.container = tk.Frame(self.root)
		self.container.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
		self.container.grid_rowconfigure(1, weight=1)
		self.container.grid_columnconfigure(0, weight=1)
		self.container.grid_columnconfigure(1, weight=1)

		solve = ttk.Button(self.container, text="Solve Equation", command=lambda: self.solve.tkraise())
		solve.grid(row=0, column=0, padx=10, pady=10, sticky="new")
		draw = ttk.Button(self.container, text="Draw Equation", command=lambda: self.draw.tkraise())
		draw.grid(row=0, column=1, padx=10, pady=10, sticky="new")

		self.solve = tk.Frame(self.container)
		self.solve.grid_rowconfigure(4, weight=1)
		self.solve.grid_columnconfigure(0, weight=1)
		self.solve.grid(row=1, column=0, columnspan=2, sticky="nsew")

		self.expr_e = CustomEntry(self.solve, placeholder="Expression...", font=("Monospace Regular", 14))
		self.expr_e.bind("<Return>", self.evaluate)
		self.expr_e.grid(row=0, column=0, padx=10, pady=10, sticky="new")
		self.domain_e = CustomEntry(self.solve, placeholder="Domain... (default is R)", font=("Monospace Regular", 14))
		self.domain_e.bind("<Return>", self.evaluate)
		self.domain_e.grid(row=1, column=0, padx=10, pady=10, sticky="new")
		self.var_e = CustomEntry(self.solve, placeholder="Solve for... (default is x)", font=("Monospace Regular", 14))
		self.var_e.bind("<Return>", self.evaluate)
		self.var_e.grid(row=2, column=0, padx=10, pady=10, sticky="new")
		inst_l = ttk.Label(self.solve, font=16)
		inst_l.config(text="""
Instructions:
'Abs(x)' is the absolute value of 'x' e.g. '|x|'\n also note that the 'A' must be capital
'log(x)' takes two arguments first is value and the second\n is the base e.g. 'log(x,10)'. Note: default base is 'e'
'sqrt(x)' is the square root of 'x'
'root(x,n)' is the 'n'th root of 'x' e.g. 'root(x,3)=5 x=125'
'x**n' is 'x' to the power of 'n'
		""")
		inst_l.grid(row=3, column=0, padx=10, pady=10, sticky="nsew")
		self.figure = Figure(figsize=(1,1))
		self.result = FigureCanvasTkAgg(self.figure, master=self.solve)
		self.result.get_tk_widget().grid(row=4, column=0, padx=10, pady=10, sticky="nsew")

		self.draw = tk.Frame(self.container)
		self.draw.grid_rowconfigure(0, weight=1)
		self.draw.grid_rowconfigure(1, weight=1)
		self.draw.grid_columnconfigure(0, weight=1)
		self.draw.grid(row=1, column=0, columnspan=2, sticky="nsew")

		self.func = CustomEntry(self.draw, placeholder="Function...", font=("Monospace Regular", 14))
		self.func.bind("<Return>", self.drawFunc)
		self.func.grid(row=0, column=0, padx=10, pady=10, sticky="new")
		self.df = Figure()
		self.draw_f = FigureCanvasTkAgg(self.df, master=self.draw)
		self.draw_f.get_tk_widget().grid(row=1, column=0, padx=10, pady=10, sticky="nsew")

	def drawFunc(self, event):
		x, y = symbols("x y")
		transformations = (standard_transformations + (implicit_multiplication_application, convert_equals_signs))
		parsed = parse_expr(event.widget.get().replace("^", "**"), locals(), transformations=transformations)
		x_values = []
		y_values = []
		for i in numpy.linspace(-5, 5, 100):
			args = solveset(parsed.subs(x, i), y, Reals).args
			if len(args) == 1:
				y_values.append(args[0])
				x_values.append(i)
		self.df.clf()
		ax = self.df.add_subplot(111)
		ax.set_title(rf"${latex(parsed)}$")
		ax.grid(which="major", linestyle="-", linewidth="0.5")
		ax.grid(which="minor", linestyle=":", linewidth="0.5")
		ax.minorticks_on()
		ax.axhspan(0, 0, linewidth=2, color="black")
		ax.axvline(0, linewidth=2, color="black")
		ax.margins(0.1, 0.1)
		# ax.scatter(x_values, y_values, marker=".")
		ax.plot(x_values, y_values)
		self.draw_f.draw()
		# self.df.savefig(f"{time.time()}.png", bbox_inches="tight")

	def evaluate(self, event=None):
		# try:
		x, y, z = symbols("x y z")
		transformations = (standard_transformations + (implicit_multiplication_application, convert_equals_signs))
		parsed = parse_expr(self.expr_e.get().replace("^", "**"), locals(), transformations=transformations)
		domain = self.domain_e.get()
		if domain and "Domain" not in domain:
			domain = domain.replace(" ", "")
			if re.search(r"(^[\[\(]\-?(?:\-?\d*|\-oo|\-?\d*\/?\-?\d*\*?pi)\,(?:\-?\d*|oo|\-?\d*\/?\-?\d*\*?pi)[\]\)]$)", domain, re.IGNORECASE):
				left = domain.startswith("(")
				right = domain.endswith(")")
				domain = domain[1:-1].split(",")
				domain = Interval(eval(domain[0]), eval(domain[1]), left, right)
			else:
				raise Exception("Check your domain")
		else:
			domain = Reals
		solution = latex(solveset(parsed, x, domain))
		result = rf"${latex(parsed)}$      $x \in {latex(domain)}$"+"\n"+rf"Solution: ${solution}$"
		# except Exception as msg:
		# 	result = msg
		# finally:
		self.figure.clf()
		ax = self.figure.add_subplot(111)
		ax.set_xticks([])
		ax.set_yticks([])
		ax.axis("off")
		ax.text(0.5, 0.5, result, size=14, ha="center", va="center")
		self.result.draw()
		# self.figure.savefig(f"{time.time()}.png", bbox_inches="tight")

	def run(self):
		self.root.mainloop()
		# self.root.MainLoop()

class CustomEntry(ttk.Entry):
	def __init__(self, container, *args, placeholder=None, **kargs):
		super().__init__(container, *args, style="Placeholder.TEntry", **kargs)
		self.bind("<Control-KeyRelease-a>", lambda event: (event.widget.select_range(0, "end"), event.widget.icursor("end")))
		if placeholder:
			self.placeholder = placeholder
			self.insert("0", placeholder)
			self.bind("<FocusIn>", self._clear_placeholder)
			self.bind("<FocusOut>", self._add_placeholder)

	def _clear_placeholder(self, event):
		if self["style"] == "Placeholder.TEntry":
			self.delete("0", "end")
			self["style"] = "TEntry"

	def _add_placeholder(self, event):
		if not self.get():
			self.insert("0", self.placeholder)
			self["style"] = "Placeholder.TEntry"

if __name__ == "__main__":
	init_printing(use_latex="matplotlib")
	app = App()
	app.run()