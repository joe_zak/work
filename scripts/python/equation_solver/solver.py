from sympy import symbols, latex, Interval, pi, oo, Reals, solveset
from sympy.parsing.sympy_parser import parse_expr, standard_transformations, implicit_multiplication_application, convert_equals_signs
import numpy as np
import re

def evaluate(expr_e, axes, domain=None):
		# try:
		x, y = symbols("x y")
		transformations = (standard_transformations + (implicit_multiplication_application, convert_equals_signs))
		parsed = parse_expr(expr_e.replace("^", "**"), locals(), transformations=transformations)
		if domain:
			domain = domain.replace(" ", "")
			if re.search(r"(^[\[\(]\-?(?:\-?\d*|\-oo|\-?\d*\/?\-?\d*\*?pi)\,(?:\-?\d*|oo|\-?\d*\/?\-?\d*\*?pi)[\]\)]$)", domain, re.IGNORECASE):
				left = domain.startswith("(")
				right = domain.endswith(")")
				domain = domain[1:-1].split(",")
				domain = Interval(eval(domain[0]), eval(domain[1]), left, right)
			else:
				raise Exception("Check your domain")
		else:
			domain = Reals
		solution = latex(solveset(parsed, x, domain))
		result = rf"${latex(parsed)}$      $x \in {latex(domain)}$"+"\n"+rf"Solution: ${solution}$"
		# except Exception as msg:
		# 	result = msg
		# finally:
		axes.clear()
		axes.set_xticks([])
		axes.set_yticks([])
		axes.axis("off")
		axes.text(0.5, 0.5, result, size=14, ha="center", va="center")
		# return result

def draw(expr_e, axes):
	x, y = symbols("x y")
	transformations = (standard_transformations + (implicit_multiplication_application, convert_equals_signs))
	parsed = parse_expr(expr_e.replace("^", "**"), locals(), transformations=transformations)
	x_values = []
	y_values = []
	for i in np.linspace(-5, 5, 100):
		args = solveset(parsed.subs(x, i), y, Reals).args
		if len(args) == 1:
			y_values.append(args[0])
			x_values.append(i)
	axes.clear()
	axes.set_title(rf"${latex(parsed)}$")
	axes.grid(which="major", linestyle="-", linewidth="0.5")
	axes.grid(which="minor", linestyle=":", linewidth="0.5")
	axes.minorticks_on()
	axes.axhspan(0, 0, linewidth=2, color="black")
	axes.axvline(0, linewidth=2, color="black")
	axes.margins(0.1, 0.1)
	axes.plot(x_values, y_values)
	# return rf"${latex(parsed)}$"

if __name__ == "__main__":
	pass
