import sys, os
from cx_Freeze import setup, Executable

__version__ = "1.1.0"

packages = ["os", "socket", "subprocess"]

setup(
	name = "appname",
	description="App Description",
	version=__version__,
	options = {"build_exe": {
		"packages": packages,
		"include_files": [],
		"excludes": [],
		"include_msvcr": True,
	}},
	executables = [Executable("b2b_conn.py", base="Win32GUI")]
)