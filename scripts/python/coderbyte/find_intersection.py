"""
Find Intersection
Have the function FindIntersection(strArr) read the array of strings stored in strArr which will contain 2 elements: the first element will represent a list of comma-separated numbers sorted in ascending order, the second element will represent a second list of comma-separated numbers (also sorted). Your goal is to return a comma-separated string containing the numbers that occur in elements of strArr in sorted order. If there is no intersection, return the string false.
Examples
Input: ["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"]
Output: 1,4,13
Input: ["1, 3, 9, 10, 17, 18", "1, 4, 9, 10"]
Output: 1,9,10
"""
import math

def FindIntersection(strArr):
	a = set(map(int, map(str.strip, strArr[0].split(","))))
	b = set(map(int, map(str.strip, strArr[1].split(","))))
	intersection = a.intersection(b)
	if intersection:
		strArr = ",".join(map(str, sorted(intersection)))
	else:
		strArr = "false"
	# for i, num in enumerate(a):
	# 	x = len(b)/2
	# 	for i2, num2 in enumerate(b):
	# 		if x % 1:
	# 			middle = math.floor(x)
	# 		else:
	# 			middle = x
	# 		if int(b[middle]) == int(num):
	# 			return num
	return strArr

print(FindIntersection(["1, 3, 4, 7, 13", "1, 2, 4, 13, 15"]))