from urllib.parse import quote
from bs4 import BeautifulSoup
from threading import Thread
from sys import platform
from time import sleep
from os import system
import requests
import json
import re

if platform == "linux" or platform == "linux2":
	import readline
	RED = "\033[31m"
	GREEN = "\033[32m"
	YELLOW = "\033[33m"
	CLEAR = "\033[00m"
else:
	RED = GREEN = YELLOW = CLEAR = ""

def p_print():
	chars = ["-", "\\", "|", "/"]
	print()
	while links.check:
		for i in chars:
			print(f"{GREEN}[+]{CLEAR} Loading {i}\r", end='', flush=True)
			sleep(0.1)

class data():
	all = {"fb": True, "tw": True, "ig": True}
	def get_fb(self, username):
		soup = BeautifulSoup(re.findall(r"<!--(.*)-->", requests.get(f"http://www.facebook.com/public/{username}", headers={"Accept-Language": "en-US,en;q=0.5"}).text)[0], "html.parser")
		self.fb = soup.find("div", {"id": "BrowseResultsContainer"})

	def get_tw(self, username):
		self.tw = json.loads(requests.get(f"https://twitter.com/i/search/typeahead.json?q={username}").text)["users"]

	def get_ig(self, username):
		self.ig = json.loads(requests.get(f"https://www.instagram.com/web/search/topsearch/?query={username}").text)["users"]

if __name__ == "__main__":
	links = data()
	while True:
		try:
			print(f"\t{YELLOW}( fb={RED}{links.all['fb']}{YELLOW}, tw={RED}{links.all['tw']}{YELLOW}, ig={RED}{links.all['ig']}{YELLOW} ){CLEAR}")
			username = quote(input(f"{YELLOW}username >{CLEAR} "), safe='').lower()
			if username:
				specify = username.split("%3d")
				if len(specify) == 2:
					if specify[0] in ["fb", "tw", "ig"]:
						if specify[1] == "false":
							links.all[specify[0]] = False
							print()
							continue
						if specify[1] == "true":
							links.all[specify[0]] = True
							print()
							continue
			else:
				print()
				continue
			if username == "%21c":
				system("clear")
				continue
			if username == "%21q":
				break
		except (KeyboardInterrupt, EOFError):
			print()
			break
		try:
			if links.all["fb"] or links.all["tw"] or links.all["ig"]:
				if "%3d" in username:
					print(f"\n{RED}[-]{CLEAR} Invalid string.\n")
					continue
				links.check = True
				Thread(target=p_print).start()
				if links.all["fb"]:
					links.get_fb(username)
				if links.all["tw"]:
					links.get_tw(username)
				if links.all["ig"]:
					links.get_ig(username)
				links.check = False
				sleep(0.5)
				if links.all["fb"]:
					print(f"{GREEN}[+]{CLEAR} Facebook:")
					for n, i in enumerate(links.fb.find_all("div", {"class": "clearfix _ikh"})):
						for x in i.find_all(name="a"):
							if "ajaxify" not in x.attrs:
								if x.img:
									print(f"\t{GREEN}[{n+1}]{CLEAR} {x['href']}")
									print(f"\t{GREEN}[{n+1}]{CLEAR} profile_image_url: {x.img['src'].replace('&amp;', '&')}")
								elif x.span:
									print(f"\t{GREEN}[{n+1}]{CLEAR} name: {x.span.text}")
						for x in i.find_all("div", {"class": "_pac"}):
							if "Works" in x.text:
								print(f"\t{GREEN}[{n+1}]{CLEAR} works_for: {x.a['href']} ({x.a.text})")
						for x in i.find_all("div", {"class": "_52eh"}):
							if "Studied" in x.text:
								print(f"\t{GREEN}[{n+1}]{CLEAR} studied_at: {x.a['href']} ({x.a.text})")
							elif "Lives" in x.text:
								print(f"\t{GREEN}[{n+1}]{CLEAR} lives_in: {x.a['href']} ({x.a.text})")
							elif "Went" in x.text:
								print(f"\t{GREEN}[{n+1}]{CLEAR} went_to: {x.a['href']} ({x.a.text})")
						print("\t", YELLOW, '*'*100, CLEAR, sep="")
					print()
				if links.all["tw"]:
					print(f"{GREEN}[+]{CLEAR} Twitter:")
					for n, i in enumerate(links.tw):
						print(f"\t{GREEN}[{n+1}]{CLEAR} https://twitter.com/{i['screen_name']}")
						for x in i:
							print(f"\t{GREEN}[{n+1}]{CLEAR} {x}: {i[x]}")
						print("\t", YELLOW, '*'*100, CLEAR, sep="")
					print()
				if links.all["ig"]:
					print("{}[+]{} Instagram:".format(GREEN, CLEAR))
					for n, i in enumerate(links.ig):
						print(f"\t{GREEN}[{n+1}]{CLEAR} https://www.instagram.com/{i['user']['username']}/")
						for x in i["user"]:
							print(f"\t{GREEN}[{n+1}]{CLEAR} {x}: {i['user'][x]}")
						print("\t", YELLOW, '*'*100, CLEAR, sep="")
					print()
			else:
				print(f"\n{RED}[-]{CLEAR} Check your settings.\n")
		except (KeyboardInterrupt, EOFError):
			links.check = False
			sleep(0.5)
			print()
			continue