import requests
import argparse
import subprocess
import shutil
import tqdm
import os
import multiprocessing
import time
import glob

def remove(link):
	if "=" in link:
		return link.split("=")[-1]
	elif "/" in link:
		return link.split("/")[-1]
	return link

def get_info(id, instance):
	return requests.get(f"{instance}/api/v1/videos/{id}?fields=title,adaptiveFormats,captions").json()

def progress(t, f, s):
	s = int(s)
	while True:
		size = f.tell()
		print(f"\r Downloading \"{t}\" {int((size/s)*100):3}%", end="", flush=True)
		time.sleep(.5)

def download(url, fname, size):
	mode = "wb"
	headers = {}
	if os.path.isfile(fname):
		mode = "ab"
		with open(fname, mode) as f:
			pos = f.tell()
		headers["Range"] = f"bytes={pos}-"
	with requests.get(url, headers=headers, stream=True) as r:
		if r.status_code in (200, 206):
			with open(fname, mode) as f:
				t = multiprocessing.Process(target=progress, args=(fname, f, size))
				t.start()
				shutil.copyfileobj(r.raw, f)
				t.terminate()
			return True
		else:
			print("Error: ", r.status_code)
	return False

def main(id, instance):
	data = get_info(id, instance)
	filename = f"{data['title']}-{id}"
	if glob.glob(f"{filename}.mkv"):
		print(f"{filename} already downloaded")
		exit()
	video = None
	audio = None
	resolution = [0, 0]
	for F in data["adaptiveFormats"]:
		if "video" in F["type"]:
			if "resolution" in F:
				vtemp = int(F["resolution"][:-1])
				if resolution[0] < vtemp:
					resolution[0] = vtemp
					video = F
		else:
			atemp = int(F["itag"])
			if resolution[1] < atemp:
				resolution[1] = atemp
				audio = F
	vname = f"{filename}.{video['itag']}.{video['container']}"
	aname = f"{filename}.{audio['itag']}.{audio['container']}"
	if download(video["url"], vname, video["clen"]) and \
		download(audio["url"], aname, audio["clen"]):
		subprocess.Popen((
			"ffmpeg","-i", vname, "-i", aname,
			"-acodec", "copy", "-vcodec", "copy",
			f"{filename}.mkv"
		)).wait()
		os.remove(vname)
		os.remove(aname)
		print("Done\n")

def print_formats(id, instance):
	for F in get_info(id, instance)["adaptiveFormats"]:
		print(f"itag={F['itag']:5} type={F['type']:34}", end="")
		if "video" in F["type"]:
			if "resolution" in F:
				print(f"  resolution={F['resolution']}")
			else:
				print()
		else:
			print()

instance = "https://invidious.snopyta.org"
parser = argparse.ArgumentParser()
parser.add_argument("-i", type=str, metavar="instance")
parser.add_argument("-F", action="store_true")
parser.add_argument("links", nargs="*")
args = parser.parse_args()
args.links = map(remove, args.links)
if args.i:
	instance = args.i
for id in args.links:
	if args.F:
		print_formats(id, instance)
	else:
		main(id, instance)