from pynput import keyboard

def on_press(key):
    try:
        print("alphanumric key {} pressed".format(key.char))
    except AttributeError:
        print("special key {} pressed".format(key))

def on_release(key):
    print("{} released".format(key))
    if key == keyboard.Key.esc:
        return False

with keyboard.Listener(on_press=on_press, on_release=on_release) as Listener:
    Listener.join()