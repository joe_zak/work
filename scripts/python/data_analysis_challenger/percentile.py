from main import parse
import numpy as np
import readline

if __name__ == "__main__":
	nums = parse(input("> "))
	print("25%:", np.percentile(nums, 25))
	print("50%:", np.percentile(nums, 50))
	print("75%:", np.percentile(nums, 75))