from main import parse
from measures_of_center import mean
import math
import numpy as np

def variance(nums):
	m = mean(nums)
	for i in range(len(nums)):
		nums[i] = (nums[i] - m)**2
	return sum(nums) / len(nums)

if __name__ == "__main__":
	nums = parse(input("> "))
	v = variance(nums)
	print("Variance:", v)
	print("Standard Daviation:", math.sqrt(v))
	print("Variance:", np.var(nums))
	print("Standard Daviation:", np.std(nums))