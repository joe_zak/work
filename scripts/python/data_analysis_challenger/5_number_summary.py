from measures_of_center import median
from main import parse
import math

def q(nums, q2):
    q1 = 0
    q3 = 0
    if len(nums) % 2 != 0:
        del nums[(len(nums)//2)-1]
    if len(nums) % 4 == 0:
        i1 = len(nums) // 4
        i3 = i1 * 3
        q1 = (nums[i1-1] + nums[i1]) / 2
        q3 = (nums[i3-1] + nums[i3]) / 2
    else:
        i1 = len(nums) // 4
        i3 = math.ceil(len(nums) * 3 / 4)
        print(i1, i3)
        q1 = nums[i1-1]
        q3 = nums[i3-1]
    return q1, q3

if __name__ == "__main__":
    nums = parse(input("> "))
    minimum = min(nums)
    maximum = max(nums)
    q2 = median(nums)
    q1, q3 = q(nums, q2)
    print("Min:", minimum)
    print("Max:", maximum)
    print("Q1:", q1)
    print("Q2:", q2)
    print("Q3:", q3)
    print("Range:", maximum - minimum)
    print("Interquartile range:", q3 - q1)
