from main import parse
import numpy as np
from scipy import stats


def mean(nums: list):
	return sum(nums) / len(nums)


def median(nums: list):
	nums = list(sorted(nums))
	if len(nums) % 2 == 0:
		i = len(nums) // 2 - 1
		result = (nums[i] + nums[i + 1]) / 2
	else:
		result = nums[len(nums) // 2]
	return result


def mode(nums: list):
	result = [0, []]
	for n in nums:
		count = nums.count(n)
		if count > result[0]:
			result[0] = count
			result[1] = [n]
		elif count == result[0]:
			result[1].append(n)
	return result[1]


if __name__ == "__main__":
	nums = parse(input("> "))
	print("Mean:", mean(nums))
	print("Median:", median(nums))
	print("Mode:", mode(nums))
	print()
	print("Mean:", np.mean(nums))
	print("Median:", np.median(nums))
	print("Mode:", stats.mode(nums)[0])
