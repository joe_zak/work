#!/usr/bin/env python3
from socket import AF_INET, SOCK_STREAM, socket
from bs4 import BeautifulSoup
from threading import Thread
import subprocess
import binascii
import hashlib

"""
message = binascii.hexlify(b"idk XD")
key = hashlib.sha256(b"i don't know").hexdigest()
result = subprocess.Popen("./encryptor -e {} {}".format(message.decode("utf-8"), key), shell=True, stdout=subprocess.PIPE)
print("$ ./encryptor -e {} {}".format(message.decode("utf-8"), key), end="\n\n")
result = result.stdout.read()
print(result, end="\n\n")
print("$ ./encryptor -d {} {}".format(result.decode("utf-8"), key), end="\n\n")
result = subprocess.Popen("./encryptor -d {} {}".format(result.decode("utf-8"), key), shell=True, stdout=subprocess.PIPE)
result = result.stdout.read()
print(result, end="\n\n")
"""

def check(conn, username):
	while True:
		try:
			conn.send("\00".encode())
		except ConnectionResetError:
			del conns[conns.index(conn)]
			break

def recv(conn, username):
	while True:
		try:
			data = conn.recv(1024)
			print("{}: {}".format(username, data.decode("utf-8")))
			for i in conns:
				if i != conn:
					i.sendall(data)
		except ConnectionResetError:
			del conns[conns.index(conn)]
			break

def listen(s):
	while True:
		conn, addr = s.accept()
		conns.append(conn)
		print("[+] {} has connected".format(addr))
		data = conn.recv(1024)[:-1].decode("utf-8")
		soup = BeautifulSoup(data, "html.parser")
		username = soup.find("username").string
		key = soup.find("password").string
		print("[+] {} has joined the server".format(username))
		Thread(target=check, name=check, args=(conn, username)).start()
		Thread(target=recv, name=recv, args=(conn, username)).start()

conns = []
s = socket(AF_INET, SOCK_STREAM)
s.bind(("0.0.0.0", 8080))
s.listen(10)
Thread(target=listen, name=listen, args=(s,)).start()