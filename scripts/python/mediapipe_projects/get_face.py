import cv2
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands

cap = cv2.VideoCapture(0)
with mp_hands.Hands(min_detection_confidence=0.8, min_tracking_confidence=0.8) as hands:
	while cap.isOpened():
		success, img = cap.read()
		if not success:
			print("Ignoring empty camera frame")
			continue

		img = cv2.cvtColor(cv2.flip(img, 1), cv2.COLOR_BGR2RGB)

		img.flags.writeable = False
		results_hand = hands.process(img)
		img.flags.writeable = True

		img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
		if results_hand.multi_hand_landmarks:
			for hand_landmarks in results_hand.multi_hand_landmarks:
				mp_drawing.draw_landmarks(image=img, landmark_list=hand_landmarks, connections=mp_hands.HAND_CONNECTIONS)
		cv2.imshow("FaceMesh", img)
		if cv2.waitKey(5) & 0xFF == 27:
			break
cap.release()