from pymongo import MongoClient
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from create import CreateStatuteList

CONNECTION_STRING = "mongodb://localhost:27017"

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

@app.route("/")
@cross_origin()
def index():
	client = MongoClient(CONNECTION_STRING)
	db = client["data"]
	return jsonify(db.list_collection_names())

@app.route("/statute_list", methods=["POST", "GET"])
@cross_origin(methods=["POST"])
def createStatuteList():
	data = request.get_json()
	CreateStatuteList(data)
	client = MongoClient(CONNECTION_STRING)
	db = client["data"]
	db[data["name"]].insert_one(data)
	return ""

@app.route("/statute_list", methods=["UPDATE"])
@cross_origin(methods=["UPDATE"])
def updateStatuteList():
	data = request.get_json()
	CreateStatuteList(data)
	client = MongoClient(CONNECTION_STRING)
	db = client["data"]
	db[data["name"]].update_one({"name": data["name"]}, data)
	return ""

if __name__ == "__main__":
	app.run(debug=True, threaded=True)
