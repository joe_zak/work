import { Table, Typography, Button } from "antd";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import React from "react";
import { Link, useHistory } from "react-router-dom";

import MyLayout from "../components/layout.component";

interface HomeScreenProps {
	items: Array<String>;
}

function HomeScreen({ items }: HomeScreenProps) {
	const history = useHistory();

	const dataSource = items.map((item: String) => ({
		name: item,
	}));

	const columns = [
		{
			title: "الاسم",
			dataIndex: "name",
			render: (text: String) => (
				<Link to={`/info/${text}`} component={Typography.Link}>
					{text}
				</Link>
			),
		},
		{
			title: "إزالة",
			dataIndex: "name",
			render: (text: String) => (
				<Button
					danger
					onClick={() => {
						history.push(`/delete/${text}`);
					}}
					icon={<DeleteOutlined />}
				></Button>
			),
		},
	];

	return (
		<MyLayout>
			<Table dataSource={dataSource} columns={columns} />
			<Button
				type="primary"
				icon={<PlusOutlined />}
				onClick={history.push("/add")}
			>
				إضافة
			</Button>
		</MyLayout>
	);
}

export default HomeScreen;
