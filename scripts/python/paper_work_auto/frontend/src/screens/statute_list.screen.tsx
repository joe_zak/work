import { Button, Space, Form, Tabs } from "antd";
import React, { useState } from "react";

import { StatuteListSteps } from "./steps/statute_list.steps";
import "../App.less";
import MyLayout from "../components/layout.component";

const { TabPane } = Tabs;

interface StatuteListProps {
	match: { params: { name: String } };
}

function StatuteListScreen({
	match: {
		params: { name },
	},
}: StatuteListProps) {
	const [current, setCurrent] = useState(0);

	const next = () => {
		const newCurrent = current + 1;
		if (newCurrent < StatuteListSteps.length) setCurrent(newCurrent);
	};

	const previous = () => {
		const newCurrent = current - 1;
		if (newCurrent >= 0) setCurrent(current - 1);
	};

	const submit = (v: any) => {
		v.registration_day = v.registration_date.date();
		v.registration_month = v.registration_date.month();
		v.registration_year = v.registration_date.year();
		delete v.registration_date;
		fetch("http://localhost:5000/statute_list", {
			method: "POST",
			headers: {
				"Content-Type": "Application/json",
			},
			body: JSON.stringify(v),
		});
	};

	const [belong, setBelong] = useState(0);

	return (
		<MyLayout header={name}>
			<div style={{ paddingBlock: "20px" }}>
				<Space direction="vertical" style={{ width: "100%" }}>
					<Form name="statute_list" onFinish={submit}>
						<Tabs defaultActiveKey="0" activeKey={current.toString()}>
							{StatuteListSteps.map((step, index) => (
								<TabPane tab={step.title} key={index}>
									{step.content(belong, setBelong)}
								</TabPane>
							))}
						</Tabs>
					</Form>
				</Space>
			</div>
			{current < StatuteListSteps.length - 1 && (
				<Button onClick={next}>Next</Button>
			)}
			{current > 0 && <Button onClick={previous}>Previous</Button>}
		</MyLayout>
	);
}

export default StatuteListScreen;
