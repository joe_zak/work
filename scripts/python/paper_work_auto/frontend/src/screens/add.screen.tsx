import React, { useState } from "react";
import { Typography, Radio, RadioChangeEvent } from "antd";
import { Link } from "react-router-dom";

import MyLayout from "../components/layout.component";

function AddScreen() {
	const [type, setType] = useState(0);

	return (
		<MyLayout>
			<Radio.Group
				defaultValue={0}
				onChange={(e: RadioChangeEvent) => setType(e.target.value)}
			>
				<Radio value={0}>مؤسسة</Radio>
				<Radio value={1}>جمعية</Radio>
			</Radio.Group>
			<Link to={`/add/${type}/statute_list`}>لائحة النظام الأساسي</Link>
			<Link to={`/add/${type}/meeting_minutes`}>المحاضر</Link>
			<Link to={`/add/${type}/delegate`}>المفوض</Link>
			<Link to={`/add/${type}/reconciliation`}>طلب توفيق الاوضاع</Link>
		</MyLayout>
	);
}

export default AddScreen;
