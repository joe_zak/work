import React from "react";
import { Typography } from "antd";
import { Link } from "react-router-dom";

const { Title, Text } = Typography;

import MyLayout from "../components/layout.component";

function InfoScreen({
	match: {
		params: { name },
	},
}: any) {
	return (
		<MyLayout header={name}>
			<Link to={`/statute_list/${name}`} component={Typography.Link}>
				لائحة النظام الأساسي
			</Link>
		</MyLayout>
	);
}

export default InfoScreen;
