import React, { useState } from "react";
import {
	Form,
	Select,
	InputNumber,
	Button,
	Space,
	DatePicker,
	Radio,
} from "antd";
import {
	getTwoToneColor,
	MinusCircleOutlined,
	MinusCircleTwoTone,
	PlusCircleTwoTone,
	setTwoToneColor,
} from "@ant-design/icons";
import { RequiredInput, OptionalInput } from "../../components/input.component";

const { Option } = Select;

const StatuteListSteps = [
	{
		title: "مادة 1",
		content: () => {
			return (
				<>
					<RequiredInput itemProps={{ label: "اسم المؤسسة", name: "name" }} />
					<RequiredInput
						itemProps={{
							label: "مقيدة برقم",
							name: "registration_number",
						}}
						inputProps={{ type: "number" }}
					/>
					<Form.Item
						label="تاريخ القيد"
						name="registration_date"
						rules={[{ required: true }]}
					>
						<DatePicker style={{ display: "block" }} placeholder="" />
					</Form.Item>
					<RequiredInput itemProps={{ label: "المقر", name: "headquarters" }} />
					<RequiredInput itemProps={{ label: "الادارة", name: "management" }} />
					<RequiredInput
						itemProps={{
							label: "مجال العمل الأساسي",
							name: "main_field",
						}}
					/>
				</>
			);
		},
	},

	{
		title: "مادة 2",
		content: () => {
			return (
				<>
					<Form.Item
						label="نطاق عملها الجغرافي"
						name="scope"
						rules={[{ required: true }]}
					>
						<Select style={{ width: "100%" }}>
							<Option value="الجمهورية">الجمهورية</Option>
							<Option value="المحافظة">المحافظة</Option>
							<Option value="المدينة">المدينة</Option>
							<Option value="المركز">المركز</Option>
							<Option value="الحي">الحي</Option>
							<Option value="القسم">القسم</Option>
							<Option value="القرية">القرية</Option>
						</Select>
					</Form.Item>
					<Form.List name="branches">
						{(fields, { add, remove }) => (
							<>
								<ol>
									{fields.map(({ key, name, fieldKey, ...restField }) => (
										<li>
											<Space
												key={key}
												style={{ display: "flex", marginBottom: 8 }}
												align="baseline"
											>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "name"],
														fieldKey: [fieldKey, "name"],
													}}
													inputProps={{ placeholder: "اسم الفرع" }}
												/>
												<Form.Item
													{...restField}
													name={[name, "registration_number"]}
													fieldKey={[fieldKey, "registration_number"]}
													rules={[{ required: true }]}
												>
													<InputNumber placeholder="رقم القيد" />
												</Form.Item>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "governorate"],
														fieldKey: [fieldKey, "governorate"],
													}}
													inputProps={{ placeholder: "المحافظة" }}
												/>
												<MinusCircleTwoTone
													onClick={() => remove(name)}
													twoToneColor={["#ff686a", "#ffdcdc"]}
												/>
											</Space>
										</li>
									))}
								</ol>
								<Form.Item>
									<Button
										type="dashed"
										onClick={() => add()}
										icon={<PlusCircleTwoTone />}
										block
									>
										إضافة فرع
									</Button>
								</Form.Item>
							</>
						)}
					</Form.List>
				</>
			);
		},
	},

	{
		title: "مادة 3",
		content: () => {
			return (
				<Form.List name="fields">
					{(fields, { add, remove }) => (
						<>
							<ol>
								{fields.map(({ key, name, fieldKey, ...restField }) => (
									<li>
										<Space
											key={key}
											style={{ display: "flex", marginBottom: 8 }}
											align="baseline"
										>
											<RequiredInput
												itemProps={{
													...restField,
													name,
													fieldKey,
												}}
												inputProps={{ placeholder: "الميدان" }}
											/>
											<MinusCircleTwoTone
												onClick={() => remove(name)}
												twoToneColor={["#ff686a", "#ffdcdc"]}
											/>
										</Space>
									</li>
								))}
							</ol>
							<Form.Item>
								<Button
									type="dashed"
									onClick={() => add()}
									icon={<PlusCircleTwoTone />}
									block
								>
									إضافة ميدان
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			);
		},
	},

	{
		title: "مادة 4",
		content: () => {
			return (
				<Form.List name="activities">
					{(fields, { add, remove }) => (
						<>
							<ol>
								{fields.map(({ key, name, fieldKey, ...restField }) => (
									<li>
										<Space
											key={key}
											style={{ display: "flex", marginBottom: 8 }}
											align="baseline"
										>
											<RequiredInput
												itemProps={{
													...restField,
													name,
													fieldKey,
												}}
												inputProps={{ placeholder: "النشاط" }}
											/>
											<MinusCircleTwoTone
												onClick={() => remove(name)}
												twoToneColor={["#ff686a", "#ffdcdc"]}
											/>
										</Space>
									</li>
								))}
							</ol>
							<Form.Item>
								<Button
									type="dashed"
									onClick={() => add()}
									icon={<PlusCircleTwoTone />}
									block
								>
									إضافة نشاط
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			);
		},
	},

	{
		title: "مادة 6",
		content: () => {
			return (
				<Form.List name="capitals">
					{(fields, { add, remove }) => (
						<>
							<ol>
								{fields.map(({ key, name, fieldKey, ...restField }) => (
									<li>
										<Space
											key={key}
											style={{ display: "flex", marginBottom: 8 }}
											align="baseline"
										>
											<Form.Item
												{...restField}
												name={[name, "name"]}
												fieldKey={[fieldKey, "name"]}
											>
												<Select
													style={{ width: "100%" }}
													placeholder="رأس المال"
												>
													<Option value="نقود">نقود</Option>
													<Option value="عقار">عقار</Option>
													<Option value="منقولات">منقولات</Option>
													<Option value="أوراق مالية">أوراق مالية</Option>
													<Option value="ريع أو عائد بيع أي مما سبق">
														ريع أو عائد بيع أي مما سبق
													</Option>
												</Select>
											</Form.Item>
											<RequiredInput
												itemProps={{
													...restField,
													name: [name, "value"],
													fieldKey: [fieldKey, "value"],
												}}
												inputProps={{ placeholder: "قيمة المال" }}
											/>
											<OptionalInput
												itemProps={{
													name: [name, "bond"],
													fieldKey: [fieldKey, "bond"],
												}}
												inputProps={{ placeholder: "سند" }}
											/>
											<MinusCircleTwoTone
												onClick={() => remove(name)}
												twoToneColor={["#ff686a", "#ffdcdc"]}
											/>
										</Space>
									</li>
								))}
							</ol>
							<Form.Item>
								<Button
									type="dashed"
									onClick={() => add()}
									icon={<PlusCircleTwoTone />}
									block
								>
									إضافة رأس مال
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			);
		},
	},

	{
		title: "مادة 12",
		content: () => {
			return (
				<Form.List name="records">
					{(fields, { add, remove }) => (
						<>
							<ol>
								{fields.map(({ key, name, fieldKey, ...restField }) => (
									<li>
										<Space
											key={key}
											style={{ display: "flex", marginBottom: 8 }}
											align="baseline"
										>
											<RequiredInput
												itemProps={{
													...restField,
													name,
													fieldKey,
												}}
												inputProps={{ placeholder: "السجل" }}
											/>
											<MinusCircleTwoTone
												onClick={() => remove(name)}
												twoToneColor={["#ff686a", "#ffdcdc"]}
											/>
										</Space>
									</li>
								))}
							</ol>
							<Form.Item>
								<Button
									type="dashed"
									onClick={() => add()}
									icon={<PlusCircleTwoTone />}
									block
								>
									إضافة سجل
								</Button>
							</Form.Item>
						</>
					)}
				</Form.List>
			);
		},
	},

	{
		title: "مادة 17",
		content: () => {
			return (
				<RequiredInput
					itemProps={{
						name: "bot_number",
						label: "عدد أعضاء مجلس الأمناء",
					}}
					inputProps={{
						type: "number",
					}}
				/>
			);
		},
	},

	{
		title: "مادة 32",
		content: (
			belong: Number,
			setBelong: React.Dispatch<React.SetStateAction<number>>
		) => {
			return (
				<>
					<Form.Item name={["belonging", "type"]} initialValue={0}>
						<Radio.Group onChange={(e) => setBelong(e.target.value)}>
							<Radio value={0}>
								صندوق دعم مشروعات الجمعيات والمؤسسات الاهلية
							</Radio>
							<Radio value={1}>جمعية</Radio>
							<Radio value={2}>مؤسسة</Radio>
						</Radio.Group>
					</Form.Item>
					{belong !== 0 && (
						<Space>
							<RequiredInput
								itemProps={{ name: ["belonging", "name"] }}
								inputProps={{ placeholder: "الاسم" }}
							/>
							<RequiredInput
								itemProps={{ name: ["belonging", "registration_number"] }}
								inputProps={{ placeholder: "رقم القيد", type: "number" }}
							/>
							<RequiredInput
								itemProps={{ name: ["belonging", "governorate"] }}
								inputProps={{ placeholder: "المحافظة" }}
							/>
						</Space>
					)}
					<Form.List name={["founders", "natural_persons"]}>
						{(fields, { add, remove }) => (
							<>
								<ol>
									{fields.map(({ key, name, fieldKey, ...restField }) => (
										<li>
											<Space
												key={key}
												style={{ display: "flex", marginBottom: 8 }}
												align="baseline"
											>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "name"],
														fieldKey: [fieldKey, "name"],
													}}
													inputProps={{ placeholder: "الاسم" }}
												/>
												<Form.Item
													{...restField}
													name={[name, "age"]}
													fieldKey={[fieldKey, "age"]}
													rules={[{ required: true }]}
												>
													<InputNumber placeholder="السن" />
												</Form.Item>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "nationality"],
														fieldKey: [fieldKey, "nationality"],
													}}
													inputProps={{ placeholder: "الجنسية" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "job"],
														fieldKey: [fieldKey, "job"],
													}}
													inputProps={{ placeholder: "الوظيفة" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "position"],
														fieldKey: [fieldKey, "position"],
													}}
													inputProps={{ placeholder: "المنصب" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "share"],
														fieldKey: [fieldKey, "share"],
													}}
													inputProps={{
														placeholder: "الحصة",
														type: "number",
													}}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "address"],
														fieldKey: [fieldKey, "address"],
													}}
													inputProps={{ placeholder: "محل الاقامة" }}
												/>
												<MinusCircleTwoTone
													onClick={() => remove(name)}
													twoToneColor={["#ff686a", "#ffdcdc"]}
												/>
											</Space>
										</li>
									))}
								</ol>
								<Form.Item>
									<Button
										type="dashed"
										onClick={() => add()}
										icon={<PlusCircleTwoTone />}
										block
									>
										إضافة مؤسس من الأشخاص الطبيعيين
									</Button>
								</Form.Item>
							</>
						)}
					</Form.List>
					<Form.List name={["founders", "legal_persons"]}>
						{(fields, { add, remove }) => (
							<>
								<ol>
									{fields.map(({ key, name, fieldKey, ...restField }) => (
										<li>
											<Space
												key={key}
												style={{ display: "flex", marginBottom: 8 }}
												align="baseline"
											>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "name"],
														fieldKey: [fieldKey, "name"],
													}}
													inputProps={{ placeholder: "الاسم" }}
												/>
												<Form.Item
													{...restField}
													name={[name, "age"]}
													fieldKey={[fieldKey, "age"]}
													rules={[{ required: true }]}
												>
													<InputNumber placeholder="السن" />
												</Form.Item>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "nationality"],
														fieldKey: [fieldKey, "nationality"],
													}}
													inputProps={{ placeholder: "الجنسية" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "job"],
														fieldKey: [fieldKey, "job"],
													}}
													inputProps={{ placeholder: "الوظيفة" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "position"],
														fieldKey: [fieldKey, "position"],
													}}
													inputProps={{ placeholder: "المنصب" }}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "share"],
														fieldKey: [fieldKey, "share"],
													}}
													inputProps={{
														placeholder: "الحصة",
														type: "number",
													}}
												/>
												<RequiredInput
													itemProps={{
														...restField,
														name: [name, "address"],
														fieldKey: [fieldKey, "address"],
													}}
													inputProps={{ placeholder: "محل الاقامة" }}
												/>
												<MinusCircleTwoTone
													onClick={() => remove(name)}
													twoToneColor={["#ff686a", "#ffdcdc"]}
												/>
											</Space>
										</li>
									))}
								</ol>
								<Form.Item>
									<Button
										type="dashed"
										onClick={() => add()}
										icon={<PlusCircleTwoTone />}
										block
									>
										إضافة مؤسس من الأشخاص الاعتباريين
									</Button>
								</Form.Item>
							</>
						)}
					</Form.List>
					<Form.Item>
						<Button type="primary" htmlType="submit" block>
							تم
						</Button>
					</Form.Item>
				</>
			);
		},
	},
];

export { StatuteListSteps };
