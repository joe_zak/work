import React from "react";
import { Layout, PageHeader } from "antd";
import { useHistory } from "react-router-dom";

const { Content, Footer } = Layout;

interface MyLayoutProps {
	children: React.ReactNode;
	header?: String;
}

function MyLayout({ children, header = "" }: MyLayoutProps) {
	const history = useHistory();

	return (
		<Layout style={{ minHeight: "100vh" }}>
			<PageHeader
				onBack={() => {
					history.push("/");
				}}
				title={header}
			/>
			<Content style={{ paddingInline: "10%", paddingBlock: "20px" }}>
				{children}
			</Content>
			<Footer style={{ textAlign: "center" }}>copyrights reserved</Footer>
		</Layout>
	);
}

export default MyLayout;
