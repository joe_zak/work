import React from "react";
import { Form, Input, FormItemProps, InputProps } from "antd";

interface _formInputProps {
	itemProps?: FormItemProps;
	inputProps?: InputProps;
}

function RequiredInput({ itemProps, inputProps }: _formInputProps) {
	return (
		<Form.Item {...itemProps} rules={[{ required: true }]}>
			<Input {...inputProps} />
		</Form.Item>
	);
}

function OptionalInput({ itemProps, inputProps }: _formInputProps) {
	return (
		<Form.Item {...itemProps}>
			<Input {...inputProps} />
		</Form.Item>
	);
}

export { RequiredInput, OptionalInput };
