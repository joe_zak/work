import React, { useEffect, useState } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { Spin } from "antd";

import HomeScreen from "./screens/home.screen";
import StatuteListScreen from "./screens/statute_list.screen";
import InfoScreen from "./screens/info.screen";
import AddScreen from "./screens/add.screen";

function App() {
	const [items, setItems] = useState([]);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		fetch("http://localhost:5000/")
			.then((res) => res.json())
			.then((json) => {
				setItems(json);
				setLoading(false);
			});
	}, []);

	return (
		<Spin spinning={loading}>
			<BrowserRouter>
				<Route path="/" exact>
					<HomeScreen items={items} />
				</Route>
				<Route exact path="/info/:name" component={InfoScreen} />
				<Route exact path="/statute_list/:name" component={StatuteListScreen} />
				<Route exact path="/add" component={AddScreen} />
			</BrowserRouter>
		</Spin>
	);
}

export default App;
