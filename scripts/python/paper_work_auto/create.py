from docxtpl import DocxTemplate
import os


def CreateStatuteList(data):
    doc = DocxTemplate("./مؤسسات/لائحة النظام الأساسي للمؤسسات.docx")

    if not os.path.isdir(f"./{data['name']}"):
        os.mkdir(f"./{data['name']}")

    # data = {
    # 	"name": "حبة الخردل الاجتماعية",
    # 	"registration_number": "6533",
    # 	"registration_day": "4",
    # 	"registration_month": "6",
    # 	"registration_year": "2006",
    # 	"management": "الزيتون",
    # 	"headquarters": "105 ش طومان باي الزيتون",
    # 	"main_field": "رعاية الفئات الخاصة والمعوقين",
    # 	"scope": "الجمهورية",
    # 	"branches": [
    # 		{ "name": "1", "registration_number": "1", "governorate": "القاهرة" },
    # 		{ "name": "2", "registration_number": "2", "governorate": "القاهرة" },
    # 	],
    # 	"fields": [
    # 		"الخدمات الثقافية والعلمية والدينية",
    # 		"المساعدات الاجتماعية",
    # 		"رعاية الطفولة والأمومة",
    # 		"رعاية الأسرة",
    # 		"رعاية الشيخوخة",
    # 		"أصحاب المعاشات",
    # 		"رعاية الفئات الخاصة والمعاقين",
    # 		"التنمية الإقتصادية لزيادة دخل الأسرة",
    # 		"الدفاع الإجتماعي",
    # 		"الأنشطة الصحية",
    # 		"حماية البيئة والمحافظة عليها",
    # 		"الصداقة بين الشعوب",
    # 		"تنظيم الأسرة",
    # 		"رعاية المسجونين وأسرهم",
    # 		"التنظيم والإدارة",
    # 		"حماية المستهلك",
    # 		"حقوق الإنسان",
    # 		"النشاط الأدبي",
    # 	],
    # 	"activities": [
    # 		"a",
    # 		"b",
    # 		"c",
    # 		"d",
    # 	],
    # 	"capitals": [
    # 		{"name": "نقود", "value": "100 الف جنية فقط", "bond": "100 الف جنية فقط"},
    # 	],
    # 	"records": [
    # 		"العهدة",
    # 	],
    # 	"bot_number": "9",
    # 	"belonging": {
    # 		"type": 0,
    # 		"name": "اليوبيل الذهبي",
    # 		"registration_number": "123",
    # 		"governorate": "القاهرة"
    # 	},
    # 	"founders": {
    # 		"natural_persons": [{
    # 			"name": "1",
    # 			"age": "13",
    # 			"nationality": "مصري",
    # 			"job": "محاسب",
    # 			"position": "مؤسس",
    # 			"share": "50%",
    # 			"address": "ش الشارع - القاهرة",
    # 		}],
    # 		"legal_persons": [],
    # 	}
    # }

    doc.render(data)

    doc.save(f"./{data['name']}/لائحة النظام الأساسي.docx")


if __name__ == "__main__":
    CreateStatuteList({
        "name":
        "حبة الخردل",
        "registration_number":
        "321",
        "management":
        "الزيتون",
        "headquarters":
        "20 ش المشروع عين شمس الغربية - القاهرة",
        "main_field":
        "المساعدات الاجتماعية",
        "scope":
        "الجمهورية",
        "branches": [{
            "name": "asd",
            "registration_number": 98,
            "governorate": "dsa"
        }],
        "fields": ["المساعدات الاجتماعية"],
        "activities": [
            "1111111111111111111111111111111111111111111",
            "2222222222222222222222222222222222",
            "3333333333333333333333333333333333"
        ],
        "capitals": [{
            "name": "نقود",
            "value": "100 الف جنية",
            "bond": "ايداع بنكي"
        }],
        "records": ["العهدة"],
        "bot_number":
        "7",
        "belonging": {
            "type": 0
        },
        "founders": [{
            "name": "z",
            "age": 10,
            "nationality": "x",
            "job": "c",
            "position": "v",
            "share": "60",
            "address": "b"
        }],
        "registration_day":
        13,
        "registration_month":
        8,
        "registration_year":
        2021
    })
