@echo off
if "%1%"=="" goto usage
if "%1%"=="local" goto local
if "%1%"=="remote" goto remote

:usage
	echo Usage: update.cmd {remote/local}
	exit /b 1

:local
	echo $ git fetch origin master
	echo.
	git fetch origin master -v
	echo.
	echo $ git merge origin master
	echo.
	git merge origin master -v
	echo.
	exit /b 0

:remote
	echo $ git add .
	echo.
	git add . -v
	echo.
	echo $ git commit -a -m "Update"
	echo.
	git commit -a -m "Update" -v
	echo.
	echo $ git push origin master
	echo.
	git push origin master -v
	echo.
	exit /b 0

