import tkinter as tk
import tkinter.ttk as ttk
from datetime import datetime, timedelta
import json
import math

class App:
	def __init__(self):
		self.file = open("job.json", "r+")
		self.data = json.load(self.file)
		self.root = tk.Tk()
		self.root.geometry("500x300")
		self.root.minsize(500, 300)
		self.root.protocol("WM_DELETE_WINDOW", lambda: (self.root.destroy(), self.file.close()))
		self.root.bind("<Escape>", lambda event: (self.show(self.main)))
		self.root.bind("<Control-q>", lambda event: (self.root.destroy(), self.file.close()))
		self.container = tk.Frame(self.root)
		self.container.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
		self.container.grid_rowconfigure(0, weight=1)
		self.container.grid_columnconfigure(0, weight=1)
		self.frames = {}
		for function in (self.main, self.add_w):
			self.frames[function] = function()
		self.show(self.main)

	def main(self):
		frame = tk.Frame(self.container)
		frame.grid(row=0, column=0, sticky=tk.NSEW)
		frame.rowconfigure(0, weight=1)
		frame.rowconfigure(1, weight=1)
		frame.columnconfigure(0, weight=1)
		frame.columnconfigure(1, weight=1)
		frame.columnconfigure(2, weight=1)
		frame.columnconfigure(3, weight=1)
		frame.columnconfigure(4, weight=1)
		add_b = ttk.Button(frame, text="+", command=lambda: self.show(self.add_w))
		add_b.grid(row=0, column=0, pady=10)
		delete_b = ttk.Button(frame, text="-", command=self.delete)
		delete_b.grid(row=0, column=1, pady=10)
		edit_b = ttk.Button(frame, text="Edit", command=self.edit_w)
		edit_b.grid(row=0, column=2, pady=10)
		self.tree = ttk.Treeview(frame)
		self.tree["columns"] = ("begin_date", "end_date")
		self.tree.heading("#0", text="Title")
		self.tree.heading("begin_date", text="Begin Date")
		self.tree.heading("end_date", text="End Date")
		self.tree.column("#0", width=150)
		self.tree.column("begin_date", width=20)
		self.tree.column("end_date", width=20)
		self.tree.grid(row=1, column=0, columnspan=5, sticky="sew")
		self.tree.bind("<Double-1>", self.view)
		self.tree.bind("<Delete>", self.delete)
		self.tree.bind("<Control-e>", self.edit_w)
		for i in self.data:
			self.tree.insert("", "end", text=i, values=(self.data[i]["begin_date"], self.data[i]["end_date"]))
		self.tree.focus()
		return frame

	def edit_w(self, event=None):
		try:
			iid = self.tree.selection()[0]
			title = self.tree.item(iid, "text")
			frame = tk.Frame(self.container)
			frame.grid(row=0, column=0, sticky=tk.NSEW)
			frame.grid_columnconfigure(1, weight=1)
			frame.bind("<Escape>", frame.destroy)
			name_l = ttk.Label(frame, text="Name:")
			name_l.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)
			name_e = ttk.Entry(frame)
			name_e.insert(0, title)
			name_e.grid(row=1, column=1, padx=15, sticky=tk.EW)
			begin_date_l = ttk.Label(frame, text="Begin Date:")
			begin_date_l.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)
			begin_date_e = ttk.Entry(frame)
			begin_date_e.insert(0, self.data[title]["begin_date"])
			begin_date_e.grid(row=2, column=1, padx=15, sticky=tk.EW)
			end_date_l = ttk.Label(frame, text="End Date (default: 10 pages/day):")
			end_date_l.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)
			end_date_e = ttk.Entry(frame)
			end_date_e.insert(0, self.data[title]["end_date"])
			end_date_e.grid(row=3, column=1, padx=15, sticky=tk.EW)
			total_no_l = ttk.Label(frame, text="Total No. Pages:")
			total_no_l.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)
			total_no_e = ttk.Entry(frame)
			total_no_e.insert(0, self.data[title]["total_no"])
			total_no_e.grid(row=4, column=1, padx=15, sticky=tk.EW)
			edit_b = ttk.Button(frame, text="Edit", command=lambda: self.edit(frame, name_e.get(), begin_date_e.get(), end_date_e.get(), total_no_e.get(), iid))
			edit_b.grid(row=6, columnspan=2, pady=10)
			frame.tkraise()
		except Exception:
			pass

	def edit(self, master, name, begin_date, end_date, total_no, iid):
		msg = "Invalid format"
		try:
			begin_date = datetime.strptime(begin_date, "%Y-%m-%d")
			total_no = int(total_no)
			if not end_date:
				end_date = begin_date + timedelta(days=math.ceil(total_no/10))
			else:
				end_date = datetime.strptime(end_date, "%Y-%m-%d")
			if (end_date - begin_date).days < 0:
				msg = "Begin date is after the end date"
				raise ValueError()
			self.data[name] = {}
			self.data[name]["begin_date"] = begin_date.strftime("%Y-%m-%d")
			self.data[name]["end_date"] = end_date.strftime("%Y-%m-%d")
			self.data[name]["total_no"] = total_no
			self.data[name]["pages_done"] = 0
			self.write()
			self.tree.detach(iid)
			self.tree.insert("", "end", text=name, values=(self.data[name]["begin_date"], self.data[name]["end_date"]))
			self.show(self.main)
		except Exception:
			error_l = tk.Label(master, text=msg, fg="red")
			error_l.grid(row=0, columnspan=2, pady=10)
			error_l.after(3500, error_l.destroy)

	def delete(self, event=None):
		try:
			selection = self.tree.selection()
			for iid in selection:
				title = self.tree.item(iid, "text")
				del self.data[title]
				self.write()
				self.tree.detach(iid)
		except Exception:
			pass

	def view(self, event):
		try:
			title = self.tree.item(self.tree.selection()[0], "text")
			frame = tk.Frame(self.container)
			frame.grid(row=0, column=0, sticky=tk.NSEW)
			frame.bind("<Escape>", frame.destroy)
			style = ttk.Style()
			style.configure("my.TButton", font=("Calibri", 20))
			up_b = ttk.Button(frame, text="\u2206", style="my.TButton", command=lambda: self.change(title, 1, pages_l))
			up_b.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
			pages_l = tk.Label(frame, text=f"{self.data[title]['pages_done']}/{self.data[title]['total_no']}", font="Calibri 20")
			pages_l.pack(fill=tk.X, expand=True)
			down_b = ttk.Button(frame, text="\u2207", style="my.TButton", command=lambda: self.change(title, -1, pages_l))
			down_b.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
			frame.tkraise()
		except Exception:
			pass

	def change(self, title, num, pages_l):
		new = self.data[title]["pages_done"] + num
		if new <= self.data[title]["total_no"] and new >= 0:
			self.data[title]["pages_done"] = new
			pages_l.config(text=f"{self.data[title]['pages_done']}/{self.data[title]['total_no']}")
			self.write()

	def add_w(self):
		frame = tk.Frame(self.container)
		frame.grid(row=0, column=0, sticky=tk.NSEW)
		frame.grid_columnconfigure(1, weight=1)
		name_l = ttk.Label(frame, text="Name:")
		name_l.grid(row=1, column=0, padx=10, pady=10, sticky=tk.W)
		name_e = ttk.Entry(frame)
		name_e.grid(row=1, column=1, padx=15, sticky=tk.EW)
		begin_date_l = ttk.Label(frame, text="Begin Date:")
		begin_date_l.grid(row=2, column=0, padx=10, pady=10, sticky=tk.W)
		begin_date_e = ttk.Entry(frame)
		begin_date_e.grid(row=2, column=1, padx=15, sticky=tk.EW)
		end_date_l = ttk.Label(frame, text="End Date (default: 10 pages/day):")
		end_date_l.grid(row=3, column=0, padx=10, pady=10, sticky=tk.W)
		end_date_e = ttk.Entry(frame)
		end_date_e.grid(row=3, column=1, padx=15, sticky=tk.EW)
		total_no_l = ttk.Label(frame, text="Total No. Pages:")
		total_no_l.grid(row=4, column=0, padx=10, pady=10, sticky=tk.W)
		total_no_e = ttk.Entry(frame)
		total_no_e.grid(row=4, column=1, padx=15, sticky=tk.EW)
		add_b = ttk.Button(frame, text="Add", command=lambda: self.add(frame, name_e.get(), begin_date_e.get(), end_date_e.get(), total_no_e.get()))
		add_b.grid(row=6, columnspan=2, pady=10)
		return frame

	def write(self):
		self.file.seek(0)
		json.dump(self.data, self.file)
		self.file.truncate()

	def add(self, master, name, begin_date, end_date, total_no):
		msg = "Invalid format"
		try:
			begin_date = datetime.strptime(begin_date, "%Y-%m-%d")
			total_no = int(total_no)
			if not end_date:
				end_date = begin_date + timedelta(days=math.ceil(total_no/10))
			else:
				end_date = datetime.strptime(end_date, "%Y-%m-%d")
			if (end_date - begin_date).days < 0:
				msg = "Begin date is after the end date"
				raise ValueError()
			if name in self.data:
				msg = "Already exists"
				raise ValueError()
			self.data[name] = {}
			self.data[name]["begin_date"] = begin_date.strftime("%Y-%m-%d")
			self.data[name]["end_date"] = end_date.strftime("%Y-%m-%d")
			self.data[name]["total_no"] = total_no
			self.data[name]["pages_done"] = 0
			self.write()
			self.tree.insert("", "end", text=name, values=(self.data[name]["begin_date"], self.data[name]["end_date"]))
			self.show(self.main)
		except Exception:
			error_l = tk.Label(master, text=msg, fg="red")
			error_l.grid(row=0, columnspan=2, pady=10)
			error_l.after(3500, error_l.destroy)

	def show(self, function):
		self.frames[function].tkraise()

	def run(self):
		self.root.mainloop()

app = App()
app.run()
