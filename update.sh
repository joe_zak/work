#!/bin/bash
if [ "$#" -eq 1 ]
then
	shopt -s nocasematch
	if [[ "$1" == "local" ]]
	then
		echo $ git pull origin master
		echo
		git pull origin master -v
		echo
	elif [[ "$1" == "remote" ]]
	then
		echo $ git add .
		echo
		git add . -v
		echo
		echo $ git commit -a -m \"Update\"
		echo
		git commit -a -m "Update" -v
		echo
		echo $ git push origin master
		echo
		git push origin master -v
		echo
	else
		echo Usage: $0 {remote/local}
	fi
else
	echo Usage: $0 {remote/local}
fi
